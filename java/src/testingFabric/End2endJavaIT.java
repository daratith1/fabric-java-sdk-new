/*
 *
 *  Copyright 2016,2017 DTCC, Fujitsu Australia Software Technology, IBM - All Rights Reserved.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package testingFabric;

import static java.lang.String.format;
import static java.nio.charset.StandardCharsets.UTF_8;
import static org.hyperledger.fabric.sdk.Channel.NOfEvents.createNofEvents;
import static org.hyperledger.fabric.sdk.Channel.TransactionOptions.createTransactionOptions;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static testingFabric.TestUtils.getPEMStringFromPrivateKey;
import static testingFabric.TestUtils.testRemovingAddingPeersOrderers;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.nio.file.Paths;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.spec.InvalidKeySpecException;
import java.util.Collection;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.Vector;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.regex.Pattern;

import org.hyperledger.fabric.sdk.BlockEvent;
import org.hyperledger.fabric.sdk.ChaincodeEndorsementPolicy;
import org.hyperledger.fabric.sdk.ChaincodeEvent;
import org.hyperledger.fabric.sdk.ChaincodeID;
import org.hyperledger.fabric.sdk.Channel;
import org.hyperledger.fabric.sdk.Enrollment;
import org.hyperledger.fabric.sdk.HFClient;
import org.hyperledger.fabric.sdk.InstallProposalRequest;
import org.hyperledger.fabric.sdk.InstantiateProposalRequest;
import org.hyperledger.fabric.sdk.NetworkConfig;
import org.hyperledger.fabric.sdk.Orderer;
import org.hyperledger.fabric.sdk.Peer;
import org.hyperledger.fabric.sdk.ProposalResponse;
import org.hyperledger.fabric.sdk.SDKUtils;
import org.hyperledger.fabric.sdk.TransactionProposalRequest;
import org.hyperledger.fabric.sdk.TxReadWriteSetInfo;
import org.hyperledger.fabric.sdk.User;
import org.hyperledger.fabric.sdk.BlockEvent.TransactionEvent;
import org.hyperledger.fabric.sdk.Peer.PeerRole;
import org.hyperledger.fabric.sdk.TransactionRequest.Type;
import org.hyperledger.fabric.sdk.exception.CryptoException;
import org.hyperledger.fabric.sdk.exception.InvalidArgumentException;
import org.hyperledger.fabric.sdk.exception.ProposalException;
import org.hyperledger.fabric.sdk.exception.TransactionEventException;
import org.hyperledger.fabric.sdk.security.CryptoSuite;
import org.hyperledger.fabric_ca.sdk.EnrollmentRequest;
import org.hyperledger.fabric_ca.sdk.HFCAClient;
import org.hyperledger.fabric_ca.sdk.HFCAIdentity;
import org.hyperledger.fabric_ca.sdk.RegistrationRequest;
import org.hyperledger.fabric_ca.sdk.exception.EnrollmentException;
import org.hyperledger.fabric_ca.sdk.exception.IdentityException;
import org.junit.BeforeClass;
import org.junit.Test;

/*
    This runs a version of end2end but with Java chaincode.
    It requires that End2endIT has been run already to do all enrollment and setting up of orgs,
    creation of the channels. None of that is specific to chaincode deployment language.
 */

public class End2endJavaIT extends End2endIT {

	{
		testName = "End2endJavaIT"; // Just print out what test is really running.

		// This is what changes are needed to deploy and run Node code.

		// this is relative to src/test/fixture and is where the Node chaincode source
		// is.
		CHAIN_CODE_FILEPATH = "sdkintegration/javacc/userWallet"; // override path to Node code
		CHAIN_CODE_PATH = null; // This is used only for GO.
		CHAIN_CODE_NAME = "Wallet_java"; // chaincode name.
		CHAIN_CODE_LANG = Type.JAVA; // language is Java.

	}
	String user = "user3";
	Integer found = 0;
	private static NetworkConfig networkConfig;
	private static final String TEST_ORG = "Org1";

	@BeforeClass
	public static void doMainSetup() throws Exception {
		networkConfig = NetworkConfig.fromYamlFile(testConfig.getTestNetworkConfigFileYAML());
		networkConfig.getOrdererNames().forEach(ordererName -> {
			try {
				Properties ordererProperties = networkConfig.getOrdererProperties(ordererName);
				Properties testProp = testConfig.getEndPointProperties("orderer", ordererName);
				ordererProperties.setProperty("clientCertFile", testProp.getProperty("clientCertFile"));
				ordererProperties.setProperty("clientKeyFile", testProp.getProperty("clientKeyFile"));
				networkConfig.setOrdererProperties(ordererName, ordererProperties);

			} catch (InvalidArgumentException e) {
				throw new RuntimeException(e);
			}
		});

		networkConfig.getPeerNames().forEach(peerName -> {
			try {
				Properties peerProperties = networkConfig.getPeerProperties(peerName);
				Properties testProp = testConfig.getEndPointProperties("peer", peerName);
				peerProperties.setProperty("clientCertFile", testProp.getProperty("clientCertFile"));
				peerProperties.setProperty("clientKeyFile", testProp.getProperty("clientKeyFile"));
				networkConfig.setPeerProperties(peerName, peerProperties);

			} catch (InvalidArgumentException e) {
				throw new RuntimeException(e);
			}
		});

	}

	@Override
	@Test
	public void setup() throws Exception {
		sampleStore = new SampleStore(sampleStoreFile);
//		super.testUser1 = this.user;
//		queryListIdentity(sampleStore);
//		if (found == 0) {
		enrollUsersSetup(sampleStore);
//		}
		runFabricTest(sampleStore); // just run fabric tests.
	}

	@Override
	void runChannel(HFClient client, Channel channel, boolean installChaincode, SampleOrg sampleOrg, int delta) {
		// TODO Auto-generated method stub
		class ChaincodeEventCapture { // A test class to capture chaincode events
			final String handle;
			final BlockEvent blockEvent;
			final ChaincodeEvent chaincodeEvent;

			ChaincodeEventCapture(String handle, BlockEvent blockEvent, ChaincodeEvent chaincodeEvent) {
				this.handle = handle;
				this.blockEvent = blockEvent;
				this.chaincodeEvent = chaincodeEvent;
			}
		}

		// The following is just a test to see if peers and orderers can be added and
		// removed.
		// not pertinent to the code flow.
		testRemovingAddingPeersOrderers(client, channel);

		Vector<ChaincodeEventCapture> chaincodeEvents = new Vector<>(); // Test list to capture chaincode events.

		try {

			final String channelName = channel.getName();
			boolean isFooChain = FOO_CHANNEL_NAME.equals(channelName);
			out("Running channel %s", channelName);

			Collection<Orderer> orderers = channel.getOrderers();
			final ChaincodeID chaincodeID;
			Collection<ProposalResponse> responses = null;
			Collection<ProposalResponse> successful = new LinkedList<>();
			Collection<ProposalResponse> failed = new LinkedList<>();

			// Register a chaincode event listener that will trigger for any chaincode id
			// and only for EXPECTED_EVENT_NAME event.

			String chaincodeEventListenerHandle = channel.registerChaincodeEventListener(Pattern.compile(".*"),
					Pattern.compile(Pattern.quote(EXPECTED_EVENT_NAME)), (handle, blockEvent, chaincodeEvent) -> {

						chaincodeEvents.add(new ChaincodeEventCapture(handle, blockEvent, chaincodeEvent));

						String es = blockEvent.getPeer() != null ? blockEvent.getPeer().getName() : "peer was null!!!";
						out("RECEIVED Chaincode event with handle: %s, chaincode Id: %s, chaincode event name: %s, "
								+ "transaction id: %s, event payload: \"%s\", from event source: %s", handle,
								chaincodeEvent.getChaincodeId(), chaincodeEvent.getEventName(),
								chaincodeEvent.getTxId(), new String(chaincodeEvent.getPayload()), es);

					});

			// For non foo channel unregister event listener to test events are not called.
			if (!isFooChain) {
				channel.unregisterChaincodeEventListener(chaincodeEventListenerHandle);
				chaincodeEventListenerHandle = null;

			}

			// Deprecated use v2.0 Lifecycle chaincode management.
			ChaincodeID.Builder chaincodeIDBuilder = ChaincodeID.newBuilder().setName(CHAIN_CODE_NAME)
					.setVersion(CHAIN_CODE_VERSION);
			if (null != CHAIN_CODE_PATH) {
				chaincodeIDBuilder.setPath(CHAIN_CODE_PATH);

			}
			chaincodeID = chaincodeIDBuilder.build();

			if (installChaincode) {

				installChaincode(client, channel, installChaincode, sampleOrg, chaincodeID, responses, successful,
						failed, delta);

			}

			initiateChaincode(client, channel, installChaincode, sampleOrg, chaincodeID, responses, successful, failed,
					delta);

//            Collection<ProposalResponse> data = readObjFromFile();
			Channel.NOfEvents nOfEvents = createNofEvents();
			if (!channel.getPeers(EnumSet.of(PeerRole.EVENT_SOURCE)).isEmpty()) {
				nOfEvents.addPeers(channel.getPeers(EnumSet.of(PeerRole.EVENT_SOURCE)));
			}

			// insert tx to ledger
			channel.sendTransaction(successful, createTransactionOptions() // Basically the default options but shows
																			// it's usage.
					.userContext(client.getUserContext()) // could be a different user context. this is the default.
					.shuffleOrders(false) // don't shuffle any orderers the default is true.
					.orderers(channel.getOrderers()) // specify the orderers we want to try this transaction. Fails once
														// all Orderers are tried.
					.nOfEvents(nOfEvents) // The events to signal the completion of the interest in the transaction
			).thenApply(transactionEvent -> {

				waitOnFabric(0);

				assertTrue(transactionEvent.isValid()); // must be valid to be here.

				assertNotNull(transactionEvent.getSignature()); // musth have a signature.
				BlockEvent blockEvent = transactionEvent.getBlockEvent(); // This is the blockevent that has this
																			// transaction.
				assertNotNull(blockEvent.getBlock()); // Make sure the RAW Fabric block is returned.

				out("Finished instantiate transaction with transaction id %s", transactionEvent.getTransactionID());
				try {

					assertEquals(blockEvent.getChannelId(), channel.getName());
					successful.clear();
					failed.clear();

					client.setUserContext(sampleOrg.getUser(doctorOrg1));

					///////////////
					/// Send transaction proposal to all peers
					TransactionProposalRequest transactionProposalRequest = client.newTransactionProposalRequest();
					transactionProposalRequest.setChaincodeID(chaincodeID);
					transactionProposalRequest.setChaincodeLanguage(CHAIN_CODE_LANG);
					transactionProposalRequest.setFcn("createWallet");
					transactionProposalRequest.setProposalWaitTime(testConfig.getProposalWaitTime());
					transactionProposalRequest.setArgs("kaka", "200");
					Map<String, byte[]> tm2 = new HashMap<>();
					tm2.put("HyperLedgerFabric", "TransactionProposalRequest:JavaSDK".getBytes(UTF_8)); // Just some
																										// extra junk in
																										// transient map
					tm2.put("method", "TransactionProposalRequest".getBytes(UTF_8)); // ditto
					tm2.put("result", ":)".getBytes(UTF_8)); // This should be returned in the payload see chaincode
																// why.
					if (Type.GO_LANG.equals(CHAIN_CODE_LANG) && testConfig.isFabricVersionAtOrAfter("1.2")) {

						expectedMoveRCMap.put(channelName, random.nextInt(300) + 100L); // the chaincode will return
																						// this as status see chaincode
																						// why.
						tm2.put("rc", (expectedMoveRCMap.get(channelName) + "").getBytes(UTF_8)); // This should be
																									// returned see
																									// chaincode why.
						// 400 and above results in the peer not endorsing!
					} else {
						expectedMoveRCMap.put(channelName, 200L); // not really supported for Java or Node.
					}

					tm2.put(EXPECTED_EVENT_NAME, EXPECTED_EVENT_DATA); // This should trigger an event see chaincode
																		// why.

					transactionProposalRequest.setTransientMap(tm2);

//                        out("sending transactionProposal to all peers with arguments: move(a,b,100)");

					// Collection<ProposalResponse> transactionPropResp =
					// channel.sendTransactionProposalToEndorsers(transactionProposalRequest);
					Collection<ProposalResponse> transactionPropResp = channel
							.sendTransactionProposal(transactionProposalRequest, channel.getPeers());
					for (ProposalResponse response : transactionPropResp) {
						if (response.getStatus() == ProposalResponse.Status.SUCCESS) {
							out("Successful transaction proposal response Txid: %s from peer %s",
									response.getTransactionID(), response.getPeer().getName());
							successful.add(response);
						} else {
							failed.add(response);
						}
					}

					out("Received %d transaction proposal responses. Successful+verified: %d . Failed: %d",
							transactionPropResp.size(), successful.size(), failed.size());
					if (failed.size() > 0) {
						ProposalResponse firstTransactionProposalResponse = failed.iterator().next();
						fail("Not enough endorsers for invoke(move a,b,100):" + failed.size() + " endorser error: "
								+ firstTransactionProposalResponse.getMessage() + ". Was verified: "
								+ firstTransactionProposalResponse.isVerified());
					}

					// Check that all the proposals are consistent with each other. We should have
					// only one set
					// where all the proposals above are consistent. Note the when sending to
					// Orderer this is done automatically.
					// Shown here as an example that applications can invoke and select.
					// See org.hyperledger.fabric.sdk.proposal.consistency_validation config
					// property.
					Collection<Set<ProposalResponse>> proposalConsistencySets = SDKUtils
							.getProposalConsistencySets(transactionPropResp);
					if (proposalConsistencySets.size() != 1) {
						fail(format("Expected only one set of consistent proposal responses but got %d",
								proposalConsistencySets.size()));
					}
					out("Successfully received transaction proposal responses.");

					// System.exit(10);

					ProposalResponse resp = successful.iterator().next();
//                        byte[] x = resp.getChaincodeActionResponsePayload(); // This is the data returned by the chaincode.
//                        String resultAsString = null;
//                        if (x != null) {
//                            resultAsString = new String(x, UTF_8);
//                        }
//                        assertEquals(":)", resultAsString);

					assertEquals(expectedMoveRCMap.get(channelName).longValue(),
							resp.getChaincodeActionResponseStatus()); // Chaincode's status.

					TxReadWriteSetInfo readWriteSetInfo = resp.getChaincodeActionResponseReadWriteSetInfo();
					// See blockwalker below how to transverse this
					assertNotNull(readWriteSetInfo);
					assertTrue(readWriteSetInfo.getNsRwsetCount() > 0);

					ChaincodeID cid = resp.getChaincodeID();
					assertNotNull(cid);
					final String path = cid.getPath();
					if (null == CHAIN_CODE_PATH) {
						assertTrue(path == null || "".equals(path));

					} else {

						assertEquals(CHAIN_CODE_PATH, path);

					}

					assertEquals(CHAIN_CODE_NAME, cid.getName());
					assertEquals(CHAIN_CODE_VERSION, cid.getVersion());

					////////////////////////////
					// Send Transaction Transaction to orderer
//                        out("Sending chaincode transaction(move a,b,100) to orderer.");
					return channel.sendTransaction(successful).get(testConfig.getTransactionWaitTime(),
							TimeUnit.SECONDS);

				} catch (Exception e) {
					out("Caught an exception while invoking chaincode");
					e.printStackTrace();
					fail("Failed invoking chaincode with error : " + e.getMessage());
				}
				return null;

			}).get(testConfig.getTransactionWaitTime(), TimeUnit.SECONDS);

		} catch (Exception e) {
			out("Caught an exception running channel %s", channel.getName());
			e.printStackTrace();
			fail("Test failed with error : " + e.getMessage());
		}
	}

	private void initiateChaincode(HFClient client, Channel channel, boolean installChaincode, SampleOrg sampleOrg,
			ChaincodeID chaincodeID, Collection<ProposalResponse> responses, Collection<ProposalResponse> successful,
			Collection<ProposalResponse> failed, int delta) {

		try {

			InstantiateProposalRequest instantiateProposalRequest = client.newInstantiationProposalRequest();
			instantiateProposalRequest.setProposalWaitTime(DEPLOYWAITTIME);
			instantiateProposalRequest.setChaincodeID(chaincodeID);
			instantiateProposalRequest.setChaincodeLanguage(CHAIN_CODE_LANG);
			instantiateProposalRequest.setFcn("init");
			instantiateProposalRequest.setArgs("");
			Map<String, byte[]> tm = new HashMap<>();
			tm.put("HyperLedgerFabric", "InstantiateProposalRequest:JavaSDK".getBytes(UTF_8));
			tm.put("method", "InstantiateProposalRequest".getBytes(UTF_8));
			instantiateProposalRequest.setTransientMap(tm);

			/*
			 * policy OR(Org1MSP.member, Org2MSP.member) meaning 1 signature from someone in
			 * either Org1 or Org2 See README.md Chaincode endorsement policies section for
			 * more details.
			 */
			ChaincodeEndorsementPolicy chaincodeEndorsementPolicy = new ChaincodeEndorsementPolicy();
			chaincodeEndorsementPolicy
					.fromYamlFile(new File(TEST_FIXTURES_PATH + "/sdkintegration/chaincodeendorsementpolicy.yaml"));
			instantiateProposalRequest.setChaincodeEndorsementPolicy(chaincodeEndorsementPolicy);

//         out("Sending instantiateProposalRequest to all peers with arguments: a and b set to 500 and %s respectively", "" + (200 + delta));
			successful.clear();
			failed.clear();

//         if (isFooChain) {  //Send responses both ways with specifying peers and by using those on the channel.
//             //Deprecated use v2.0 Lifecycle chaincode management.
//             responses = channel.sendInstantiationProposal(instantiateProposalRequest, channel.getPeers());
//         } else {
			// Deprecated use v2.0 Lifecycle chaincode management.
			responses = channel.sendInstantiationProposal(instantiateProposalRequest);
//         }
			for (ProposalResponse response : responses) {
				if (response.isVerified() && response.getStatus() == ProposalResponse.Status.SUCCESS) {
					successful.add(response);
					out("Succesful instantiate proposal response Txid: %s from peer %s", response.getTransactionID(),
							response.getPeer().getName());
				} else {
					failed.add(response);
				}
			}
			out("Received %d instantiate proposal responses. Successful+verified: %d . Failed: %d", responses.size(),
					successful.size(), failed.size());
			if (failed.size() > 0) {
				for (ProposalResponse fail : failed) {

					out("Not enough endorsers for instantiate :" + successful.size() + "endorser failed with "
							+ fail.getMessage() + ", on peer" + fail.getPeer());

				}
				ProposalResponse first = failed.iterator().next();
				fail("Not enough endorsers for instantiate :" + successful.size() + "endorser failed with "
						+ first.getMessage() + ". Was verified:" + first.isVerified());
			}

//         storeObjectToSer(successful);
			///////////////
			/// Send instantiate transaction to orderer
//         out("Sending instantiateTransaction to orderer with a and b set to 500 and %s respectively", "" + (200 + delta));

			// Specify what events should complete the interest in this transaction. This is
			// the default
			// for all to complete. It's possible to specify many different combinations
			// like
			// any from a group, all from one group and just one from another or even
			// None(NOfEvents.createNoEvents).
			// See. Channel.NOfEvents

		} catch (Exception e) {
			out("Caught an exception running channel %s", channel.getName());
			e.printStackTrace();
			fail("Test failed with error : " + e.getMessage());
		}

	}

	private void installChaincode(HFClient client, Channel channel, boolean installChaincode, SampleOrg sampleOrg,
			ChaincodeID chaincodeID, Collection<ProposalResponse> responses, Collection<ProposalResponse> successful,
			Collection<ProposalResponse> failed, int delta) {

		try {
			client.setUserContext(sampleOrg.getPeerAdmin());

			out("Creating install proposal");

			// Deprecated use v2.0 Lifecycle chaincode management.
			InstallProposalRequest installProposalRequest = client.newInstallProposalRequest();
			installProposalRequest.setChaincodeID(chaincodeID);

			installProposalRequest.setChaincodeInputStream(Util
					.generateTarGzInputStream((Paths.get(TEST_FIXTURES_PATH, CHAIN_CODE_FILEPATH).toFile()), "src"));

			installProposalRequest.setChaincodeVersion(CHAIN_CODE_VERSION);
			installProposalRequest.setChaincodeLanguage(CHAIN_CODE_LANG);

			out("Sending install proposal");

			////////////////////////////
			// only a client from the same org as the peer can issue an install request
			int numInstallProposal = 0;
			// Set<String> orgs = orgPeers.keySet();
			// for (SampleOrg org : testSampleOrgs) {

			Collection<Peer> peers = channel.getPeers();
			numInstallProposal = numInstallProposal + peers.size();
			// Deprecated use v2.0 Lifecycle chaincode management.
			responses = client.sendInstallProposal(installProposalRequest, peers);

			for (ProposalResponse response : responses) {
				if (response.getStatus() == ProposalResponse.Status.SUCCESS) {
					out("Successful install proposal response Txid: %s from peer %s", response.getTransactionID(),
							response.getPeer().getName());
					successful.add(response);
				} else {
					failed.add(response);
				}
			}

			// }
//        out("Received %d install proposal responses. Successful+verified: %d . Failed: %d", numInstallProposal, successful.size(), failed.size());

			if (failed.size() > 0) {
				ProposalResponse first = failed.iterator().next();
				fail("Not enough endorsers for install :" + successful.size() + ".  " + first.getMessage());
			}
		} catch (Exception e) {
			out("Caught an exception running channel %s", channel.getName());
			e.printStackTrace();
			fail("Test failed with error : " + e.getMessage());
		}
	}

	private void storeObjectToSer(Collection<ProposalResponse> successful) {

		try {

			// write object to file
			FileOutputStream fos = new FileOutputStream(
					"/home/dara/Documents/fabric-java-sdk/src/test/fixture/sdkintegration/successful.ser");
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(successful);
			oos.close();

			// read object from file

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	private Collection<ProposalResponse> readObjFromFile() throws IOException {

		Collection<ProposalResponse> successful = new LinkedList<>();
		try {
			FileInputStream fis = new FileInputStream(
					"/home/dara/Documents/fabric-java-sdk/src/test/fixture/sdkintegration/successful.ser");
			ObjectInputStream ois = new ObjectInputStream(fis);
			successful = (Collection<ProposalResponse>) ois.readObject();
			ois.close();

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

		return successful;
	}


//	@Override
//	public void runFabricTest(SampleStore sampleStore) throws Exception {
//		// TODO Auto-generated method stub
//		HFClient client = getTheClient();
//
//		client.setCryptoSuite(CryptoSuite.Factory.getCryptoSuite());
//
//		////////////////////////////
//		// Construct and run the channels
//		SampleOrg sampleOrg = testConfig.getIntegrationTestsSampleOrg("peerOrg1");
//		Channel fooChannel = constructChannelTest(client, FOO_CHANNEL_NAME);
//		sampleStore.saveChannel(fooChannel);
//		runChannel(client, fooChannel, false, sampleOrg, 0);
//		assertFalse(fooChannel.isShutdown());
//		out("\n");
//		final ChaincodeID chaincodeID;
//
//		ChaincodeID.Builder chaincodeIDBuilder = ChaincodeID.newBuilder().setName(CHAIN_CODE_NAME)
//				.setVersion(CHAIN_CODE_VERSION);
//		chaincodeID = chaincodeIDBuilder.build();
//
////        queryChaincode(client, fooChannel, chaincodeID,sampleOrg);
////        insertNewTx(client, fooChannel, chaincodeID, sampleOrg);
//
////        blockWalker(client, fooChannel);
//		assertFalse(fooChannel.isShutdown());
//		assertTrue(fooChannel.isInitialized());
//		out("That's all folks!");
//
//	}

	@Override
	Channel constructChannel(String name, HFClient client, SampleOrg sampleOrg) throws Exception {
		// override this method since we don't want to construct the channel that's been
		// done.
		// Just get it out of the samplestore!

		System.out.print(sampleOrg.getPeerAdmin());

		client.setUserContext(sampleOrg.getPeerAdmin());
		return sampleStore.getChannel(client, name).initialize();

	}

	Channel constructChannelTest(HFClient client, String channelName) throws Exception {
		// TODO Auto-generated method stub
		// Channel newChannel = client.getChannel(channelName);
		Channel newChannel = client.loadChannelFromConfig(channelName, networkConfig);
		if (newChannel == null) {
			throw new RuntimeException("Channel " + channelName + " is not defined in the config file!");
		}

		return newChannel.initialize();
	}

	// Returns a new client instance
	private static HFClient getTheClient() throws Exception {

		HFClient client = HFClient.createNewInstance();
		client.setCryptoSuite(CryptoSuite.Factory.getCryptoSuite());

		User peerAdmin = getAdminUser(TEST_ORG);
		client.setUserContext(peerAdmin);

		return client;
	}

	private static User getAdminUser(String orgName) throws Exception {

		return networkConfig.getPeerAdmin(orgName);
	}
}
