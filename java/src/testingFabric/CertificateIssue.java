package testingFabric;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigInteger;
import java.net.MalformedURLException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.SignatureException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Collection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import main.java.org.app.util.Util;

import org.hyperledger.fabric.sdk.Enrollment;
import org.hyperledger.fabric.sdk.NetworkConfig;
import org.hyperledger.fabric.sdk.User;
import org.hyperledger.fabric.sdk.exception.NetworkConfigurationException;
import org.hyperledger.fabric.sdk.security.CryptoSuite;
import testingFabric.TestConfig;
import testingFabric.SampleStore;
import testingFabric.SampleUser;
import org.hyperledger.fabric_ca.sdk.Attribute;
import org.hyperledger.fabric_ca.sdk.EnrollmentRequest;
import org.hyperledger.fabric_ca.sdk.HFCACertificateRequest;
import org.hyperledger.fabric_ca.sdk.HFCACertificateResponse;
import org.hyperledger.fabric_ca.sdk.HFCAClient;
import org.hyperledger.fabric_ca.sdk.HFCACredential;
import org.hyperledger.fabric_ca.sdk.HFCAIdentity;
import org.hyperledger.fabric_ca.sdk.HFCAX509Certificate;
import org.hyperledger.fabric_ca.sdk.RegistrationRequest;
import org.hyperledger.fabric_ca.sdk.exception.EnrollmentException;
import org.hyperledger.fabric_ca.sdk.exception.IdentityException;
import org.hyperledger.fabric_ca.sdk.exception.InfoException;
import org.hyperledger.fabric_ca.sdk.exception.InvalidArgumentException;
import org.junit.Rule;
import org.junit.rules.ExpectedException;

import static java.lang.String.format;
import static java.nio.charset.StandardCharsets.UTF_8;
import static testingFabric.TestUtils.resetConfig;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class CertificateIssue{

	@Rule
	public ExpectedException thrown = ExpectedException.none();

	private static final String TEST_ADMIN_NAME = "admin";
	private static final String TEST_ADMIN_PW = "adminpw";
	private static final String TEST_ADMIN_ORG1 = "org1";
	private static final String TEST_ADMIN_ORG2 = "org2";
	private static final String TEST_USER1_AFFILIATION = "org1.department1";
	private static final String TEST_USER2_AFFILIATION = "org2.department1";
	private static final String TEST_WITH_INTEGRATION_ORG1 = "peerOrg1";
	private static final String TEST_WITH_INTEGRATION_ORG2 = "peerOrg2";
	private static final String CA0 = "ca0";
	private static final String CA1 = "ca1";
	private static final String CA_ORG1 = "/home/dara/Documents/fabric-java-sdk/src/test/fixture/sdkintegration/e2e-2Orgs/v1.3/crypto-config/peerOrganizations/org1.example.com/ca/ca.org1.example.com-cert.pem";
	private static final String CA_ORG2 = "/home/dara/Documents/fabric-java-sdk/src/test/fixture/sdkintegration/e2e-2Orgs/v1.3/crypto-config/peerOrganizations/org2.example.com/ca/ca.org2.example.com-cert.pem";

	private static SampleStore sampleStore;
	private static HFCAClient client;
	private static SampleUser admin;
	private static NetworkConfig networkConfig;
	private static SampleUser user;
	private static RegistrationRequest rr;
	protected static String org;
	String[] username = { "Patient_1", "Patient_3" };
	String[] dir = { "usersTest/" + username[0], "usersTest/" + username[1] };
	String rootCADir = "";
	private static CryptoSuite crypto;
	public String cert = "";

	// Keeps track of how many test users we've created
	private static int userCount = 0;

	// Common prefix for all test users (the suffix will be the current user count)
	// Note that we include the time value so that these tests can be executed
	// repeatedly
	// without needing to restart the CA (because you cannot register a username
	// more than once!)
	private static String userNamePrefix = "user" + (System.currentTimeMillis() / 1000) + "_";

	private static TestConfig testConfig = TestConfig.getConfig();

	static void out(String format, Object... args) {

		System.err.flush();
		System.out.flush();

		System.out.println(format(format, args));
		System.err.flush();
		System.out.flush();

	}

	public CertificateIssue(String username, String role, String organization, String phoneNumber, String nationID)
			throws Exception {

		cert = issueCert(username, role, organization, phoneNumber, nationID);
	}

	public static void init() throws Exception {
		out("\n\n\nRUNNING: HFCAClientEnrollIT.\n");

		resetConfig();

		crypto = CryptoSuite.Factory.getCryptoSuite();
	}

	public static void setup(String organization) throws Exception {

		File sampleStoreFile = new File(System.getProperty("java.io.tmpdir") + "/HFCSampletest.properties");
		if (sampleStoreFile.exists()) { // For testing start fresh
			sampleStoreFile.delete();
		}
		sampleStore = new SampleStore(sampleStoreFile);
		sampleStoreFile.deleteOnExit();

		issueByTypeOrg(organization);

		// SampleUser can be any implementation that implements
		// org.hyperledger.fabric.sdk.User Interface

	}

	private static void issueByTypeOrg(String org)
			throws MalformedURLException, EnrollmentException, InvalidArgumentException {

		if (org.equals("org1")) {
			client = HFCAClient.createNewInstance(
					testConfig.getIntegrationTestsSampleOrg(TEST_WITH_INTEGRATION_ORG1).getCALocation(),
					testConfig.getIntegrationTestsSampleOrg(TEST_WITH_INTEGRATION_ORG1).getCAProperties());
			client.setCryptoSuite(crypto);

			admin = sampleStore.getMember(TEST_ADMIN_NAME, TEST_ADMIN_ORG1);
			if (!admin.isEnrolled()) { // Preregistered admin only needs to be enrolled with Fabric CA.
				admin.setEnrollment(client.enroll(admin.getName(), TEST_ADMIN_PW));
			}
		} else {
			client = HFCAClient.createNewInstance(
					testConfig.getIntegrationTestsSampleOrg(TEST_WITH_INTEGRATION_ORG2).getCALocation(),
					testConfig.getIntegrationTestsSampleOrg(TEST_WITH_INTEGRATION_ORG2).getCAProperties());
			client.setCryptoSuite(crypto);

			admin = sampleStore.getMember(TEST_ADMIN_NAME, TEST_ADMIN_ORG2);
			if (!admin.isEnrolled()) { // Preregistered admin only needs to be enrolled with Fabric CA.
				admin.setEnrollment(client.enroll(admin.getName(), TEST_ADMIN_PW));
			}

		}

	}

	// Tests attributes
	public static String issueCert(String username, String role, String organization, String phoneNumber,
			String nationID) throws Exception {

		init();
		setup(organization);
		if (organization.equals("org1")) {
			user = new SampleUser(username, TEST_ADMIN_ORG1, sampleStore, crypto);

			rr = new RegistrationRequest(user.getName(), TEST_USER1_AFFILIATION);
		} else {
			user = new SampleUser(username, TEST_ADMIN_ORG2, sampleStore, crypto);

			rr = new RegistrationRequest(user.getName(), TEST_USER2_AFFILIATION);
		}

		rr.setType(role);
		String password = "mrAttributespassword";
		rr.setSecret(password);

		rr.addAttribute(new Attribute("NationalID", nationID));
		rr.addAttribute(new Attribute("Phone Number", phoneNumber));
		rr.addAttribute(new Attribute("testattrDEFAULTATTR", "mrAttributesValueDEFAULTATTR", true));
		user.setEnrollmentSecret(client.register(rr, admin));
		if (!user.getEnrollmentSecret().equals(password)) {
			fail("Secret returned from RegistrationRequest not match : " + user.getEnrollmentSecret());
		}
		EnrollmentRequest req = new EnrollmentRequest();
		req.addAttrReq("testattr2").setOptional(true);

		user.setEnrollment(client.enroll(user.getName(), user.getEnrollmentSecret(), req));

		Enrollment enrollment = user.getEnrollment();
		PrivateKey privateKey = enrollment.getKey();
//        String cert1 = enrollment.getCert();

		X509Certificate cert = getCert(enrollment.getCert().getBytes());
//		System.out.print(cert);
		String certString = cert.toString();
//        String certdec = getStringCert(cert);
		Util.writex509(cert, user.getName());
		Util.writePrivateKey(privateKey, user.getName());
//		System.out.println(
//				"We issue Certificate already!" + "\n" + cert.getPublicKey() + " " + " private : " + privateKey);

		return certString;
	}

	public void importCert() throws CertificateException, InvalidArgumentException, InfoException,
			org.hyperledger.fabric.sdk.exception.InvalidArgumentException, NetworkConfigurationException, IOException,
			InvalidKeyException, NoSuchAlgorithmException, NoSuchProviderException, SignatureException {

		X509Certificate certTest = Util.readx509(rootCADir + ".pem");
		System.out.println("Public key: " + certTest.getPublicKey());
		System.out.println("Issuer: " + certTest.getIssuerX500Principal().getName());

		// ECert id
		BigInteger serial = certTest.getSerialNumber();

	}

	public void verifyWithCertificateAuthority() throws CertificateException, FileNotFoundException,
			InvalidKeyException, NoSuchAlgorithmException, NoSuchProviderException, SignatureException {

		X509Certificate certTest = Util.readx509(rootCADir + ".pem");
		System.out.println(certTest.getPublicKey());

		certTest.getBasicConstraints();
		X509Certificate issuerCert = null;
		if (org == "org1") {
			issuerCert = Util.readx509(CA_ORG1);
		} else {
			issuerCert = Util.readx509(CA_ORG2);
		}

		certTest.verify(issuerCert.getPublicKey());
	}

	private static final Pattern compile = Pattern.compile(
			"^-----BEGIN CERTIFICATE-----$" + "(.*?)" + "\n-----END CERTIFICATE-----\n",
			Pattern.DOTALL | Pattern.MULTILINE);

	static String getStringCert(String pemFormat) {
		String ret = null;

		final Matcher matcher = compile.matcher(pemFormat);
		if (matcher.matches()) {
			final String base64part = matcher.group(1).replaceAll("\n", "");
			Base64.Decoder b64dec = Base64.getDecoder();
			ret = new String(b64dec.decode(base64part.getBytes(UTF_8)));

		} else {
			fail("Certificate failed to match expected pattern. Certificate:\n" + pemFormat);
		}

		return ret;
	}

	// Tests getting all identities for a caller
	public void testGetAllIdentity() throws Exception {

		Collection<HFCAIdentity> foundIdentities = client.getHFCAIdentities(admin);

		HFCACertificateRequest certReq = client.newHFCACertificateRequest();
		HFCACertificateResponse resp = client.getHFCACertificates(admin, certReq);

		Integer found = 0;

		for (HFCAIdentity id : foundIdentities) {
			System.out.println(id.getEnrollmentId().toString());
		}

	}

	private static int createSuccessfulHCAIdentity(HFCAIdentity ident, User user)
			throws InvalidArgumentException, IdentityException {

		int rc = ident.create(user);
		assertTrue(rc < 400);
		assertNotNull(ident.getSecret());
		assertFalse(ident.getSecret().isEmpty());
		assertNotNull(ident.getEnrollmentId());
		assertFalse(ident.getEnrollmentId().isEmpty());
		assertNotNull(ident.getType());
		assertFalse(ident.getType().isEmpty());

		return rc;
	}

	private HFCAIdentity getIdentityReq(String enrollmentID, String type) throws InvalidArgumentException {
		String password = "password";

		HFCAIdentity ident = client.newHFCAIdentity(enrollmentID);
		ident.setSecret(password);
		ident.setAffiliation(TEST_USER1_AFFILIATION);
		ident.setMaxEnrollments(1);
		ident.setType(type);

		Collection<Attribute> attributes = new ArrayList<Attribute>();
		attributes.add(new Attribute("testattr1", "valueattr1"));
		ident.setAttributes(attributes);
		return ident;
	}

	// Tests getting certificates
//    @Test
//    public void testGetCertificates() throws Exception {
//
//        if (testConfig.isRunningAgainstFabric10()) {
//            return;
//        }
//
//        HFCACertificateRequest certReq = client.newHFCACertificateRequest();
//
//        SampleUser admin2 = sampleStore.getMember("admin2", "org2.department1");
//        RegistrationRequest rr = new RegistrationRequest(admin2.getName(), "org2.department1");
//        String password = "password";
//        rr.setSecret(password);
//        rr.addAttribute(new Attribute("hf.Registrar.Roles", "client,peer,user"));
//
//        client.register(rr, admin);
//        admin2.setEnrollment(client.enroll(admin2.getName(), password));
//
//        rr = new RegistrationRequest("testUser", "org2.department1");
//        rr.setSecret(password);
//        client.register(rr, admin);
//        Enrollment enroll = client.enroll("testUser", password);
//
//        // Get all certificates that 'admin2' is allowed to see because no attributes are set
//        // in the certificate request. This returns 2 certificates, one certificate for the caller
//        // itself 'admin2' and the other certificate for 'testuser2'. These are the only two users
//        // that fall under the caller's affiliation of 'org2.department1'.
//        HFCACertificateResponse resp = client.getHFCACertificates(admin2, certReq);
//        assertEquals(2, resp.getCerts().size());
//        assertTrue(resultContains(resp.getCerts(), new String[] {"admin", "testUser"}));
//
//        // Get certificate for a specific enrollment id
//        certReq.setEnrollmentID("admin2");
//        resp = client.getHFCACertificates(admin, certReq);
//        assertEquals(1, resp.getCerts().size());
//        assertTrue(resultContains(resp.getCerts(), new String[] {"admin"}));
//
//        // Get certificate for a specific serial number
//        certReq = client.newHFCACertificateRequest();
//        X509Certificate cert = getCert(enroll.getCert().getBytes());
//        String serial = cert.getSerialNumber().toString(16);
//        certReq.setSerial(serial);
//        resp = client.getHFCACertificates(admin, certReq);
//        assertEquals(1, resp.getCerts().size());
//        assertTrue(resultContains(resp.getCerts(), new String[] {"testUser"}));
//
//        // Get certificate for a specific AKI
//        certReq = client.newHFCACertificateRequest();
//        String oid = Extension.authorityKeyIdentifier.getId();
//        byte[] extensionValue = cert.getExtensionValue(oid);
//        ASN1OctetString aki0c = ASN1OctetString.getInstance(extensionValue);
//        AuthorityKeyIdentifier aki = AuthorityKeyIdentifier.getInstance(aki0c.getOctets());
//        String aki2 = DatatypeConverter.printHexBinary(aki.getKeyIdentifier());
//        certReq.setAki(aki2);
//        resp = client.getHFCACertificates(admin2, certReq);
//        assertEquals(2, resp.getCerts().size());
//
//        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
//
//        // Get certificates that expired before a specific date
//        // In this case, using a really old date should return 0 certificates
//        certReq = client.newHFCACertificateRequest();
//        certReq.setExpiredEnd(formatter.parse("2014-30-31"));
//        resp = client.getHFCACertificates(admin, certReq);
//        assertEquals(0, resp.getCerts().size());
//
//        // Get certificates that expired before a specific date
//        // In this case, using a date far into the future should return all certificates
//        certReq = client.newHFCACertificateRequest();
//        Calendar cal = Calendar.getInstance();
//        Date date = new Date();
//        cal.setTime(date);
//        cal.add(Calendar.YEAR, 20);
//        date = cal.getTime();
//        certReq.setExpiredEnd(date);
//        resp = client.getHFCACertificates(admin2, certReq);
//        assertEquals(2, resp.getCerts().size());
//        assertTrue(resultContains(resp.getCerts(), new String[] {"admin2", "testUser"}));
//
//        // Get certificates that expired after specific date
//        // In this case, using a really old date should return all certificates that the caller is
//        // allowed to see because they all have a future expiration date
//        certReq = client.newHFCACertificateRequest();
//        certReq.setExpiredStart(formatter.parse("2014-03-31"));
//        resp = client.getHFCACertificates(admin2, certReq);
//        assertEquals(2, resp.getCerts().size());
//
//        // Get certificates that expired after specified date
//        // In this case, using a date far into the future should return zero certificates
//        certReq = client.newHFCACertificateRequest();
//        certReq.setExpiredStart(date);
//        resp = client.getHFCACertificates(admin, certReq);
//        assertEquals(0, resp.getCerts().size());
//
//        client.revoke(admin, "testUser", "baduser");
//
//        // Get certificates that were revoked after specific date
//        certReq = client.newHFCACertificateRequest();
//        certReq.setRevokedStart(formatter.parse("2014-03-31"));
//        resp = client.getHFCACertificates(admin2, certReq);
//        assertEquals(1, resp.getCerts().size());
//
//        certReq = client.newHFCACertificateRequest();
//        certReq.setRevokedEnd(formatter.parse("2014-03-31"));
//        resp = client.getHFCACertificates(admin2, certReq);
//        assertEquals(0, resp.getCerts().size());
//
//        certReq = client.newHFCACertificateRequest();
//        certReq.setRevoked(false);
//        resp = client.getHFCACertificates(admin2, certReq);
//        assertEquals(1, resp.getCerts().size());
//        assertTrue(resultContains(resp.getCerts(), new String[] {"admin2"}));
//        assertFalse(resultContains(resp.getCerts(), new String[] {"testUser"}));
//
//        certReq = client.newHFCACertificateRequest();
//        certReq.setRevoked(true);
//        resp = client.getHFCACertificates(admin2, certReq);
//        assertTrue(resultContains(resp.getCerts(), new String[] {"admin2", "testUser"}));
//        assertEquals(2, resp.getCerts().size());
//
//        certReq = client.newHFCACertificateRequest();
//        certReq.setExpired(false);
//        resp = client.getHFCACertificates(admin2, certReq);
//        assertEquals(2, resp.getCerts().size());
//    }

	private boolean resultContains(Collection<HFCACredential> creds, String[] names) {
		int numFound = 0;
		for (HFCACredential cred : creds) {
			for (int i = 0; i < names.length; i++) {
				HFCAX509Certificate cert = (HFCAX509Certificate) cred;
				if (cert.getX509().getSubjectDN().toString().contains(names[i])) {
					numFound++;
					break;
				}
			}
		}
		if (numFound == names.length) {
			return true;
		}
		return false;
	}

	private static X509Certificate getCert(byte[] certBytes) throws CertificateException {
		BufferedInputStream pem = new BufferedInputStream(new ByteArrayInputStream(certBytes));
		CertificateFactory certFactory = CertificateFactory.getInstance("X509");
		X509Certificate certificate = (X509Certificate) certFactory.generateCertificate(pem);
		return certificate;
	}

}
