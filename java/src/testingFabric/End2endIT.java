/*
 *  Copyright 2016, 2017 DTCC, Fujitsu Australia Software Technology, IBM - All Rights Reserved.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *    http://www.apache.org/licenses/LICENSE-2.0
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package testingFabric;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.nio.file.Paths;
import java.security.GeneralSecurityException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Collection;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Properties;
import java.util.Random;
import java.util.Set;
import java.util.Vector;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.codec.binary.Hex;
import org.hyperledger.fabric.protos.ledger.rwset.kvrwset.KvRwset;
import org.hyperledger.fabric.sdk.BlockEvent;
import org.hyperledger.fabric.sdk.BlockInfo;
import org.hyperledger.fabric.sdk.BlockchainInfo;
import org.hyperledger.fabric.sdk.ChaincodeEndorsementPolicy;
import org.hyperledger.fabric.sdk.ChaincodeEvent;
import org.hyperledger.fabric.sdk.ChaincodeID;
import org.hyperledger.fabric.sdk.Channel;
import org.hyperledger.fabric.sdk.ChannelConfiguration;
import org.hyperledger.fabric.sdk.Enrollment;
import org.hyperledger.fabric.sdk.HFClient;
import org.hyperledger.fabric.sdk.InstallProposalRequest;
import org.hyperledger.fabric.sdk.InstantiateProposalRequest;
import org.hyperledger.fabric.sdk.Orderer;
import org.hyperledger.fabric.sdk.Peer;
import org.hyperledger.fabric.sdk.Peer.PeerRole;
import org.hyperledger.fabric.sdk.ProposalResponse;
import org.hyperledger.fabric.sdk.QueryByChaincodeRequest;
import org.hyperledger.fabric.sdk.SDKUtils;
import testingFabric.TestConfigHelper;
import org.hyperledger.fabric.sdk.TransactionInfo;
import org.hyperledger.fabric.sdk.TransactionProposalRequest;
import org.hyperledger.fabric.sdk.TransactionRequest.Type;
import org.hyperledger.fabric.sdk.TxReadWriteSetInfo;
import org.hyperledger.fabric.sdk.User;
import org.hyperledger.fabric.sdk.BlockEvent.TransactionEvent;
import org.hyperledger.fabric.sdk.exception.CryptoException;
import org.hyperledger.fabric.sdk.exception.InvalidArgumentException;
import org.hyperledger.fabric.sdk.exception.InvalidProtocolBufferRuntimeException;
import org.hyperledger.fabric.sdk.exception.ProposalException;
import org.hyperledger.fabric.sdk.exception.TransactionEventException;
import org.hyperledger.fabric.sdk.security.CryptoSuite;
import testingFabric.TestConfig;

import org.hyperledger.fabric_ca.sdk.Attribute;
import org.hyperledger.fabric_ca.sdk.EnrollmentRequest;
import org.hyperledger.fabric_ca.sdk.HFCAClient;
import org.hyperledger.fabric_ca.sdk.HFCAIdentity;
import org.hyperledger.fabric_ca.sdk.HFCAInfo;
import org.hyperledger.fabric_ca.sdk.RegistrationRequest;
import org.hyperledger.fabric_ca.sdk.exception.EnrollmentException;
import org.hyperledger.fabric_ca.sdk.exception.IdentityException;
import org.junit.Before;
import org.junit.Test;

import com.google.common.collect.Iterables;

import main.java.org.app.client.ChannelClient;
import main.java.org.app.client.FabricClient;
import main.java.org.app.config.Config;
import main.java.org.app.user.UserContext;

import static java.lang.String.format;
import static java.nio.charset.StandardCharsets.UTF_8;
import static org.hyperledger.fabric.sdk.BlockInfo.EnvelopeType.TRANSACTION_ENVELOPE;
import static org.hyperledger.fabric.sdk.Channel.NOfEvents.createNofEvents;
import static org.hyperledger.fabric.sdk.Channel.PeerOptions.createPeerOptions;
import static org.hyperledger.fabric.sdk.Channel.TransactionOptions.createTransactionOptions;
import static testingFabric.TestUtils.getPEMStringFromPrivateKey;
import static testingFabric.TestUtils.resetConfig;
import static testingFabric.TestUtils.testRemovingAddingPeersOrderers;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Test end to end scenario
 */
public class End2endIT {

	protected static final TestConfig testConfig = TestConfig.getConfig();
	static final String TEST_ADMIN_NAME = "admin";
	protected static final String TEST_FIXTURES_PATH = "/home/dara/Documents/fabric-java-sdk/src/test/fixture";

	protected static Random random = new Random();

	protected static final String FOO_CHANNEL_NAME = "foo";
	private static final String BAR_CHANNEL_NAME = "bar";
	String user = "mrAttributes214";
	int found = 0;

	protected static final int DEPLOYWAITTIME = testConfig.getDeployWaitTime();

	protected static final byte[] EXPECTED_EVENT_DATA = "!".getBytes(UTF_8);
	protected static final String EXPECTED_EVENT_NAME = "event";
	private static final Map<String, String> TX_EXPECTED;

	static String testName = "End2endJavaIT";

	String CHAIN_CODE_FILEPATH = "sdkintegration/gocc/sample1";
	String CHAIN_CODE_NAME = "example_cc_go";
	String CHAIN_CODE_PATH = "github.com/example_cc";
	String CHAIN_CODE_VERSION = "1";
//    String CHAIN_CODE_FILEPATH = "sdkintegration/javacc/sample1"; //override path to Node code
//    String CHAIN_CODE_PATH = null; //This is used only for GO.
//    String CHAIN_CODE_NAME = "example_cc_java"; // chaincode name.
//    Type CHAIN_CODE_LANG = Type.JAVA; //language is Java.
	static Type CHAIN_CODE_LANG = Type.GO_LANG;
	String password = "password";
	private static final String TEST_USER1_AFFILIATION = "org1.department1";
	HFCAClient client1;
	private static final String TEST_WITH_INTEGRATION_ORG = "peerOrg1";
	private static CryptoSuite crypto;
	protected SampleUser admin;
	static Set<String> roles;

	static {
		TX_EXPECTED = new HashMap<>();
		TX_EXPECTED.put("readset1", "Missing readset for channel bar block 1");
		TX_EXPECTED.put("writeset1", "Missing writeset for channel bar block 1");
	}

	private final static TestConfigHelper configHelper = new TestConfigHelper();
	String testTxID = null; // save the CC invoke TxID and use in queries
	SampleStore sampleStore = null;
	protected static Collection<SampleOrg> testSampleOrgs;
	static String doctorOrg1 = "Dr.David";
	static String doctorOrg2 = "Dr.Alaska";

	static void out(String format, Object... args) {

		System.err.flush();
		System.out.flush();

		System.out.println(format(format, args));
		System.err.flush();
		System.out.flush();

	}
	// CHECKSTYLE.ON: Method length is 320 lines (max allowed is 150).

	static String printableString(final String string) {
		int maxLogStringLength = 64;
		if (string == null || string.length() == 0) {
			return string;
		}

		String ret = string.replaceAll("[^\\p{Print}]", "?");

		ret = ret.substring(0, Math.min(ret.length(), maxLogStringLength))
				+ (ret.length() > maxLogStringLength ? "..." : "");

		return ret;

	}

	public static void checkConfig()
			throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException,
			MalformedURLException, org.hyperledger.fabric_ca.sdk.exception.InvalidArgumentException {
		out("\n\n\nRUNNING: %s.\n", testName);
		// configHelper.clearConfig();
		// assertEquals(256, Config.getConfig().getSecurityLevel());
		resetConfig();
		configHelper.customizeConfig();

		testSampleOrgs = testConfig.getIntegrationTestsSampleOrgs();
		// Set up hfca for each sample org

		for (SampleOrg sampleOrg : testSampleOrgs) {
			String caName = sampleOrg.getCAName(); // Try one of each name and no name.
			if (caName != null && !caName.isEmpty()) {
				sampleOrg.setCAClient(
						HFCAClient.createNewInstance(caName, sampleOrg.getCALocation(), sampleOrg.getCAProperties()));
			} else {
				sampleOrg.setCAClient(
						HFCAClient.createNewInstance(sampleOrg.getCALocation(), sampleOrg.getCAProperties()));
			}
		}
	}

	static Map<String, Properties> clientTLSProperties = new HashMap<>();

	File sampleStoreFile = new File(System.getProperty("java.io.tmpdir") + "/HFCSampletest.properties");

	@Test
	public void setup() throws Exception {
		checkConfig();
		if (sampleStoreFile.exists()) { // For testing start fresh
			sampleStoreFile.delete();
		}

		roles = new HashSet<String>();
		roles.add("medical doctor");
		sampleStore = new SampleStore(sampleStoreFile);

//		queryListIdentity(sampleStore);
		enrollUsersSetup(sampleStore); // This enrolls users with fabric ca and setups sample store to get users later.
		runFabricTest(sampleStore); // Runs Fabric tests with constructing channels, joining peers, exercising
									// chaincode

	}

	/**
	 * Will register and enroll users persisting them to samplestore.
	 *
	 * @param sampleStore
	 * @throws Exception
	 */
	public static void enrollUsersSetup(SampleStore sampleStore) throws Exception {

		out("***** Enrolling Users *****");
		for (SampleOrg sampleOrg : testSampleOrgs) {

			HFCAClient ca = sampleOrg.getCAClient();

			final String orgName = sampleOrg.getName();
			final String mspid = sampleOrg.getMSPID();
			ca.setCryptoSuite(CryptoSuite.Factory.getCryptoSuite());

			if (testConfig.isRunningFabricTLS()) {
				// This shows how to get a client TLS certificate from Fabric CA
				// we will use one client TLS certificate for orderer peers etc.
				final EnrollmentRequest enrollmentRequestTLS = new EnrollmentRequest();
				enrollmentRequestTLS.addHost("localhost");
				enrollmentRequestTLS.setProfile("tls");
				final Enrollment enroll = ca.enroll("admin", "adminpw", enrollmentRequestTLS);
				final String tlsCertPEM = enroll.getCert();
				final String tlsKeyPEM = getPEMStringFromPrivateKey(enroll.getKey());

				final Properties tlsProperties = new Properties();

				tlsProperties.put("clientKeyBytes", tlsKeyPEM.getBytes(UTF_8));
				tlsProperties.put("clientCertBytes", tlsCertPEM.getBytes(UTF_8));
				clientTLSProperties.put(sampleOrg.getName(), tlsProperties);
				// Save in samplestore for follow on tests.
				sampleStore.storeClientPEMTLCertificate(sampleOrg, tlsCertPEM);
				sampleStore.storeClientPEMTLSKey(sampleOrg, tlsKeyPEM);
			}

			HFCAInfo info = ca.info(); // just check if we connect at all.
			assertNotNull(info);
			String infoName = info.getCAName();
			if (infoName != null && !infoName.isEmpty()) {
				assertEquals(ca.getCAName(), infoName);
			}

			SampleUser admin = sampleStore.getMember(TEST_ADMIN_NAME, orgName);
			if (!admin.isEnrolled()) { // Preregistered admin only needs to be enrolled with Fabric caClient.
				admin.setEnrollment(ca.enroll(admin.getName(), "adminpw"));
				admin.setRoles(roles);
				admin.setMspId(mspid);
			}

			final String sampleOrgName = sampleOrg.getName();
			final String sampleOrgDomainName = sampleOrg.getDomainName();

			SampleUser peerOrgAdmin = sampleStore.getMember(sampleOrgName + "Admin", sampleOrgName,
					sampleOrg.getMSPID(),
					Util.findFileSk(Paths
							.get(testConfig.getTestChannelPath(), "crypto-config/peerOrganizations/",
									sampleOrgDomainName, format("/users/Admin@%s/msp/keystore", sampleOrgDomainName))
							.toFile()),
					Paths.get(testConfig.getTestChannelPath(), "crypto-config/peerOrganizations/", sampleOrgDomainName,
							format("/users/Admin@%s/msp/signcerts/Admin@%s-cert.pem", sampleOrgDomainName,
									sampleOrgDomainName))
							.toFile());
			sampleOrg.setPeerAdmin(peerOrgAdmin); // A special user that can create channels, join peers and install
													// chaincode
			sampleOrg.setAdmin(admin); // The admin of this org --

			// add doctor1 to org1 and doctor2 to org2

			if (sampleOrg.getName().equals("peerOrg1")) {
				SampleUser user = sampleStore.getMember(doctorOrg1, sampleOrg.getName());
				if (!user.isRegistered()) { // users need to be registered AND enrolled
					RegistrationRequest rr = new RegistrationRequest(user.getName(), "org1.department1");
					rr.addAttribute(new Attribute("role", "doctor"));
					user.setEnrollmentSecret(ca.register(rr, admin));
				}
				if (!user.isEnrolled()) {
					user.setEnrollment(ca.enroll(user.getName(), user.getEnrollmentSecret()));
					user.setMspId(mspid);
				}

				Enrollment enrollment = user.getEnrollment();
				String cert = enrollment.getCert();
				String certdec = getStringCert(cert);

				writeECert(doctorOrg1, certdec);
				sampleOrg.addUser(user);
			} else {
				SampleUser user = sampleStore.getMember(doctorOrg2, sampleOrg.getName());
				if (!user.isRegistered()) { // users need to be registered AND enrolled
					RegistrationRequest rr = new RegistrationRequest(user.getName(), "org2.department1");
					rr.addAttribute(new Attribute("role", "doctor"));
					user.setEnrollmentSecret(ca.register(rr, admin));
				}
				if (!user.isEnrolled()) {
					user.setEnrollment(ca.enroll(user.getName(), user.getEnrollmentSecret()));
					user.setMspId(mspid);
				}

				Enrollment enrollment = user.getEnrollment();
				String cert = enrollment.getCert();
				String certdec = getStringCert(cert);

				writeECert(doctorOrg2, certdec);
				sampleOrg.addUser(user);

			}

//			System.out.println("This " + testUser1 + " register to " + sampleOrgName + " successfully!");
		}
	}

	public void runFabricTest(final SampleStore sampleStore) throws Exception {

		////////////////////////////
		// Setup client

		// Create instance of client.
		HFClient client = HFClient.createNewInstance();

		client.setCryptoSuite(CryptoSuite.Factory.getCryptoSuite());

		////////////////////////////
		// Construct and run the channels
		Collection<SampleOrg> sample = testConfig.getIntegrationTestsSampleOrgs();
		SampleOrg sampleOrg = testConfig.getIntegrationTestsSampleOrg("peerOrg1");
		Channel fooChannel = constructChannel(FOO_CHANNEL_NAME, client, sampleOrg);
		sampleStore.saveChannel(fooChannel);
		instantiateBarble(client, fooChannel, true, sample, 0);

		assertFalse(fooChannel.isShutdown());
		fooChannel.shutdown(true); // Force foo channel to shutdown clean up resources.
		assertTrue(fooChannel.isShutdown());

		assertNull(client.getChannel(FOO_CHANNEL_NAME));
		out("\n");

		out("That's all folks!");
	}

	/**
	 * Will register and enroll users persisting them to samplestore.
	 *
	 * @param sampleStore
	 * @throws Exception
	 */
	public void enrollUsersSetup1(SampleStore sampleStore) throws Exception {

		out("***** Enrolling Users *****");
		for (SampleOrg sampleOrg : testSampleOrgs) {

			HFCAClient ca = HFCAClient.createNewInstance(
					testConfig.getIntegrationTestsSampleOrg(sampleOrg.getName()).getCALocation(),
					testConfig.getIntegrationTestsSampleOrg(sampleOrg.getName()).getCAProperties());

			final String orgName = sampleOrg.getName();
			final String mspid = sampleOrg.getMSPID();
			ca.setCryptoSuite(CryptoSuite.Factory.getCryptoSuite());

			admin = sampleStore.getMember(TEST_ADMIN_NAME, orgName);
			if (!admin.isEnrolled()) { // Preregistered admin only needs to be enrolled with Fabric caClient.
				admin.setEnrollment(ca.enroll(admin.getName(), "adminpw"));
				admin.setRoles(roles);
				admin.setMspId(mspid);
			}

			if (testConfig.isRunningFabricTLS()) {
				// This shows how to get a client TLS certificate from Fabric CA
				// we will use one client TLS certificate for orderer peers etc.
				final EnrollmentRequest enrollmentRequestTLS = new EnrollmentRequest();
				enrollmentRequestTLS.addHost("localhost");
				enrollmentRequestTLS.setProfile("tls");
				final Enrollment enroll = ca.enroll("admin", "adminpw", enrollmentRequestTLS);
				final String tlsCertPEM = enroll.getCert();
				final String tlsKeyPEM = getPEMStringFromPrivateKey(enroll.getKey());

				final Properties tlsProperties = new Properties();

				tlsProperties.put("clientKeyBytes", tlsKeyPEM.getBytes(UTF_8));
				tlsProperties.put("clientCertBytes", tlsCertPEM.getBytes(UTF_8));
				clientTLSProperties.put(sampleOrg.getName(), tlsProperties);
				// Save in samplestore for follow on tests.
				sampleStore.storeClientPEMTLCertificate(sampleOrg, tlsCertPEM);
				sampleStore.storeClientPEMTLSKey(sampleOrg, tlsKeyPEM);
			}

			Collection<HFCAIdentity> foundIdentities = ca.getHFCAIdentities(admin);
			String[] expectedIdenities = new String[] { doctorOrg1 };
			Integer found = 0;
//
			for (HFCAIdentity id : foundIdentities) {
				for (String name : expectedIdenities) {
					System.out.println(sampleOrg.getName() + " " + id.getEnrollmentId() + " " + id.getAffiliation()
							+ " " + " " + id.getType());
					if (id.getEnrollmentId().equals(name)) {
						found++;
					}
				}
			}
//            
			if (found == 0) {
				SampleUser user = new SampleUser(doctorOrg1, orgName, sampleStore, crypto);
				RegistrationRequest rr = new RegistrationRequest(user.getName(), "org1.department1");
				rr.setSecret(password);
				user.setEnrollmentSecret(ca.register(rr, admin));

				if (!user.getEnrollmentSecret().equals(password)) {
					fail("Secret returned from RegistrationRequest not match : " + user.getEnrollmentSecret());
				}

				EnrollmentRequest req = new EnrollmentRequest();
				user.setEnrollment(ca.enroll(user.getName(), user.getEnrollmentSecret()));
				user.setMspId(mspid);

				Enrollment enrollment = user.getEnrollment();
				String cert = enrollment.getCert();
				String certdec = getStringCert(cert);

				writeECert(doctorOrg1, certdec);
				final String sampleOrgName = sampleOrg.getName();
				final String sampleOrgDomainName = sampleOrg.getDomainName();

				SampleUser peerOrgAdmin = sampleStore.getMember(sampleOrgName + "Admin", sampleOrgName,
						sampleOrg.getMSPID(),
						Util.findFileSk(Paths.get(testConfig.getTestChannelPath(), "crypto-config/peerOrganizations/",
								sampleOrgDomainName, format("/users/Admin@%s/msp/keystore", sampleOrgDomainName))
								.toFile()),
						Paths.get(testConfig.getTestChannelPath(), "crypto-config/peerOrganizations/",
								sampleOrgDomainName, format("/users/Admin@%s/msp/signcerts/Admin@%s-cert.pem",
										sampleOrgDomainName, sampleOrgDomainName))
								.toFile());
				sampleOrg.setPeerAdmin(peerOrgAdmin); // A special user that can create channels, join peers and install
														// chaincode
				sampleOrg.setAdmin(admin); // The admin of this org --
				sampleOrg.addUser(user);
			}
		}
	}

	void queryListIdentity(SampleStore samplestore)
			throws IllegalAccessException, InstantiationException, ClassNotFoundException, CryptoException,
			InvalidArgumentException, NoSuchMethodException, InvocationTargetException, EnrollmentException,
			org.hyperledger.fabric_ca.sdk.exception.InvalidArgumentException, IdentityException, IOException,
			NoSuchAlgorithmException, NoSuchProviderException, InvalidKeySpecException {

		out("***** Enrolling Users *****");
		for (SampleOrg sampleOrg : testSampleOrgs) {

			HFCAClient ca = HFCAClient.createNewInstance(
					testConfig.getIntegrationTestsSampleOrg(sampleOrg.getName()).getCALocation(),
					testConfig.getIntegrationTestsSampleOrg(sampleOrg.getName()).getCAProperties());

			final String orgName = sampleOrg.getName();
			final String mspid = sampleOrg.getMSPID();
			ca.setCryptoSuite(CryptoSuite.Factory.getCryptoSuite());

			admin = samplestore.getMember(TEST_ADMIN_NAME, orgName);
			if (!admin.isEnrolled()) { // Preregistered admin only needs to be enrolled with Fabric caClient.
				admin.setEnrollment(ca.enroll(admin.getName(), "adminpw"));
				admin.setRoles(roles);
				admin.setMspId(mspid);
			}

			Collection<HFCAIdentity> foundIdentities = ca.getHFCAIdentities(admin);
			String[] expectedIdenities = new String[] { user };

			for (HFCAIdentity id : foundIdentities) {
				for (String name : expectedIdenities) {
					System.out.println(sampleOrg.getName() + " " + id.getEnrollmentId() + " " + id.getAffiliation()
							+ " " + " " + id.getType());
					if (id.getEnrollmentId().equals(name)) {
						found++;
					}
				}
			}

			if (found != 0) {

				System.out.println("This " + user + " has already register!");

			}

			String directoryPath = "usersTest";
			String privatekeyfile = directoryPath + "/" + doctorOrg1 + "_sk" + ".pem";
			String publickeyfile = directoryPath + "/" + doctorOrg1 + ".pem";

//			SampleUser user = samplestore.getMember(testUser1, sampleOrg.getName());
			SampleUser user = null;
			try {
				user = samplestore.getMemberEdition(doctorOrg1, sampleOrg.getName(), mspid, privatekeyfile,
						publickeyfile);
			} catch (GeneralSecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			final String sampleOrgName = sampleOrg.getName();
			final String sampleOrgDomainName = sampleOrg.getDomainName();

			SampleUser peerOrgAdmin = samplestore.getMember(sampleOrgName + "Admin", sampleOrgName,
					sampleOrg.getMSPID(),
					Util.findFileSk(Paths
							.get(testConfig.getTestChannelPath(), "crypto-config/peerOrganizations/",
									sampleOrgDomainName, format("/users/Admin@%s/msp/keystore", sampleOrgDomainName))
							.toFile()),
					Paths.get(testConfig.getTestChannelPath(), "crypto-config/peerOrganizations/", sampleOrgDomainName,
							format("/users/Admin@%s/msp/signcerts/Admin@%s-cert.pem", sampleOrgDomainName,
									sampleOrgDomainName))
							.toFile());
			sampleOrg.setPeerAdmin(peerOrgAdmin); // A special user that can create channels, join peers and install
			sampleOrg.addUser(user); // chaincode
			sampleOrg.setAdmin(admin); // The admin of this org --
		}

	}

	public static void writeECert(String user, String cert) throws Exception {
		String directoryPath = "users/" + user;
		String filePath = directoryPath + "/" + user + ".ser";
		File directory = new File(directoryPath);
		if (!directory.exists())
			directory.mkdirs();

		FileOutputStream file = new FileOutputStream(filePath);
		ObjectOutputStream out = new ObjectOutputStream(file);

		// Method for serialization of object
		out.writeObject(cert);

		out.close();
		file.close();
	}

	private static final Pattern compile = Pattern.compile(
			"^-----BEGIN CERTIFICATE-----$" + "(.*?)" + "\n-----END CERTIFICATE-----\n",
			Pattern.DOTALL | Pattern.MULTILINE);

	static String getStringCert(String pemFormat) {
		String ret = null;

		final Matcher matcher = compile.matcher(pemFormat);
		if (matcher.matches()) {
			final String base64part = matcher.group(1).replaceAll("\n", "");
			Base64.Decoder b64dec = Base64.getDecoder();
			ret = new String(b64dec.decode(base64part.getBytes(UTF_8)));

		} else {
			fail("Certificate failed to match expected pattern. Certificate:\n" + pemFormat);
		}

		return ret;
	}

	static Map<String, Long> expectedMoveRCMap = new HashMap<>(); // map from channel name to move chaincode's return
																	// code.

	void instantiateBarble(HFClient client, Channel channel, boolean installChaincode, Collection<SampleOrg> sampleOrg,
			int delta) {

		class ChaincodeEventCapture { // A test class to capture chaincode events
			final String handle;
			final BlockEvent blockEvent;
			final ChaincodeEvent chaincodeEvent;

			ChaincodeEventCapture(String handle, BlockEvent blockEvent, ChaincodeEvent chaincodeEvent) {
				this.handle = handle;
				this.blockEvent = blockEvent;
				this.chaincodeEvent = chaincodeEvent;
			}
		}

		Vector<ChaincodeEventCapture> chaincodeEvents = new Vector<>(); // Test list to capture chaincode events.

		try {

			final String channelName = channel.getName();
			boolean isFooChain = FOO_CHANNEL_NAME.equals(channelName);
			out("Running channel %s", channelName);

			Collection<Orderer> orderers = channel.getOrderers();
			System.out.print("Orderer" + orderers.toString());
			final ChaincodeID chaincodeID;
			Collection<ProposalResponse> responses;
			Collection<ProposalResponse> successful = new LinkedList<>();
			Collection<ProposalResponse> failed = new LinkedList<>();

			// Register a chaincode event listener that will trigger for any chaincode id
			// and only for EXPECTED_EVENT_NAME event.

			String chaincodeEventListenerHandle = channel.registerChaincodeEventListener(Pattern.compile(".*"),
					Pattern.compile(Pattern.quote(EXPECTED_EVENT_NAME)), (handle, blockEvent, chaincodeEvent) -> {

						chaincodeEvents.add(new ChaincodeEventCapture(handle, blockEvent, chaincodeEvent));

						String es = blockEvent.getPeer() != null ? blockEvent.getPeer().getName() : "peer was null!!!";
						out("RECEIVED Chaincode event with handle: %s, chaincode Id: %s, chaincode event name: %s, "
								+ "transaction id: %s, event payload: \"%s\", from event source: %s", handle,
								chaincodeEvent.getChaincodeId(), chaincodeEvent.getEventName(),
								chaincodeEvent.getTxId(), new String(chaincodeEvent.getPayload()), es);

					});

			// For non foo channel unregister event listener to test events are not called.
			if (!isFooChain) {
				channel.unregisterChaincodeEventListener(chaincodeEventListenerHandle);
				chaincodeEventListenerHandle = null;

			}

			// Deprecated use v2.0 Lifecycle chaincode management.
			ChaincodeID.Builder chaincodeIDBuilder = ChaincodeID.newBuilder().setName(Config.CHAINCODE_2_NAME)
					.setVersion(CHAIN_CODE_VERSION);
			if (null != Config.CHAINCODE_2_PATH) {
				chaincodeIDBuilder.setPath(Config.CHAINCODE_2_PATH);

			}
			chaincodeID = chaincodeIDBuilder.build();

			////////////////////////////
			// Install Proposal Request
			//
			for (SampleOrg sampOrg : sampleOrg) {
				client.setUserContext(sampOrg.getPeerAdmin());
				Collection<Peer> peers = new LinkedList<Peer>();

				for (String name : sampOrg.getPeerNames()) {
					// for peer0.org2 location is null
					String peerLocation = sampOrg.getPeerLocation(name);
					Properties peerProperties = testConfig.getPeerProperties(name);
					peers.add(client.newPeer(name, peerLocation, peerProperties));
				}

				if (installChaincode) {

					out("Creating install proposal");

					// Deprecated use v2.0 Lifecycle chaincode management.
					InstallProposalRequest installProposalRequest = client.newInstallProposalRequest();
					installProposalRequest.setChaincodeID(chaincodeID);
					installProposalRequest.setChaincodeVersion(CHAIN_CODE_VERSION);
					installProposalRequest.setChaincodeLanguage(CHAIN_CODE_LANG);

					if (isFooChain) {

						installProposalRequest.setChaincodeInputStream(Util.generateTarGzInputStream(
								(Paths.get(Config.CHAINCODE_ROOT_DIR, "src", Config.CHAINCODE_2_PATH).toFile()),
								Paths.get("src", Config.CHAINCODE_2_PATH).toString()));
					} else {
						// On bar chain install from an input stream.

						// For inputstream if indicies are desired the application needs to make sure
						// the META-INF is provided in the stream.
						// The SDK does not change anything in the stream.

						if (CHAIN_CODE_LANG.equals(Type.GO_LANG)) {

							installProposalRequest.setChaincodeInputStream(Util.generateTarGzInputStream(
									(Paths.get(Config.CHAINCODE_ROOT_DIR, "src", Config.CHAINCODE_2_PATH).toFile()),
									Paths.get("src", Config.CHAINCODE_2_PATH).toString()));
						} else {
							installProposalRequest.setChaincodeInputStream(Util.generateTarGzInputStream(
									(Paths.get(TEST_FIXTURES_PATH, CHAIN_CODE_FILEPATH).toFile()), "src"));
						}
					}

					out("Sending install proposal");

					////////////////////////////
					// only a client from the same org as the peer can issue an install request
					int numInstallProposal = 0;
					// Set<String> orgs = orgPeers.keySet();
					// for (SampleOrg org : testSampleOrgs) {

					numInstallProposal = numInstallProposal + peers.size();
					// Deprecated use v2.0 Lifecycle chaincode management.
					responses = client.sendInstallProposal(installProposalRequest, peers);

					for (ProposalResponse response : responses) {
						if (response.getStatus() == ProposalResponse.Status.SUCCESS) {
							out("Successful install proposal response Txid: %s from peer %s",
									response.getTransactionID(), response.getPeer().getName());
							successful.add(response);
						} else {
							failed.add(response);
						}
					}

					// }
					out("Received %d install proposal responses. Successful+verified: %d . Failed: %d",
							numInstallProposal, successful.size(), failed.size());

					if (failed.size() > 0) {
						ProposalResponse first = failed.iterator().next();
						fail("Not enough endorsers for install :" + successful.size() + ".  " + first.getMessage());
					}
				}
			}

			String[] arg = { "" };

			InstantiateProposalRequest instantiateProposalRequest = client.newInstantiationProposalRequest();
			instantiateProposalRequest.setProposalWaitTime(DEPLOYWAITTIME);
			instantiateProposalRequest.setChaincodeID(chaincodeID);
			instantiateProposalRequest.setChaincodeLanguage(CHAIN_CODE_LANG);
			instantiateProposalRequest.setProposalWaitTime(180000);
			instantiateProposalRequest.setArgs(arg);
			instantiateProposalRequest.setFcn("Init");
			Map<String, byte[]> tm = new HashMap<>();
			tm.put("HyperLedgerFabric", "InstantiateProposalRequest:JavaSDK".getBytes(UTF_8));
			tm.put("method", "InstantiateProposalRequest".getBytes(UTF_8));
			instantiateProposalRequest.setTransientMap(tm);

			/*
			 * policy OR(Org1MSP.member, Org2MSP.member) meaning 1 signature from someone in
			 * either Org1 or Org2 See README.md Chaincode endorsement policies section for
			 * more details.
			 */
			ChaincodeEndorsementPolicy chaincodeEndorsementPolicy = new ChaincodeEndorsementPolicy();
			chaincodeEndorsementPolicy
					.fromYamlFile(new File(TEST_FIXTURES_PATH + "/sdkintegration/chaincodeendorsementpolicy.yaml"));
			instantiateProposalRequest.setChaincodeEndorsementPolicy(chaincodeEndorsementPolicy);

			out("Sending instantiateProposalRequest to all peers with arguments: a and b set to 500 and %s respectively",
					"" + (200 + delta));
			successful.clear();
			failed.clear();

			// Deprecated use v2.0 Lifecycle chaincode management.
			responses = channel.sendInstantiationProposal(instantiateProposalRequest);
//            }
			for (ProposalResponse response : responses) {
				if (response.isVerified() && response.getStatus() == ProposalResponse.Status.SUCCESS) {
					successful.add(response);
					out("Succesful instantiate proposal response Txid: %s from peer %s", response.getTransactionID(),
							response.getPeer().getName());
				} else {
					failed.add(response);
				}
			}
			out("Received %d instantiate proposal responses. Successful+verified: %d . Failed: %d", responses.size(),
					successful.size(), failed.size());
			if (failed.size() > 0) {
				for (ProposalResponse fail : failed) {

					out("Not enough endorsers for instantiate :" + successful.size() + "endorser failed with "
							+ fail.getMessage() + ", on peer" + fail.getPeer());

				}
				ProposalResponse first = failed.iterator().next();
				fail("Not enough endorsers for instantiate :" + successful.size() + "endorser failed with "
						+ first.getMessage() + ". Was verified:" + first.isVerified());
			}
			///////////////
			/// Send instantiate transaction to orderer
			out("Sending instantiateTransaction to orderer");

			CompletableFuture<TransactionEvent> cf = channel.sendTransaction(responses);

			Logger.getLogger(ChannelClient.class.getName()).log(Level.INFO, "Chaincode " + Config.CHAINCODE_2_NAME
					+ " on channel " + channel.getName() + " instantiation " + cf);

			Channel.NOfEvents nOfEvents = createNofEvents();
			if (!channel.getPeers(EnumSet.of(PeerRole.EVENT_SOURCE)).isEmpty()) {
				nOfEvents.addPeers(channel.getPeers(EnumSet.of(PeerRole.EVENT_SOURCE)));
			}
//			}

		} catch (Exception e) {
			out("Caught an exception running channel %s", channel.getName());
			e.printStackTrace();
			fail("Test failed with error : " + e.getMessage());
		}
	}

	// CHECKSTYLE.OFF: Method length is 320 lines (max allowed is 150).
	void runChannel(HFClient client, Channel channel, boolean installChaincode, SampleOrg sampleOrg, int delta) {

		class ChaincodeEventCapture { // A test class to capture chaincode events
			final String handle;
			final BlockEvent blockEvent;
			final ChaincodeEvent chaincodeEvent;

			ChaincodeEventCapture(String handle, BlockEvent blockEvent, ChaincodeEvent chaincodeEvent) {
				this.handle = handle;
				this.blockEvent = blockEvent;
				this.chaincodeEvent = chaincodeEvent;
			}
		}

		// The following is just a test to see if peers and orderers can be added and
		// removed.
		// not pertinent to the code flow.
		testRemovingAddingPeersOrderers(client, channel);

		Vector<ChaincodeEventCapture> chaincodeEvents = new Vector<>(); // Test list to capture chaincode events.

		try {

			final String channelName = channel.getName();
			boolean isFooChain = FOO_CHANNEL_NAME.equals(channelName);
			out("Running channel %s", channelName);

			Collection<Orderer> orderers = channel.getOrderers();
			final ChaincodeID chaincodeID;
			Collection<ProposalResponse> responses;
			Collection<ProposalResponse> successful = new LinkedList<>();
			Collection<ProposalResponse> failed = new LinkedList<>();

			// Register a chaincode event listener that will trigger for any chaincode id
			// and only for EXPECTED_EVENT_NAME event.

			String chaincodeEventListenerHandle = channel.registerChaincodeEventListener(Pattern.compile(".*"),
					Pattern.compile(Pattern.quote(EXPECTED_EVENT_NAME)), (handle, blockEvent, chaincodeEvent) -> {

						chaincodeEvents.add(new ChaincodeEventCapture(handle, blockEvent, chaincodeEvent));

						String es = blockEvent.getPeer() != null ? blockEvent.getPeer().getName() : "peer was null!!!";
						out("RECEIVED Chaincode event with handle: %s, chaincode Id: %s, chaincode event name: %s, "
								+ "transaction id: %s, event payload: \"%s\", from event source: %s", handle,
								chaincodeEvent.getChaincodeId(), chaincodeEvent.getEventName(),
								chaincodeEvent.getTxId(), new String(chaincodeEvent.getPayload()), es);

					});

			// For non foo channel unregister event listener to test events are not called.
			if (!isFooChain) {
				channel.unregisterChaincodeEventListener(chaincodeEventListenerHandle);
				chaincodeEventListenerHandle = null;

			}

			// Deprecated use v2.0 Lifecycle chaincode management.
			ChaincodeID.Builder chaincodeIDBuilder = ChaincodeID.newBuilder().setName(CHAIN_CODE_NAME)
					.setVersion(CHAIN_CODE_VERSION);
			if (null != CHAIN_CODE_PATH) {
				chaincodeIDBuilder.setPath(CHAIN_CODE_PATH);

			}
			chaincodeID = chaincodeIDBuilder.build();

			if (installChaincode) {
				////////////////////////////
				// Install Proposal Request
				//

				client.setUserContext(sampleOrg.getPeerAdmin());

				out("Creating install proposal");

				// Deprecated use v2.0 Lifecycle chaincode management.
				InstallProposalRequest installProposalRequest = client.newInstallProposalRequest();
				installProposalRequest.setChaincodeID(chaincodeID);

				if (isFooChain) {
					installProposalRequest
							.setChaincodeSourceLocation(Paths.get(TEST_FIXTURES_PATH, CHAIN_CODE_FILEPATH).toFile());

					if (testConfig.isFabricVersionAtOrAfter("1.1")) { // Fabric 1.1 added support for META-INF in the
						installProposalRequest.setChaincodeMetaInfLocation(
								new File("/home/dara/Documents/fabric-java-sdk/src/test/fixture/meta-infs/end2endit"));
					}
				} else {

					if (CHAIN_CODE_LANG.equals(Type.GO_LANG)) {

						installProposalRequest.setChaincodeInputStream(Util.generateTarGzInputStream(
								(Paths.get(TEST_FIXTURES_PATH, CHAIN_CODE_FILEPATH, "src", CHAIN_CODE_PATH).toFile()),
								Paths.get("src", CHAIN_CODE_PATH).toString()));
					} else {
						installProposalRequest.setChaincodeInputStream(Util.generateTarGzInputStream(
								(Paths.get(TEST_FIXTURES_PATH, CHAIN_CODE_FILEPATH).toFile()), "src"));
					}
				}

				installProposalRequest.setChaincodeVersion(CHAIN_CODE_VERSION);
				installProposalRequest.setChaincodeLanguage(CHAIN_CODE_LANG);

				out("Sending install proposal");

				////////////////////////////
				// only a client from the same org as the peer can issue an install request
				int numInstallProposal = 0;
				// Set<String> orgs = orgPeers.keySet();
				// for (SampleOrg org : testSampleOrgs) {

				Collection<Peer> peers = channel.getPeers();
				numInstallProposal = numInstallProposal + peers.size();
				// Deprecated use v2.0 Lifecycle chaincode management.
				responses = client.sendInstallProposal(installProposalRequest, peers);

				for (ProposalResponse response : responses) {
					if (response.getStatus() == ProposalResponse.Status.SUCCESS) {
						out("Successful install proposal response Txid: %s from peer %s", response.getTransactionID(),
								response.getPeer().getName());
						successful.add(response);
					} else {
						failed.add(response);
					}
				}

				// }
				out("Received %d install proposal responses. Successful+verified: %d . Failed: %d", numInstallProposal,
						successful.size(), failed.size());

				if (failed.size() > 0) {
					ProposalResponse first = failed.iterator().next();
					fail("Not enough endorsers for install :" + successful.size() + ".  " + first.getMessage());
				}
			}

			// Deprecated use v2.0 Lifecycle chaincode management.
			InstantiateProposalRequest instantiateProposalRequest = client.newInstantiationProposalRequest();
			instantiateProposalRequest.setProposalWaitTime(DEPLOYWAITTIME);
			instantiateProposalRequest.setChaincodeID(chaincodeID);
			instantiateProposalRequest.setChaincodeLanguage(CHAIN_CODE_LANG);
			instantiateProposalRequest.setFcn("init");
			instantiateProposalRequest.setArgs(new String[] { "a", "500", "b", "" + (200 + delta) });
			Map<String, byte[]> tm = new HashMap<>();
			tm.put("HyperLedgerFabric", "InstantiateProposalRequest:JavaSDK".getBytes(UTF_8));
			tm.put("method", "InstantiateProposalRequest".getBytes(UTF_8));
			instantiateProposalRequest.setTransientMap(tm);

			/*
			 * policy OR(Org1MSP.member, Org2MSP.member) meaning 1 signature from someone in
			 * either Org1 or Org2 See README.md Chaincode endorsement policies section for
			 * more details.
			 */
			ChaincodeEndorsementPolicy chaincodeEndorsementPolicy = new ChaincodeEndorsementPolicy();
			chaincodeEndorsementPolicy
					.fromYamlFile(new File(TEST_FIXTURES_PATH + "/sdkintegration/chaincodeendorsementpolicy.yaml"));
			instantiateProposalRequest.setChaincodeEndorsementPolicy(chaincodeEndorsementPolicy);

			out("Sending instantiateProposalRequest to all peers with arguments: a and b set to 500 and %s respectively",
					"" + (200 + delta));
			successful.clear();
			failed.clear();

			// Deprecated use v2.0 Lifecycle chaincode management.
			responses = channel.sendInstantiationProposal(instantiateProposalRequest);
			for (ProposalResponse response : responses) {
				if (response.isVerified() && response.getStatus() == ProposalResponse.Status.SUCCESS) {
					successful.add(response);
					out("Succesful instantiate proposal response Txid: %s from peer %s", response.getTransactionID(),
							response.getPeer().getName());
				} else {
					failed.add(response);
				}
			}
			out("Received %d instantiate proposal responses. Successful+verified: %d . Failed: %d", responses.size(),
					successful.size(), failed.size());
			if (failed.size() > 0) {
				for (ProposalResponse fail : failed) {

					out("Not enough endorsers for instantiate :" + successful.size() + "endorser failed with "
							+ fail.getMessage() + ", on peer" + fail.getPeer());

				}
				ProposalResponse first = failed.iterator().next();
				fail("Not enough endorsers for instantiate :" + successful.size() + "endorser failed with "
						+ first.getMessage() + ". Was verified:" + first.isVerified());
			}
			///////////////
			/// Send instantiate transaction to orderer
			out("Sending instantiateTransaction to orderer with a and b set to 500 and %s respectively",
					"" + (200 + delta));

			Channel.NOfEvents nOfEvents = createNofEvents();
			if (!channel.getPeers(EnumSet.of(PeerRole.EVENT_SOURCE)).isEmpty()) {
				nOfEvents.addPeers(channel.getPeers(EnumSet.of(PeerRole.EVENT_SOURCE)));
			}

			channel.sendTransaction(successful, createTransactionOptions() // Basically the default options but shows
																			// it's usage.
					.userContext(client.getUserContext()) // could be a different user context. this is the default.
					.shuffleOrders(false) // don't shuffle any orderers the default is true.
					.orderers(channel.getOrderers()) // specify the orderers we want to try this transaction. Fails once
														// all Orderers are tried.
					.nOfEvents(nOfEvents) // The events to signal the completion of the interest in the transaction
			).thenApply(transactionEvent -> {

				waitOnFabric(0);

				assertTrue(transactionEvent.isValid()); // must be valid to be here.

				assertNotNull(transactionEvent.getSignature()); // musth have a signature.
				BlockEvent blockEvent = transactionEvent.getBlockEvent(); // This is the blockevent that has this
																			// transaction.
				assertNotNull(blockEvent.getBlock()); // Make sure the RAW Fabric block is returned.

				out("Finished instantiate transaction with transaction id %s", transactionEvent.getTransactionID());

				try {
					assertEquals(blockEvent.getChannelId(), channel.getName());
					successful.clear();
					failed.clear();

					client.setUserContext(sampleOrg.getUser(doctorOrg1));

					///////////////
					/// Send transaction proposal to all peers
					TransactionProposalRequest transactionProposalRequest = client.newTransactionProposalRequest();
					transactionProposalRequest.setChaincodeID(chaincodeID);
					transactionProposalRequest.setChaincodeLanguage(CHAIN_CODE_LANG);
					// transactionProposalRequest.setFcn("invoke");
					transactionProposalRequest.setFcn("move");
					transactionProposalRequest.setProposalWaitTime(testConfig.getProposalWaitTime());
					transactionProposalRequest.setArgs("a", "b", "100");

					Map<String, byte[]> tm2 = new HashMap<>();
					tm2.put("HyperLedgerFabric", "TransactionProposalRequest:JavaSDK".getBytes(UTF_8)); // Just some
																										// extra junk in
																										// transient map
					tm2.put("method", "TransactionProposalRequest".getBytes(UTF_8)); // ditto
					tm2.put("result", ":)".getBytes(UTF_8)); // This should be returned in the payload see chaincode
																// why.
					if (Type.GO_LANG.equals(CHAIN_CODE_LANG) && testConfig.isFabricVersionAtOrAfter("1.2")) {

						expectedMoveRCMap.put(channelName, random.nextInt(300) + 100L); // the chaincode will return
																						// this as status see chaincode
																						// why.
						tm2.put("rc", (expectedMoveRCMap.get(channelName) + "").getBytes(UTF_8)); // This should be
																									// returned see
																									// chaincode why.
						// 400 and above results in the peer not endorsing!
					} else {
						expectedMoveRCMap.put(channelName, 200L); // not really supported for Java or Node.
					}

					tm2.put(EXPECTED_EVENT_NAME, EXPECTED_EVENT_DATA); // This should trigger an event see chaincode
																		// why.

					transactionProposalRequest.setTransientMap(tm2);

					out("sending transactionProposal to all peers with arguments: move(a,b,100)");

					// Collection<ProposalResponse> transactionPropResp =
					// channel.sendTransactionProposalToEndorsers(transactionProposalRequest);
					Collection<ProposalResponse> transactionPropResp = channel
							.sendTransactionProposal(transactionProposalRequest, channel.getPeers());
					for (ProposalResponse response : transactionPropResp) {
						if (response.getStatus() == ProposalResponse.Status.SUCCESS) {
							out("Successful transaction proposal response Txid: %s from peer %s",
									response.getTransactionID(), response.getPeer().getName());
							successful.add(response);
						} else {
							failed.add(response);
						}
					}

					out("Received %d transaction proposal responses. Successful+verified: %d . Failed: %d",
							transactionPropResp.size(), successful.size(), failed.size());

//                    storeObjectToSer(successful);
					if (failed.size() > 0) {
						ProposalResponse firstTransactionProposalResponse = failed.iterator().next();
						fail("Not enough endorsers for invoke(move a,b,100):" + failed.size() + " endorser error: "
								+ firstTransactionProposalResponse.getMessage() + ". Was verified: "
								+ firstTransactionProposalResponse.isVerified());
					}

					Collection<Set<ProposalResponse>> proposalConsistencySets = SDKUtils
							.getProposalConsistencySets(transactionPropResp);
					if (proposalConsistencySets.size() != 1) {
						fail(format("Expected only one set of consistent proposal responses but got %d",
								proposalConsistencySets.size()));
					}
					out("Successfully received transaction proposal responses.");

					// System.exit(10);

					ProposalResponse resp = successful.iterator().next();
					byte[] x = resp.getChaincodeActionResponsePayload(); // This is the data returned by the chaincode.
					String resultAsString = null;
					if (x != null) {
						resultAsString = new String(x, UTF_8);
					}
					assertEquals(":)", resultAsString);

					assertEquals(expectedMoveRCMap.get(channelName).longValue(),
							resp.getChaincodeActionResponseStatus()); // Chaincode's status.

					TxReadWriteSetInfo readWriteSetInfo = resp.getChaincodeActionResponseReadWriteSetInfo();
					// See blockwalker below how to transverse this
					assertNotNull(readWriteSetInfo);
					assertTrue(readWriteSetInfo.getNsRwsetCount() > 0);

					ChaincodeID cid = resp.getChaincodeID();
					assertNotNull(cid);
					final String path = cid.getPath();
					if (null == CHAIN_CODE_PATH) {
						assertTrue(path == null || "".equals(path));

					} else {

						assertEquals(CHAIN_CODE_PATH, path);

					}

					assertEquals(CHAIN_CODE_NAME, cid.getName());
					assertEquals(CHAIN_CODE_VERSION, cid.getVersion());

					////////////////////////////
					// Send Transaction Transaction to orderer
					out("Sending chaincode transaction(move a,b,100) to orderer.");
					return channel.sendTransaction(successful).get(testConfig.getTransactionWaitTime(),
							TimeUnit.SECONDS);

				} catch (Exception e) {
					out("Caught an exception while invoking chaincode");
					e.printStackTrace();
					fail("Failed invoking chaincode with error : " + e.getMessage());
				}

				return null;

			}).thenApply(transactionEvent -> {
				try {

					waitOnFabric(0);

					assertTrue(transactionEvent.isValid()); // must be valid to be here.
					out("Finished transaction with transaction id %s", transactionEvent.getTransactionID());
					testTxID = transactionEvent.getTransactionID(); // used in the channel queries later

					////////////////////////////
					// Send Query Proposal to all peers
					//
					String expect = "" + (300 + delta);
					out("Now query chaincode for the value of b.");
					QueryByChaincodeRequest queryByChaincodeRequest = client.newQueryProposalRequest();
					queryByChaincodeRequest.setArgs(new String[] { "b" });
					queryByChaincodeRequest.setFcn("query");
					queryByChaincodeRequest.setChaincodeID(chaincodeID);

					Map<String, byte[]> tm2 = new HashMap<>();
					tm2.put("HyperLedgerFabric", "QueryByChaincodeRequest:JavaSDK".getBytes(UTF_8));
					tm2.put("method", "QueryByChaincodeRequest".getBytes(UTF_8));
					queryByChaincodeRequest.setTransientMap(tm2);

					Collection<ProposalResponse> queryProposals = channel.queryByChaincode(queryByChaincodeRequest,
							channel.getPeers());
					for (ProposalResponse proposalResponse : queryProposals) {
						if (!proposalResponse.isVerified()
								|| proposalResponse.getStatus() != ProposalResponse.Status.SUCCESS) {
							fail("Failed query proposal from peer " + proposalResponse.getPeer().getName() + " status: "
									+ proposalResponse.getStatus() + ". Messages: " + proposalResponse.getMessage()
									+ ". Was verified : " + proposalResponse.isVerified());
						} else {
							String payload = proposalResponse.getProposalResponse().getResponse().getPayload()
									.toStringUtf8();
							out("Query payload of b from peer %s returned %s", proposalResponse.getPeer().getName(),
									payload);
							assertEquals(payload, expect);
						}
					}

					return null;
				} catch (Exception e) {
					out("Caught exception while running query");
					e.printStackTrace();
					fail("Failed during chaincode query with error : " + e.getMessage());
				}

				return null;
			}).exceptionally(e -> {
				if (e instanceof TransactionEventException) {
					BlockEvent.TransactionEvent te = ((TransactionEventException) e).getTransactionEvent();
					if (te != null) {
						throw new AssertionError(
								format("Transaction with txid %s failed. %s", te.getTransactionID(), e.getMessage()),
								e);
					}
				}

				throw new AssertionError(
						format("Test failed with %s exception %s", e.getClass().getName(), e.getMessage()), e);

			}).get(testConfig.getTransactionWaitTime(), TimeUnit.SECONDS);

			BlockchainInfo channelInfo = channel.queryBlockchainInfo();
			out("Channel info for : " + channelName);
			out("Channel height: " + channelInfo.getHeight());
			String chainCurrentHash = Hex.encodeHexString(channelInfo.getCurrentBlockHash());
			String chainPreviousHash = Hex.encodeHexString(channelInfo.getPreviousBlockHash());
			out("Chain current block hash: " + chainCurrentHash);
			out("Chainl previous block hash: " + chainPreviousHash);

			// Query by block number. Should return latest block, i.e. block number 2
			BlockInfo returnedBlock = channel.queryBlockByNumber(channelInfo.getHeight() - 1);
			String previousHash = Hex.encodeHexString(returnedBlock.getPreviousHash());
			out("queryBlockByNumber returned correct block with blockNumber " + returnedBlock.getBlockNumber()
					+ " \n previous_hash " + previousHash);
			assertEquals(channelInfo.getHeight() - 1, returnedBlock.getBlockNumber());
			assertEquals(chainPreviousHash, previousHash);

			// Query by block hash. Using latest block's previous hash so should return
			// block number 1
			byte[] hashQuery = returnedBlock.getPreviousHash();
			returnedBlock = channel.queryBlockByHash(hashQuery);
			out("queryBlockByHash returned block with blockNumber " + returnedBlock.getBlockNumber());
			assertEquals(channelInfo.getHeight() - 2, returnedBlock.getBlockNumber());

			// Query block by TxID. Since it's the last TxID, should be block 2
			returnedBlock = channel.queryBlockByTransactionID(testTxID);
			out("queryBlockByTxID returned block with blockNumber " + returnedBlock.getBlockNumber());
			assertEquals(channelInfo.getHeight() - 1, returnedBlock.getBlockNumber());

			// query transaction by ID
			TransactionInfo txInfo = channel.queryTransactionByID(testTxID);
			out("QueryTransactionByID returned TransactionInfo: txID " + txInfo.getTransactionID()
					+ "\n     validation code " + txInfo.getValidationCode().getNumber());

			if (chaincodeEventListenerHandle != null) {

				channel.unregisterChaincodeEventListener(chaincodeEventListenerHandle);

				final int numberEventsExpected = channel.getPeers(EnumSet.of(PeerRole.EVENT_SOURCE)).size();
				// just make sure we get the notifications.
				for (int i = 15; i > 0; --i) {
					if (chaincodeEvents.size() == numberEventsExpected) {
						break;
					} else {
						Thread.sleep(90); // wait for the events.
					}

				}
				assertEquals(numberEventsExpected, chaincodeEvents.size());

				for (ChaincodeEventCapture chaincodeEventCapture : chaincodeEvents) {
					assertEquals(chaincodeEventListenerHandle, chaincodeEventCapture.handle);
					assertEquals(testTxID, chaincodeEventCapture.chaincodeEvent.getTxId());
					assertEquals(EXPECTED_EVENT_NAME, chaincodeEventCapture.chaincodeEvent.getEventName());
					assertTrue(Arrays.equals(EXPECTED_EVENT_DATA, chaincodeEventCapture.chaincodeEvent.getPayload()));
					assertEquals(CHAIN_CODE_NAME, chaincodeEventCapture.chaincodeEvent.getChaincodeId());

					BlockEvent blockEvent = chaincodeEventCapture.blockEvent;
					assertEquals(channelName, blockEvent.getChannelId());

				}

			} else {
				assertTrue(chaincodeEvents.isEmpty());
			}

			out("Running for Channel %s done", channelName);

		} catch (Exception e) {
			out("Caught an exception running channel %s", channel.getName());
			e.printStackTrace();
			fail("Test failed with error : " + e.getMessage());
		}
	}

	Channel constructChannel(String name, HFClient client, SampleOrg sampleOrg) throws Exception {
		////////////////////////////
		// Construct the channel
		//

		out("Constructing channel %s", name);
		// Only peer Admin org
		SampleUser peerAdmin = sampleOrg.getPeerAdmin();
		client.setUserContext(peerAdmin);

		Collection<Orderer> orderers = new LinkedList<>();

		for (String orderName : sampleOrg.getOrdererNames()) {

			Properties ordererProperties = testConfig.getOrdererProperties(orderName);

			// example of setting keepAlive to avoid timeouts on inactive http2 connections.
			// Under 5 minutes would require changes to server side to accept faster ping
			// rates.
			ordererProperties.put("grpc.NettyChannelBuilderOption.keepAliveTime",
					new Object[] { 5L, TimeUnit.MINUTES });
			ordererProperties.put("grpc.NettyChannelBuilderOption.keepAliveTimeout",
					new Object[] { 8L, TimeUnit.SECONDS });
			ordererProperties.put("grpc.NettyChannelBuilderOption.keepAliveWithoutCalls", new Object[] { true });

			orderers.add(client.newOrderer(orderName, sampleOrg.getOrdererLocation(orderName), ordererProperties));
		}

		// Just pick the first orderer in the list to create the channel.

		Orderer anOrderer = orderers.iterator().next();
		orderers.remove(anOrderer);

		String path = TEST_FIXTURES_PATH + "/sdkintegration/e2e-2Orgs/" + testConfig.getFabricConfigGenVers() + "/"
				+ name + ".tx";
		ChannelConfiguration channelConfiguration = new ChannelConfiguration(new File(path));

		// Create channel that has only one signer that is this orgs peer admin. If
		// channel creation policy needed more signature they would need to be added
		// too.
		Channel newChannel = client.newChannel(name, anOrderer, channelConfiguration,
				client.getChannelConfigurationSignature(channelConfiguration, peerAdmin));

		out("Created channel %s", name);

		boolean everyother = true; // test with both cases when doing peer eventing.
		for (String peerName : sampleOrg.getPeerNames()) {
			String peerLocation = sampleOrg.getPeerLocation(peerName);

			Properties peerProperties = testConfig.getPeerProperties(peerName); // test properties for peer.. if any.
			if (peerProperties == null) {
				peerProperties = new Properties();
			}

			// Example of setting specific options on grpc's NettyChannelBuilder
			peerProperties.put("grpc.NettyChannelBuilderOption.maxInboundMessageSize", 9000000);

			Peer peer = client.newPeer(peerName, peerLocation, peerProperties);
			if (testConfig.isFabricVersionAtOrAfter("1.3")) {
				newChannel.joinPeer(peer, createPeerOptions().setPeerRoles(EnumSet.of(PeerRole.ENDORSING_PEER,
						PeerRole.LEDGER_QUERY, PeerRole.CHAINCODE_QUERY, PeerRole.EVENT_SOURCE))); // Default is all
																									// roles.

			} else {
				if (everyother) {
					newChannel.joinPeer(peer, createPeerOptions().setPeerRoles(EnumSet.of(PeerRole.ENDORSING_PEER,
							PeerRole.LEDGER_QUERY, PeerRole.CHAINCODE_QUERY, PeerRole.EVENT_SOURCE))); // Default is all
																										// roles.
				} else {
					// Set peer to not be all roles but eventing.
					newChannel.joinPeer(peer, createPeerOptions().setPeerRoles(
							EnumSet.of(PeerRole.ENDORSING_PEER, PeerRole.LEDGER_QUERY, PeerRole.CHAINCODE_QUERY)));
				}
			}
			out("Peer %s joined channel %s", peerName, name);
			everyother = !everyother;
		}
		// just for testing ...
		if (testConfig.isFabricVersionAtOrAfter("1.3")) {
			// Make sure there is one of each type peer at the very least.
			assertFalse(newChannel.getPeers(EnumSet.of(PeerRole.EVENT_SOURCE)).isEmpty());
			assertFalse(newChannel.getPeers(PeerRole.NO_EVENT_SOURCE).isEmpty());
		}

		for (Orderer orderer : orderers) { // add remaining orderers if any.
			newChannel.addOrderer(orderer);
		}

		newChannel.initialize();

		SampleOrg sampleOrg2 = testConfig.getIntegrationTestsSampleOrg("peerOrg2");

		SampleUser peerAdmin2 = sampleOrg2.getPeerAdmin();
		client.setUserContext(peerAdmin2);

		newChannel = client.getChannel(name);

		for (String peerName : sampleOrg2.getPeerNames()) {
			String peerLocation = sampleOrg2.getPeerLocation(peerName);

			Properties peerProperties = testConfig.getPeerProperties(peerName); // test properties for peer.. if any.
			if (peerProperties == null) {
				peerProperties = new Properties();
			}

			// Example of setting specific options on grpc's NettyChannelBuilder
			peerProperties.put("grpc.NettyChannelBuilderOption.maxInboundMessageSize", 9000000);

			Peer peer = client.newPeer(peerName, peerLocation, peerProperties);
			if (testConfig.isFabricVersionAtOrAfter("1.3")) {
				newChannel.joinPeer(peer, createPeerOptions().setPeerRoles(EnumSet.of(PeerRole.ENDORSING_PEER,
						PeerRole.LEDGER_QUERY, PeerRole.CHAINCODE_QUERY, PeerRole.EVENT_SOURCE))); // Default is all
																									// roles.

			} else {
				if (everyother) {
					newChannel.joinPeer(peer, createPeerOptions().setPeerRoles(EnumSet.of(PeerRole.ENDORSING_PEER,
							PeerRole.LEDGER_QUERY, PeerRole.CHAINCODE_QUERY, PeerRole.EVENT_SOURCE))); // Default is all
																										// roles.
				} else {
					// Set peer to not be all roles but eventing.
					newChannel.joinPeer(peer, createPeerOptions().setPeerRoles(
							EnumSet.of(PeerRole.ENDORSING_PEER, PeerRole.LEDGER_QUERY, PeerRole.CHAINCODE_QUERY)));
				}
			}
			out("Peer %s joined channel %s", peerName, name);
			everyother = !everyother;
		}

		out("Finished initialization channel %s", name);

		// Just checks if channel can be serialized and deserialized .. otherwise this
		// is just a waste :)
		byte[] serializedChannelBytes = newChannel.serializeChannel();
		newChannel.shutdown(true);

		return client.deSerializeChannel(serializedChannelBytes).initialize();

	}

	protected void waitOnFabric(int additional) {
		// NOOP today

	}

	static void blockWalker(HFClient client, Channel channel)
			throws InvalidArgumentException, ProposalException, IOException {
		try {
			BlockchainInfo channelInfo = channel.queryBlockchainInfo();

			for (long current = channelInfo.getHeight() - 1; current > -1; --current) {
				BlockInfo returnedBlock = channel.queryBlockByNumber(current);
				final long blockNumber = returnedBlock.getBlockNumber();

				out("current block number %d has data hash: %s", blockNumber,
						Hex.encodeHexString(returnedBlock.getDataHash()));
				out("current block number %d has previous hash id: %s", blockNumber,
						Hex.encodeHexString(returnedBlock.getPreviousHash()));
				out("current block number %d has calculated block hash is %s", blockNumber,
						Hex.encodeHexString(SDKUtils.calculateBlockHash(client, blockNumber,
								returnedBlock.getPreviousHash(), returnedBlock.getDataHash())));

				final int envelopeCount = returnedBlock.getEnvelopeCount();
				assertEquals(1, envelopeCount);
				out("current block number %d has %d envelope count:", blockNumber, returnedBlock.getEnvelopeCount());
				int i = 0;
				int transactionCount = 0;
				for (BlockInfo.EnvelopeInfo envelopeInfo : returnedBlock.getEnvelopeInfos()) {
					++i;

					out("  Transaction number %d has transaction id: %s", i, envelopeInfo.getTransactionID());
					final String channelId = envelopeInfo.getChannelId();
					assertTrue("foo".equals(channelId) || "bar".equals(channelId));

					out("  Transaction number %d has channel id: %s", i, channelId);
//                    out("  Transaction number %d has epoch: %d", i, envelopeInfo.getEpoch());
					out("  Transaction number %d has transaction timestamp: %tB %<te,  %<tY  %<tT %<Tp", i,
							envelopeInfo.getTimestamp());
					out("  Transaction number %d has type id: %s", i, "" + envelopeInfo.getType());
					out("  Transaction number %d has nonce : %s", i, "" + Hex.encodeHexString(envelopeInfo.getNonce()));
					out("  Transaction number %d has submitter mspid: %s, certificate: %s", i,
							envelopeInfo.getCreator().getMspid(), envelopeInfo.getCreator().getId());

					if (envelopeInfo.getType() == TRANSACTION_ENVELOPE) {
						++transactionCount;
						BlockInfo.TransactionEnvelopeInfo transactionEnvelopeInfo = (BlockInfo.TransactionEnvelopeInfo) envelopeInfo;

						out("  Transaction number %d has %d actions", i,
								transactionEnvelopeInfo.getTransactionActionInfoCount());
						assertEquals(1, transactionEnvelopeInfo.getTransactionActionInfoCount()); // for now there is
																									// only 1 action per
																									// transaction.
						out("  Transaction number %d isValid %b", i, transactionEnvelopeInfo.isValid());
						assertEquals(transactionEnvelopeInfo.isValid(), true);
						out("  Transaction number %d validation code %d", i,
								transactionEnvelopeInfo.getValidationCode());
						assertEquals(0, transactionEnvelopeInfo.getValidationCode());

						int j = 0;
						for (BlockInfo.TransactionEnvelopeInfo.TransactionActionInfo transactionActionInfo : transactionEnvelopeInfo
								.getTransactionActionInfos()) {
							++j;
							out("   Transaction action %d has response status %d", j,
									transactionActionInfo.getResponseStatus());
							out("   Transaction action %d has response message bytes as string: %s", j, printableString(
									new String(transactionActionInfo.getResponseMessageBytes(), UTF_8)));
							out("   Transaction action %d has %d endorsements", j,
									transactionActionInfo.getEndorsementsCount());
//							assertEquals(2, transactionActionInfo.getEndorsementsCount());

							for (int n = 0; n < transactionActionInfo.getEndorsementsCount(); ++n) {
								BlockInfo.EndorserInfo endorserInfo = transactionActionInfo.getEndorsementInfo(n);
								out("Endorser %d signature: %s", n, Hex.encodeHexString(endorserInfo.getSignature()));
								out("Endorser %d endorser: mspid %s \n certificate %s", n, endorserInfo.getMspid(),
										endorserInfo.getId());
							}
							out("   Transaction action %d has %d chaincode input arguments", j,
									transactionActionInfo.getChaincodeInputArgsCount());
							for (int z = 0; z < transactionActionInfo.getChaincodeInputArgsCount(); ++z) {
								out("     Transaction action %d has chaincode input argument %d is: %s", j, z,
										printableString(
												new String(transactionActionInfo.getChaincodeInputArgs(z), UTF_8)));
							}

							out("   Transaction action %d proposal response status: %d", j,
									transactionActionInfo.getProposalResponseStatus());
							out("   Transaction action %d proposal response payload: %s", j,
									printableString(new String(transactionActionInfo.getProposalResponsePayload())));

							String chaincodeIDName = transactionActionInfo.getChaincodeIDName();
							String chaincodeIDVersion = transactionActionInfo.getChaincodeIDVersion();
							String chaincodeIDPath = transactionActionInfo.getChaincodeIDPath();
							out("   Transaction action %d proposal chaincodeIDName: %s, chaincodeIDVersion: %s,  chaincodeIDPath: %s ",
									j, chaincodeIDName, chaincodeIDVersion, chaincodeIDPath);

							TxReadWriteSetInfo rwsetInfo = transactionActionInfo.getTxReadWriteSet();
							if (null != rwsetInfo) {
								out("   Transaction action %d has %d name space read write sets", j,
										rwsetInfo.getNsRwsetCount());

								for (TxReadWriteSetInfo.NsRwsetInfo nsRwsetInfo : rwsetInfo.getNsRwsetInfos()) {
									final String namespace = nsRwsetInfo.getNamespace();
									KvRwset.KVRWSet rws = nsRwsetInfo.getRwset();

									int rs = -1;
									for (KvRwset.KVRead readList : rws.getReadsList()) {
										rs++;

										out("     Namespace %s read set %d key %s  version [%d:%d]", namespace, rs,
												readList.getKey(), readList.getVersion().getBlockNum(),
												readList.getVersion().getTxNum());

										if ("bar".equals(channelId) && blockNumber == 2) {
											if ("example_cc_go".equals(namespace)) {
												if (rs == 0) {
													assertEquals("a", readList.getKey());
													assertEquals(1, readList.getVersion().getBlockNum());
													assertEquals(0, readList.getVersion().getTxNum());
												} else if (rs == 1) {
													assertEquals("b", readList.getKey());
													assertEquals(1, readList.getVersion().getBlockNum());
													assertEquals(0, readList.getVersion().getTxNum());
												} else {
													fail(format("unexpected readset %d", rs));
												}

											}
										}
									}

									rs = -1;
									for (KvRwset.KVWrite writeList : rws.getWritesList()) {
										rs++;
										String valAsString = printableString(
												new String(writeList.getValue().toByteArray(), UTF_8));

										out("     Namespace %s write set %d key %s has value '%s' ", namespace, rs,
												writeList.getKey(), valAsString);

										if ("bar".equals(channelId) && blockNumber == 2) {
											if (rs == 0) {
												assertEquals("a", writeList.getKey());
												assertEquals("400", valAsString);
											} else if (rs == 1) {
												assertEquals("b", writeList.getKey());
												assertEquals("400", valAsString);
											} else {
												fail(format("unexpected writeset %d", rs));
											}

										}
									}
								}
							}
						}
					}

					assertEquals(transactionCount, returnedBlock.getTransactionCount());

				}
			}
		} catch (InvalidProtocolBufferRuntimeException e) {
			throw e.getCause();
		}
	}

}