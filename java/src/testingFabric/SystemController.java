package testingFabric;

import static java.lang.String.format;
import static java.nio.charset.StandardCharsets.UTF_8;
import static org.hyperledger.fabric.sdk.BlockInfo.EnvelopeType.TRANSACTION_ENVELOPE;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Properties;
import java.util.Scanner;

import org.apache.commons.codec.binary.Hex;
import org.hyperledger.fabric.protos.ledger.rwset.kvrwset.KvRwset;
import org.hyperledger.fabric.sdk.BlockInfo;
import org.hyperledger.fabric.sdk.BlockchainInfo;
import org.hyperledger.fabric.sdk.ChaincodeID;
import org.hyperledger.fabric.sdk.Channel;
import org.hyperledger.fabric.sdk.HFClient;
import org.hyperledger.fabric.sdk.NetworkConfig;
import org.hyperledger.fabric.sdk.Peer;
import org.hyperledger.fabric.sdk.SDKUtils;
import org.hyperledger.fabric.sdk.TxReadWriteSetInfo;
import org.hyperledger.fabric.sdk.User;
import org.hyperledger.fabric.sdk.TransactionRequest.Type;
import org.hyperledger.fabric.sdk.exception.InvalidArgumentException;
import org.hyperledger.fabric.sdk.exception.InvalidProtocolBufferRuntimeException;
import org.hyperledger.fabric.sdk.exception.ProposalException;
import org.hyperledger.fabric.sdk.security.CryptoSuite;
import org.junit.BeforeClass;
import org.junit.runner.JUnitCore;

import main.java.org.app.config.Config;
import testingFabric.CertificateIssue;
import testingFabric.End2endIT;
import testingFabric.UpgradeChaincode;
import proxyReEncryption.*;

public class SystemController extends End2endIT {

	End2endIT clientSide;
	static CertificateIssue certIssue;

	static File sampleStoreFile = new File(System.getProperty("java.io.tmpdir") + "/HFCSampletest.properties");
	static SampleStore sampleStore = null;
	private static NetworkConfig networkConfig;
	private static final String TEST_ORG2 = "Org2";
	private static final String TEST_ORG1 = "Org1";
	private static final int DEPLOYWAITTIME = testConfig.getDeployWaitTime();
	String user = "user3";
	Integer found = 0;
	private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
	private static final SimpleDateFormat timeDocFormate = new SimpleDateFormat("yyyy-MM-dd");
	String[] userId = { "Id19041", "Id15123", "Id12415", "Id54131", "Id65412" };
	String[] doctorId = { "dr52114", "dr61231", "dr12353", "dr64261", "dr57822" };
	String[] hosId = { "hos6542", "hos8591", "hos7631", "hos5237", "hos6217" };
	String[] action = { "Create", "Read", "Transfer" };
	String[] records = { "ThA+zUVJlNVyD6/w7soi", "ThA+zUVJlNVyD6/w7soitS8A+Pkjjw==",
			"ThA+zUVJlNVyD6/w7soitS8A+Pkjj98p93Kc", "ThA+zUVJlNVyD6/w7soitS8A+Pkjj98p93KcG1yq45uN05wf" };
	String[] recordsHash = { "AEF1F803577BCFBC708EC5F391ADF05C657596B72DB0977C0CF81D095EDFC632",
			"D7330D6469A8AAE057BCE4A19A5A26ED81FB8D080182E85D3DBA8321B65AADCF",
			"63C4A1334473F338E470C53111827C4027F96B7292D425861A21FC124518D5D7",
			"D0E958A883D401E45F9F0AA378CA303179271EB8B399F6CCB96FBA767D847F1B",
			"7079E198FCD275196FB2811148C03ACF770287C989DF383EC655D3A40245DA06" };
	String permission = "Succeed";
	String[] consentVersion = { "Version 1", "Version 2", "Version 3", "Version 4" };
	String[] consentHash = { "7B51A787C9C5234D1B04E69F8D3C6515DBED48DBA78F0E57BF033F44E3369B7C",
			"C02475AC116DBAC9775D3C12B6A79292852D72F978F1491E493F52537DA70D4C",
			"A86E0C0B789BEB8BB585220EBB7DA2B57BDFD9ACB9D6BFE53A1F67C493299C06",
			"42964C8C60BA2F0F82E711EB454D9B126DC6888FFCD1BFB6E92BF56060648D0A" };
	String[] recordAddress = { "192.168.145.2/oitS8A+Pkjj9", "192.158.123.4/w7soitS8A", "124.1.13.18/98p93KcG1yq" };
	String[] recordOwnerAdd = { "121.18.14.21/B7DA2B57BDFD9", "125.118.16.42/B04E69F8D", "112.54.14.75/BED48DBA7" };
	static String CHAIN_CODE_VERSION_1 = "1";
	static String CHAIN_CODE_VERSION_2 = "2";
	{
		CHAIN_CODE_FILEPATH = "sdkintegration/javacc/userWallet"; // override path to Node code
		CHAIN_CODE_PATH = null; // This is used only for GO.
		CHAIN_CODE_NAME = "Wallet_java"; // chaincode name.
		CHAIN_CODE_LANG = Type.GO_LANG; // language is Java.
	}

	String purposeTree = "[{" + "\"Education\":{" + "\"E-Statistic\":{" + "\"S-Survey\":\"null\"" + "},"
			+ "\"E-MedicineDiscovery\":\"null\"" + "}," + "\"MedicalTreatment\":{" + "\"M-Cancer\": \"null\","
			+ "\"M-Diabetic\":\"null\"," + "\"M-Education\":{" + "\"E-Reporting\": \"null\"" + "},"
			+ "\"M-Mental\":\"null\"" + "}," + "\"Insurance\":{" + "\"I-EvaluateInsuranceStatus\": \"null\"" + "}"
			+ "}]";

	static ChaincodeID.Builder chaincodeIDBuilderFabcar_1 = ChaincodeID.newBuilder().setName(Config.CHAINCODE_2_NAME)
			.setVersion(CHAIN_CODE_VERSION_1).setPath(Config.CHAINCODE_2_PATH);
	static ChaincodeID chaincodeIDFabcar_1 = chaincodeIDBuilderFabcar_1.build();

	static ChaincodeID.Builder chaincodeIDBuilderFabcar_2 = ChaincodeID.newBuilder().setName(Config.CHAINCODE_2_NAME)
			.setVersion(CHAIN_CODE_VERSION_2).setPath(Config.CHAINCODE_2_PATH);
	static ChaincodeID chaincodeIDFabcar_2 = chaincodeIDBuilderFabcar_2.build();

	public static void main(String arg[]) throws Exception {

		/** exexute the main class to run the engine */
//		JUnitCore junit = new JUnitCore();
//		junit.run(End2endIT.class);

//		String password = "12qwqe3";
//		String genPass = BCrypt.hashpw(password, BCrypt.gensalt());

		/**
		 * After starting the engine, we need to run this code everytime we want to
		 * interact to blockchain again!
		 */
		newdoMainSetup();
		newsetup();

		// Issue and get all identity of Patient ECert to the specific organization
//		certIssue = new CertificateIssue();
//		certIssue.org = "org2";
//		certIssue.rootCADir = certIssue.dir[1];

//		certIssue.init();
//		certIssue.setup("org2");
//		certIssue.issueCert();
//		certIssue.importCert();
//		certIssue.verifyWithCertificateAuthority();
//		certIssue.testGetAllIdentity();

	}

	public static void newdoMainSetup() throws Exception {
		networkConfig = NetworkConfig.fromYamlFile(testConfig.getTestNetworkConfigFileYAML());
		networkConfig.getOrdererNames().forEach(ordererName -> {
			try {
				Properties ordererProperties = networkConfig.getOrdererProperties(ordererName);
				Properties testProp = testConfig.getEndPointProperties("orderer", ordererName);
				ordererProperties.setProperty("clientCertFile", testProp.getProperty("clientCertFile"));
				ordererProperties.setProperty("clientKeyFile", testProp.getProperty("clientKeyFile"));
				networkConfig.setOrdererProperties(ordererName, ordererProperties);

			} catch (InvalidArgumentException e) {
				throw new RuntimeException(e);
			}
		});

		networkConfig.getPeerNames().forEach(peerName -> {
			try {
				Properties peerProperties = networkConfig.getPeerProperties(peerName);
				Properties testProp = testConfig.getEndPointProperties("peer", peerName);
				peerProperties.setProperty("clientCertFile", testProp.getProperty("clientCertFile"));
				peerProperties.setProperty("clientKeyFile", testProp.getProperty("clientKeyFile"));
				networkConfig.setPeerProperties(peerName, peerProperties);

			} catch (InvalidArgumentException e) {
				throw new RuntimeException(e);
			}
		});

	}

	public static void newsetup() throws Exception {
		// TODO Auto-generated method stub

		sampleStore = new SampleStore(sampleStoreFile);
//		queryListIdentity(sampleStore);
//		super.roles.add("medical doctor");

		// need these two below for upgrading chaincode
		checkConfig();
		enrollUsersSetup(sampleStore);

		runFabricTest(); // Runs Fabric tests with constructing channels, joining peers, exercising
		// chaincode

	}

	public static void pressEnterToContinue() {
		String s = "Press enter Test Proxy Re-encryption:";
		System.out.println(s);
		Scanner scanner = new Scanner(System.in);
		scanner.nextLine();
		scanner.close();
	}

	public static void runFabricTest() throws Exception {
		// TODO Auto-generated method stub
		HFClient client = getTheClient();
		ProxyMain proxyMain = new ProxyMain();

		////////////////////////////
		// Construct and run the channels
		Collection<SampleOrg> sample = testConfig.getIntegrationTestsSampleOrgs();
		SampleOrg sampleOrg = testConfig.getIntegrationTestsSampleOrg("peerOrg1");
		client.setUserContext(sampleOrg.getPeerAdmin());
		Channel fooChannel = constructChannelTest(client, FOO_CHANNEL_NAME);

		Collection<Peer> peers = fooChannel.getPeers();

//		UpgradeChaincode.upgradeChaincode(client, fooChannel, chaincodeIDFabcar_1, chaincodeIDFabcar_2, sample);
//		SystemExecution.initPurposeTree(client, fooChannel, chaincodeIDFabcar_1, sampleOrg);
// 		SystemExecution.initRecordsWithConsent(client,fooChannel,chaincodeIDFabcar_1,sampleOrg);
//		SystemExecution.searchUser(client, fooChannel, chaincodeIDFabcar_1, sampleOrg);
//		pressEnterToContinue();
//		//please generate re-key
//		ProxyMain.execute();

		// when patient not in the hospital
		// add new data with consent that no nurse role after updating

//		SystemExecution.accessRequest(client, fooChannel, chaincodeIDFabcar_1, sampleOrg);
//		SystemExecution.updateConsent(client, fooChannel, chaincodeIDFabcar_1, sampleOrg);
//		SystemExecution.getHistoryForData(client, fooChannel, chaincodeIDFabcar_1, sampleOrg);
		blockWalker2(client, fooChannel);

//		SystemExecution.initMarble(client, fooChannel, chaincodeIDFabcar_1, sampleOrg);

		////////////////////////////////
		/* please do not operate them */

//		for (Peer peer : fooChannel.getPeers()) {
//			if (peer.getName().equals(Config.ORG1_PEER_0) || peer.getName().equals(Config.ORG1_PEER_1)) {
//				sampleOrg = testConfig.getIntegrationTestsSampleOrg("peerOrg1");
//				client.setUserContext(sampleOrg.getPeerAdmin());
//			} else {
//				sampleOrg = testConfig.getIntegrationTestsSampleOrg("peerOrg2");
//				client.setUserContext(sampleOrg.getPeerAdmin());
//			}
//			System.out.print(checkInstalledChaincode(client, peer, Config.CHAINCODE_2_NAME, Config.CHAINCODE_2_PATH,
//					CHAIN_CODE_VERSION_2));
//		}
//		for (Peer peer : fooChannel.getPeers()) {
//		System.out.print(checkInstantiatedChaincode(fooChannel, peer, Config.CHAINCODE_2_NAME, Config.CHAINCODE_2_PATH,
//				CHAIN_CODE_VERSION_2));
//		}
//		for (Peer peer : fooChannel.getPeers()) {
//			if (peer.getName().equals(Config.ORG1_PEER_0) || peer.getName().equals(Config.ORG1_PEER_1)) {
//				sampleOrg = testConfig.getIntegrationTestsSampleOrg("peerOrg1");
//				client.setUserContext(sampleOrg.getPeerAdmin());
//			} else {
//				sampleOrg = testConfig.getIntegrationTestsSampleOrg("peerOrg2");
//				client.setUserContext(sampleOrg.getPeerAdmin());
//			}
//		}
//		sampleOrg = testConfig.getIntegrationTestsSampleOrg("peerOrg2");
//		client.setUserContext(sampleOrg.getPeerAdmin());
//		initFabcar(client, fooChannel, chaincodeIDFabcar, sampleOrg);
//		queryFabcar(client, fooChannel, chaincodeIDFabcar, sampleOrg);

//		queryMarble(client, fooChannel, chaincodeIDFabcar, sampleOrg);
//		initMarbleOrigin(client, fooChannel, chaincodeIDFabcar, sampleOrg);
//        insertNewTx(client, fooChannel, chaincodeID, sampleOrg);

		/* please do not operate them */
		////////////////////////////////

		out("That's all folks!");

	}

	static void blockWalker2(HFClient client, Channel channel)
			throws InvalidArgumentException, ProposalException, IOException {
		try {
			BlockchainInfo channelInfo = channel.queryBlockchainInfo();

			for (long current = channelInfo.getHeight() - 1; current > -1; --current) {
				BlockInfo returnedBlock = channel.queryBlockByNumber(current);
				final long blockNumber = returnedBlock.getBlockNumber();

				out("current block number %d has data hash: %s", blockNumber,
						Hex.encodeHexString(returnedBlock.getDataHash()));
				out("current block number %d has previous hash id: %s", blockNumber,
						Hex.encodeHexString(returnedBlock.getPreviousHash()));
				out("current block number %d has calculated block hash is %s", blockNumber,
						Hex.encodeHexString(SDKUtils.calculateBlockHash(client, blockNumber,
								returnedBlock.getPreviousHash(), returnedBlock.getDataHash())));

				final int envelopeCount = returnedBlock.getEnvelopeCount();
				assertEquals(1, envelopeCount);
				out("current block number %d has %d envelope count:", blockNumber, returnedBlock.getEnvelopeCount());
				int i = 0;
				int transactionCount = 0;
				for (BlockInfo.EnvelopeInfo envelopeInfo : returnedBlock.getEnvelopeInfos()) {
					++i;

					out("  Transaction number %d has transaction id: %s", i, envelopeInfo.getTransactionID());
					final String channelId = envelopeInfo.getChannelId();
					assertTrue("foo".equals(channelId) || "bar".equals(channelId));

					out("  Transaction number %d has channel id: %s", i, channelId);
//                    out("  Transaction number %d has epoch: %d", i, envelopeInfo.getEpoch());
					out("  Transaction number %d has transaction timestamp: %tB %<te,  %<tY  %<tT %<Tp", i,
							envelopeInfo.getTimestamp());
					out("  Transaction number %d has type id: %s", i, "" + envelopeInfo.getType());
					out("  Transaction number %d has nonce : %s", i, "" + Hex.encodeHexString(envelopeInfo.getNonce()));
					out("  Transaction number %d has submitter mspid: %s, certificate: %s", i,
							envelopeInfo.getCreator().getMspid(), envelopeInfo.getCreator().getId());

					if (envelopeInfo.getType() == TRANSACTION_ENVELOPE) {
						++transactionCount;
						BlockInfo.TransactionEnvelopeInfo transactionEnvelopeInfo = (BlockInfo.TransactionEnvelopeInfo) envelopeInfo;

						out("  Transaction number %d has %d actions", i,
								transactionEnvelopeInfo.getTransactionActionInfoCount());
						assertEquals(1, transactionEnvelopeInfo.getTransactionActionInfoCount()); // for now there is
																									// only 1 action per
																									// transaction.
						out("  Transaction number %d isValid %b", i, transactionEnvelopeInfo.isValid());
						assertEquals(transactionEnvelopeInfo.isValid(), true);
						out("  Transaction number %d validation code %d", i,
								transactionEnvelopeInfo.getValidationCode());
						assertEquals(0, transactionEnvelopeInfo.getValidationCode());

						int j = 0;
						for (BlockInfo.TransactionEnvelopeInfo.TransactionActionInfo transactionActionInfo : transactionEnvelopeInfo
								.getTransactionActionInfos()) {
							++j;
							out("   Transaction action %d has response status %d", j,
									transactionActionInfo.getResponseStatus());
							out("   Transaction action %d has response message bytes as string: %s", j, printableString(
									new String(transactionActionInfo.getResponseMessageBytes(), UTF_8)));
							out("   Transaction action %d has %d endorsements", j,
									transactionActionInfo.getEndorsementsCount());
//							assertEquals(2, transactionActionInfo.getEndorsementsCount());

							for (int n = 0; n < transactionActionInfo.getEndorsementsCount(); ++n) {
								BlockInfo.EndorserInfo endorserInfo = transactionActionInfo.getEndorsementInfo(n);
								out("Endorser %d signature: %s", n, Hex.encodeHexString(endorserInfo.getSignature()));
								out("Endorser %d endorser: mspid %s \n certificate %s", n, endorserInfo.getMspid(),
										endorserInfo.getId());
							}
							out("   Transaction action %d has %d chaincode input arguments", j,
									transactionActionInfo.getChaincodeInputArgsCount());
							for (int z = 0; z < transactionActionInfo.getChaincodeInputArgsCount(); ++z) {
								out("     Transaction action %d has chaincode input argument %d is: %s", j, z,
										printableString(
												new String(transactionActionInfo.getChaincodeInputArgs(z), UTF_8)));
							}

							out("   Transaction action %d proposal response status: %d", j,
									transactionActionInfo.getProposalResponseStatus());
							out("   Transaction action %d proposal response payload: %s", j,
									printableString(new String(transactionActionInfo.getProposalResponsePayload())));

							String chaincodeIDName = transactionActionInfo.getChaincodeIDName();
							String chaincodeIDVersion = transactionActionInfo.getChaincodeIDVersion();
							String chaincodeIDPath = transactionActionInfo.getChaincodeIDPath();
							out("   Transaction action %d proposal chaincodeIDName: %s, chaincodeIDVersion: %s,  chaincodeIDPath: %s ",
									j, chaincodeIDName, chaincodeIDVersion, chaincodeIDPath);

							TxReadWriteSetInfo rwsetInfo = transactionActionInfo.getTxReadWriteSet();
							if (null != rwsetInfo) {
								out("   Transaction action %d has %d name space read write sets", j,
										rwsetInfo.getNsRwsetCount());

								for (TxReadWriteSetInfo.NsRwsetInfo nsRwsetInfo : rwsetInfo.getNsRwsetInfos()) {
									final String namespace = nsRwsetInfo.getNamespace();
									KvRwset.KVRWSet rws = nsRwsetInfo.getRwset();

									int rs = -1;
									for (KvRwset.KVRead readList : rws.getReadsList()) {
										rs++;

										out("     Namespace %s read set %d key %s  version [%d:%d]", namespace, rs,
												readList.getKey(), readList.getVersion().getBlockNum(),
												readList.getVersion().getTxNum());

										if ("bar".equals(channelId) && blockNumber == 2) {
											if ("example_cc_go".equals(namespace)) {
												if (rs == 0) {
													assertEquals("a", readList.getKey());
													assertEquals(1, readList.getVersion().getBlockNum());
													assertEquals(0, readList.getVersion().getTxNum());
												} else if (rs == 1) {
													assertEquals("b", readList.getKey());
													assertEquals(1, readList.getVersion().getBlockNum());
													assertEquals(0, readList.getVersion().getTxNum());
												} else {
													fail(format("unexpected readset %d", rs));
												}

											}
										}
									}

									rs = -1;
									for (KvRwset.KVWrite writeList : rws.getWritesList()) {
										rs++;
										String valAsString = printableString(
												new String(writeList.getValue().toByteArray(), UTF_8));

										out("     Namespace %s write set %d key %s has value '%s' ", namespace, rs,
												writeList.getKey(), valAsString);

										if ("bar".equals(channelId) && blockNumber == 2) {
											if (rs == 0) {
												assertEquals("a", writeList.getKey());
												assertEquals("400", valAsString);
											} else if (rs == 1) {
												assertEquals("b", writeList.getKey());
												assertEquals("400", valAsString);
											} else {
												fail(format("unexpected writeset %d", rs));
											}

										}
									}
								}
							}
						}
					}

					assertEquals(transactionCount, returnedBlock.getTransactionCount());

				}
			}
		} catch (InvalidProtocolBufferRuntimeException e) {
			throw e.getCause();
		}
	}

	// Returns a new client instance
	private static HFClient getTheClient() throws Exception {

		HFClient client = HFClient.createNewInstance();
		client.setCryptoSuite(CryptoSuite.Factory.getCryptoSuite());
		client.setUserContext(getAdminUser(TEST_ORG2));

		return client;
	}

	private static User getAdminUser(String orgName) throws Exception {

		return networkConfig.getPeerAdmin(orgName);
	}

	static Channel constructChannelTest(HFClient client, String channelName) throws Exception {
		// TODO Auto-generated method stub
		// Channel newChannel = client.getChannel(channelName);
		Channel newChannel = client.loadChannelFromConfig(channelName, networkConfig);
		if (newChannel == null) {
			throw new RuntimeException("Channel " + channelName + " is not defined in the config file!");
		}

		return newChannel.initialize();
	}

}
