package testingFabric;

import static java.lang.String.format;
import static java.nio.charset.StandardCharsets.UTF_8;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Paths;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.spec.InvalidKeySpecException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.CompletionException;
import java.util.concurrent.TimeUnit;

import org.hyperledger.fabric.protos.peer.Query.ChaincodeInfo;
import org.hyperledger.fabric.sdk.ChaincodeID;
import org.hyperledger.fabric.sdk.Channel;
import org.hyperledger.fabric.sdk.HFClient;
import org.hyperledger.fabric.sdk.NetworkConfig;
import org.hyperledger.fabric.sdk.Peer;
import org.hyperledger.fabric.sdk.ProposalResponse;
import org.hyperledger.fabric.sdk.QueryByChaincodeRequest;
import org.hyperledger.fabric.sdk.TransactionProposalRequest;
import org.hyperledger.fabric.sdk.TransactionRequest.Type;
import org.hyperledger.fabric.sdk.exception.CryptoException;
import org.hyperledger.fabric.sdk.exception.InvalidArgumentException;
import org.hyperledger.fabric.sdk.exception.ProposalException;
import org.hyperledger.fabric.sdk.security.CryptoSuite;
import org.hyperledger.fabric_ca.sdk.Attribute;
import org.hyperledger.fabric_ca.sdk.HFCAClient;
import org.hyperledger.fabric_ca.sdk.HFCAIdentity;
import org.hyperledger.fabric_ca.sdk.exception.EnrollmentException;
import org.hyperledger.fabric_ca.sdk.exception.IdentityException;
import org.springframework.security.crypto.bcrypt.BCrypt;

import java.sql.Timestamp;

import main.java.org.app.config.Config;
import testingFabric.SampleStore;

public class SystemExecution extends End2endIT {

	/*
	 * <dependency> <groupId>org.springframework.boot</groupId>
	 * <artifactId>spring-boot-starter-security</artifactId> </dependency>
	 */

	static File sampleStoreFile = new File(System.getProperty("java.io.tmpdir") + "/HFCSampletest.properties");
	static SampleStore sampleStore = null;
	static String user = "user3";
	Integer found = 0;
	private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
	private static final SimpleDateFormat timeDocFormate = new SimpleDateFormat("yyyy-MM-dd");
	static String[] userId = { "Id19041", "Id15123", "Id12415", "Id54131", "Id65412" };
	static String[] doctorId = { "dr52114", "dr61231", "dr12353", "dr64261", "dr57822" };
	static String[] hosId = { "hos6542", "hos8591", "hos7631", "hos5237", "hos6217" };
	static String[] action = { "Create", "Read", "Transfer" };
	static String[] records = { "<675781831108921667386124593484946328903463761786042331020431164816930848166051273189437849596013876085782309973283619197068353069490517879598843248952172048987208567195633323241993724032482284412183246212083032933448980918234765780597655448104320998220496251725707051956637476191916055682190220620352462503930894779657155557622082933819071419742067030870167134539432941302800907108654161482986997374465954602367413431366308997808620559902455907734867640280017471,302626988915318240448065150032047829682617698593309952613632455092410392099728275709962344503116553063968373255301128332276059497630988551508681739558734481166016431168422432495391229929769920109762989071308231949580605533484371045920413864597944466725313205863451889479458289144470808097422423693582302596018961315945640861883144361606593304407688172718023523538417135282315228943771050771280650001493622365338616508113392092346726391551965786918899038581529315,0, {x=2131685114418416075696321953979104613685866399014052278842011147105088457947786928865981517372894358922441345852225561723051209860468380460299576415030304449315012609343640536723945899990167798876828671352223033609052047903349690350467657773996915970381167252397445474107254666913773033779255554648872362693147713605467085460542509916640517656328664463952594977753700849064341928797289081566989172430640596337834420540161976589467896583161748402850607782024208308,y=1439384682799326456225698149239864264002042218672543570211706611229628118169609564095498305767002216218612962691686036253587721701643269907316281768170135458560730653723782353495929342245357568744296115191510969497690707240797801380649820608980071308357708364725669972969125862481604712145768679904416299992384076326093665308629638562886713803544136098961318338702433022160268927223872409846712954329631271073261287484009970122869322362967692022112829054849492771}>" };
	static String[] recordsHash = { "AEF1F803577BCFBC708EC5F391ADF05C657596B72DB0977C0CF81D095EDFC632",
			"D7330D6469A8AAE057BCE4A19A5A26ED81FB8D080182E85D3DBA8321B65AADCF",
			"63C4A1334473F338E470C53111827C4027F96B7292D425861A21FC124518D5D7",
			"D0E958A883D401E45F9F0AA378CA303179271EB8B399F6CCB96FBA767D847F1B",
			"7079E198FCD275196FB2811148C03ACF770287C989DF383EC655D3A40245DA06" };
	static String permission = "Succeed";
	static String[] consentVersion = { "Version 1", "Version 2", "Version 3", "Version 4" };
	static String[] consentHash = { "7B51A.787C9C5234D1B04E69F8D$3C6515DBED48DBA78F0E57BF033F44E3369B7C",
			"C02475AC116DBAC9775$D3C12B6A7929.2852D72F978F1491E49/3F52537DA70D4C",
			"A86E0C0B789BEB8$BB585220EBB7DA2B.57BDFD9AC/B9D6BFE53A1F67C493299C06",
			"42964C8C60BA2F$0F82E711EB454D9.B126DC6888FFCD1BFB6E92BF56060648D0A" };
	static String[] recordAddress = { "192.168.145.2/folder1_001", "192.158.123.4/folder2_cc512",
			"142.118.12.9/folder2_512", "192.18.3.4/folder21_51221", "192.8.1.4/folder2_565512",
			"121.187.13.4/folder121_7112", "192.58.123.4/folder25_612", "12.8.1.5/folder3_aw512",
			"1.2.13.30/98p93KcG1yq" };
	static String[] recordOwnerAdd = { "121.18.14.21/B7DA2B57BDFD9", "125.118.16.42/B04E69F8D",
			"112.54.14.75/BED48DBA7" };

	static String purposeTree = "[{" + "\"Education\":{" + "\"E-Statistic\":{" + "\"S-Survey\":\"null\"" + "},"
			+ "\"E-MedicineDiscovery\":\"null\"" + "}," + "\"MedicalTreatment\":{" + "\"M-Cancer\": \"null\","
			+ "\"M-Diabetic\":\"null\"," + "\"M-Education\":{" + "\"E-Reporting\": \"null\"" + "},"
			+ "\"M-Mental\":\"null\"" + "}," + "\"Insurance\":{" + "\"I-EvaluateInsuranceStatus\": \"null\"" + "}"
			+ "}]";

	static void searchUser(HFClient client, Channel channel, ChaincodeID chaincodeID, SampleOrg sampleOrg)
			throws InvalidArgumentException {
		String eCerHash = "7B51A.787C9C5234D1B04E69F8D$3C6515DBED48DBA78F0E57BF033F44E3369B7C";
		try {
//	client.setUserContext(sampleOrg.getUser(testUser1));

			out("Now query chaincode %s on channel %s for the value of dara expecting to see:", chaincodeID,
					channel.getName());
			QueryByChaincodeRequest queryByChaincodeRequest = client.newQueryProposalRequest();
			queryByChaincodeRequest.setFcn("searchBasedSalt");
			queryByChaincodeRequest.setArgs(eCerHash);
			queryByChaincodeRequest.setChaincodeID(chaincodeID);

			Collection<ProposalResponse> queryProposals;

			queryProposals = channel.queryByChaincode(queryByChaincodeRequest);

			for (ProposalResponse response : queryProposals) {
				String payload = response.getProposalResponse().getResponse().getPayload().toStringUtf8();
				if (response.getStatus() == ProposalResponse.Status.SUCCESS) {
					out("Successful query response Txid: %s from peer %s and value is %s", response.getTransactionID(),
							response.getPeer().getName(), payload);
				} else {
				}
			}
		} catch (Exception e) {
			throw new CompletionException(e);
		}
	}

	static void getHistoryForData(HFClient client, Channel channel, ChaincodeID chaincodeID, SampleOrg sampleOrg)
			throws InvalidArgumentException {
		String id = "20200707165948";
		try {
//	client.setUserContext(sampleOrg.getUser(testUser1));

			out("Now query chaincode %s on channel %s for the value of dara expecting to see:", chaincodeID,
					channel.getName());
			QueryByChaincodeRequest queryByChaincodeRequest = client.newQueryProposalRequest();
			queryByChaincodeRequest.setFcn("getHistoryForData");
			queryByChaincodeRequest.setArgs(id);
			queryByChaincodeRequest.setChaincodeID(chaincodeID);

			Collection<ProposalResponse> queryProposals;

			queryProposals = channel.queryByChaincode(queryByChaincodeRequest);

			for (ProposalResponse response : queryProposals) {
				String payload = response.getProposalResponse().getResponse().getPayload().toStringUtf8();
				if (response.getStatus() == ProposalResponse.Status.SUCCESS) {
					out("Successful query response Txid: %s from peer %s and value is %s", response.getTransactionID(),
							response.getPeer().getName(), payload);
				} else {
				}
			}
		} catch (Exception e) {
			throw new CompletionException(e);
		}
	}

	static String returnTimestamp() {
		// method 1
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		// format timestamp
		return sdf.format(timestamp);
	}

	static String timeDoc() {
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		// format timestamp
		return timeDocFormate.format(timestamp);

	}

	static String random(String str) {

		Random r = new Random();

		String data = "";

		if (str == "userId") {
			int randomNumber = r.nextInt(userId.length);
			data = userId[randomNumber];
		} else if (str == "doctorId") {
			int randomNumber = r.nextInt(doctorId.length);
			data = doctorId[randomNumber];
		} else if (str == "hosId") {
			int randomNumber = r.nextInt(hosId.length);
			data = hosId[randomNumber];
		} else if (str == "action") {
			int randomNumber = r.nextInt(action.length);
			data = action[randomNumber];
		} else if (str == "records") {
			int randomNumber = r.nextInt(records.length);
			data = records[randomNumber];
		} else if (str == "recordsHash") {
			int randomNumber = r.nextInt(recordsHash.length);
			data = recordsHash[randomNumber];
		} else if (str == "consentVersion") {
			int randomNumber = r.nextInt(consentVersion.length);
			data = consentVersion[randomNumber];
		} else if (str == "consentHash") {
			int randomNumber = r.nextInt(consentHash.length);
			data = consentHash[randomNumber];
		} else if (str == "recordAddress") {
			int randomNumber = r.nextInt(recordAddress.length);
			System.out.println("random: " + randomNumber);
			data = recordAddress[randomNumber];
		} else if (str == "recordOwnerAdd") {
			int randomNumber = r.nextInt(recordOwnerAdd.length);
			data = recordOwnerAdd[randomNumber];
		}

		return data;
	}

	static void initRecordsWithConsent(HFClient client, Channel channel, ChaincodeID chaincodeID, SampleOrg sampleOrg) {

		String Id = returnTimestamp();
		String eCerHash = "7B51A.787C9C5234D1B04E69F8D$3C6515DBED48DBA78F0E57BF033F44E3369B7C";
		String time = timeDoc();
		String rHashID = random("recordsHash");
		String recordsAdd = random("recordAddress");
		String encryptedSymmetricKey = random("records");
		String recordOwner = random("recordOwnerAdd");

		Collection<ProposalResponse> successful = new LinkedList<>();
		Collection<ProposalResponse> failed = new LinkedList<>();
		try {
			TransactionProposalRequest transactionProposalRequest = client.newTransactionProposalRequest();
			transactionProposalRequest.setChaincodeID(chaincodeID);
			transactionProposalRequest.setChaincodeLanguage(CHAIN_CODE_LANG);
			transactionProposalRequest.setFcn("createRecordsWithConsent");
			transactionProposalRequest.setArgs(Id, BCrypt.hashpw(eCerHash, BCrypt.gensalt()), time, rHashID, recordsAdd,
					encryptedSymmetricKey, recordOwner);
			Map<String, byte[]> tm2 = new HashMap<>();
			tm2.put("HyperLedgerFabric", "TransactionProposalRequest:JavaSDK".getBytes(UTF_8)); // Just some extra junk
																								// in transient map
			tm2.put("method", "TransactionProposalRequest".getBytes(UTF_8)); // ditto
			tm2.put("result", ":)".getBytes(UTF_8)); // This should be returned in the payload see chaincode why.
			if (Type.GO_LANG.equals(CHAIN_CODE_LANG) && testConfig.isFabricVersionAtOrAfter("1.2")) {

				expectedMoveRCMap.put(channel.getName(), random.nextInt(300) + 100L); // the chaincode will return this
																						// as status see chaincode why.
				tm2.put("rc", (expectedMoveRCMap.get(channel.getName()) + "").getBytes(UTF_8)); // This should be
																								// returned see
																								// chaincode why.
				// 400 and above results in the peer not endorsing!
			} else {
				expectedMoveRCMap.put(channel.getName(), 200L); // not really supported for Java or Node.
			}

			tm2.put(EXPECTED_EVENT_NAME, EXPECTED_EVENT_DATA); // This should trigger an event see chaincode why.

			transactionProposalRequest.setTransientMap(tm2);

			// Collection<ProposalResponse> transactionPropResp =
			// channel.sendTransactionProposalToEndorsers(transactionProposalRequest);
			Collection<ProposalResponse> transactionPropResp = channel
					.sendTransactionProposal(transactionProposalRequest, channel.getPeers());
			for (ProposalResponse response : transactionPropResp) {
				if (response.getStatus() == ProposalResponse.Status.SUCCESS) {
					out("Successful transaction proposal response Txid: %s from peer %s", response.getTransactionID(),
							response.getPeer().getName());
					successful.add(response);
				} else {
					failed.add(response);
				}
			}

			out("Received %d transaction proposal responses. Successful+verified: %d . Failed: %d",
					transactionPropResp.size(), successful.size(), failed.size());
			if (failed.size() > 0) {
				ProposalResponse firstTransactionProposalResponse = failed.iterator().next();
				fail("Not enough endorsers" + failed.size() + " endorser error: "
						+ firstTransactionProposalResponse.getMessage() + ". Was verified: "
						+ firstTransactionProposalResponse.isVerified());
			}
			channel.sendTransaction(successful).get(testConfig.getTransactionWaitTime(), TimeUnit.SECONDS);

		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	static void updateConsent(HFClient client, Channel channel, ChaincodeID chaincodeID, SampleOrg sampleOrg) {

		String Id = "20200707165948";

		Collection<ProposalResponse> successful = new LinkedList<>();
		Collection<ProposalResponse> failed = new LinkedList<>();
		try {
			TransactionProposalRequest transactionProposalRequest = client.newTransactionProposalRequest();
			transactionProposalRequest.setChaincodeID(chaincodeID);
			transactionProposalRequest.setChaincodeLanguage(CHAIN_CODE_LANG);
			transactionProposalRequest.setFcn("updateConsent");
			transactionProposalRequest.setArgs(Id);
			Map<String, byte[]> tm2 = new HashMap<>();
			tm2.put("HyperLedgerFabric", "TransactionProposalRequest:JavaSDK".getBytes(UTF_8)); // Just some extra junk
																								// in transient map
			tm2.put("method", "TransactionProposalRequest".getBytes(UTF_8)); // ditto
			tm2.put("result", ":)".getBytes(UTF_8)); // This should be returned in the payload see chaincode why.
			if (Type.GO_LANG.equals(CHAIN_CODE_LANG) && testConfig.isFabricVersionAtOrAfter("1.2")) {

				expectedMoveRCMap.put(channel.getName(), random.nextInt(300) + 100L); // the chaincode will return this
																						// as status see chaincode why.
				tm2.put("rc", (expectedMoveRCMap.get(channel.getName()) + "").getBytes(UTF_8)); // This should be
																								// returned see
																								// chaincode why.
				// 400 and above results in the peer not endorsing!
			} else {
				expectedMoveRCMap.put(channel.getName(), 200L); // not really supported for Java or Node.
			}

			tm2.put(EXPECTED_EVENT_NAME, EXPECTED_EVENT_DATA); // This should trigger an event see chaincode why.

			transactionProposalRequest.setTransientMap(tm2);

			// Collection<ProposalResponse> transactionPropResp =
			// channel.sendTransactionProposalToEndorsers(transactionProposalRequest);
			Collection<ProposalResponse> transactionPropResp = channel
					.sendTransactionProposal(transactionProposalRequest, channel.getPeers());
			for (ProposalResponse response : transactionPropResp) {
				if (response.getStatus() == ProposalResponse.Status.SUCCESS) {
					out("Successful transaction proposal response Txid: %s from peer %s", response.getTransactionID(),
							response.getPeer().getName());
					successful.add(response);
				} else {
					failed.add(response);
				}
			}

			out("Received %d transaction proposal responses. Successful+verified: %d . Failed: %d",
					transactionPropResp.size(), successful.size(), failed.size());
			if (failed.size() > 0) {
				ProposalResponse firstTransactionProposalResponse = failed.iterator().next();
				fail("Not enough endorsers" + failed.size() + " endorser error: "
						+ firstTransactionProposalResponse.getMessage() + ". Was verified: "
						+ firstTransactionProposalResponse.isVerified());
			}
			channel.sendTransaction(successful).get(testConfig.getTransactionWaitTime(), TimeUnit.SECONDS);

		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	static void initMarble(HFClient client, Channel channel, ChaincodeID chaincodeID, SampleOrg sampleOrg) {

		String Id = returnTimestamp();
		String time = timeDoc();
		String pID = random("userId");
		String dID = random("doctorId");
		String hID = random("hosId");
		String aID = random("action");
		String rID = random("records");
		String rHashID = random("recordsHash");
		String ConsentV = random("consentVersion");
		String ConsentH = random("consentHash");
		String permissionCheck = permission;

		Collection<ProposalResponse> successful = new LinkedList<>();
		Collection<ProposalResponse> failed = new LinkedList<>();
		try {
			TransactionProposalRequest transactionProposalRequest = client.newTransactionProposalRequest();
			transactionProposalRequest.setChaincodeID(chaincodeID);
			transactionProposalRequest.setChaincodeLanguage(CHAIN_CODE_LANG);
			transactionProposalRequest.setFcn("initRecords");
			transactionProposalRequest.setArgs(Id, pID, time, rID, hID, dID, aID, permissionCheck, rHashID, ConsentV,
					ConsentH);
			Map<String, byte[]> tm2 = new HashMap<>();
			tm2.put("HyperLedgerFabric", "TransactionProposalRequest:JavaSDK".getBytes(UTF_8)); // Just some extra junk
																								// in transient map
			tm2.put("method", "TransactionProposalRequest".getBytes(UTF_8)); // ditto
			tm2.put("result", ":)".getBytes(UTF_8)); // This should be returned in the payload see chaincode why.
			if (Type.GO_LANG.equals(CHAIN_CODE_LANG) && testConfig.isFabricVersionAtOrAfter("1.2")) {

				expectedMoveRCMap.put(channel.getName(), random.nextInt(300) + 100L); // the chaincode will return this
																						// as status see chaincode why.
				tm2.put("rc", (expectedMoveRCMap.get(channel.getName()) + "").getBytes(UTF_8)); // This should be
																								// returned see
																								// chaincode why.
				// 400 and above results in the peer not endorsing!
			} else {
				expectedMoveRCMap.put(channel.getName(), 200L); // not really supported for Java or Node.
			}

			tm2.put(EXPECTED_EVENT_NAME, EXPECTED_EVENT_DATA); // This should trigger an event see chaincode why.

			transactionProposalRequest.setTransientMap(tm2);

			// Collection<ProposalResponse> transactionPropResp =
			// channel.sendTransactionProposalToEndorsers(transactionProposalRequest);
			Collection<ProposalResponse> transactionPropResp = channel
					.sendTransactionProposal(transactionProposalRequest, channel.getPeers());
			for (ProposalResponse response : transactionPropResp) {
				if (response.getStatus() == ProposalResponse.Status.SUCCESS) {
					out("Successful transaction proposal response Txid: %s from peer %s", response.getTransactionID(),
							response.getPeer().getName());
					successful.add(response);
				} else {
					failed.add(response);
				}
			}

			out("Received %d transaction proposal responses. Successful+verified: %d . Failed: %d",
					transactionPropResp.size(), successful.size(), failed.size());
			if (failed.size() > 0) {
				ProposalResponse firstTransactionProposalResponse = failed.iterator().next();
				fail("Not enough endorsers" + failed.size() + " endorser error: "
						+ firstTransactionProposalResponse.getMessage() + ". Was verified: "
						+ firstTransactionProposalResponse.isVerified());
			}
			channel.sendTransaction(successful).get(testConfig.getTransactionWaitTime(), TimeUnit.SECONDS);

		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	static void initPurposeTree(HFClient client, Channel channel, ChaincodeID chaincodeID, SampleOrg sampleOrg) {

		String consentID = "v1.3";

		Collection<ProposalResponse> successful = new LinkedList<>();
		Collection<ProposalResponse> failed = new LinkedList<>();
		try {
			TransactionProposalRequest transactionProposalRequest = client.newTransactionProposalRequest();
			transactionProposalRequest.setChaincodeID(chaincodeID);
			transactionProposalRequest.setChaincodeLanguage(CHAIN_CODE_LANG);
			transactionProposalRequest.setFcn("uploadPurposeTree");
			transactionProposalRequest.setArgs(consentID, purposeTree);
			Map<String, byte[]> tm2 = new HashMap<>();
			tm2.put("HyperLedgerFabric", "TransactionProposalRequest:JavaSDK".getBytes(UTF_8)); // Just some extra junk
																								// in transient map
			tm2.put("method", "TransactionProposalRequest".getBytes(UTF_8)); // ditto
			tm2.put("result", ":)".getBytes(UTF_8)); // This should be returned in the payload see chaincode why.
			if (Type.GO_LANG.equals(CHAIN_CODE_LANG) && testConfig.isFabricVersionAtOrAfter("1.2")) {

				expectedMoveRCMap.put(channel.getName(), random.nextInt(300) + 100L); // the chaincode will return this
																						// as status see chaincode why.
				tm2.put("rc", (expectedMoveRCMap.get(channel.getName()) + "").getBytes(UTF_8)); // This should be
																								// returned see
																								// chaincode why.
				// 400 and above results in the peer not endorsing!
			} else {
				expectedMoveRCMap.put(channel.getName(), 200L); // not really supported for Java or Node.
			}

			tm2.put(EXPECTED_EVENT_NAME, EXPECTED_EVENT_DATA); // This should trigger an event see chaincode why.

			transactionProposalRequest.setTransientMap(tm2);

			// Collection<ProposalResponse> transactionPropResp =
			// channel.sendTransactionProposalToEndorsers(transactionProposalRequest);
			Collection<ProposalResponse> transactionPropResp = channel
					.sendTransactionProposal(transactionProposalRequest, channel.getPeers());
			for (ProposalResponse response : transactionPropResp) {
				if (response.getStatus() == ProposalResponse.Status.SUCCESS) {
					out("Successful transaction proposal response Txid: %s from peer %s", response.getTransactionID(),
							response.getPeer().getName());
					successful.add(response);
				} else {
					failed.add(response);
				}
			}

			out("Received %d transaction proposal responses. Successful+verified: %d . Failed: %d",
					transactionPropResp.size(), successful.size(), failed.size());
			if (failed.size() > 0) {
				ProposalResponse firstTransactionProposalResponse = failed.iterator().next();
				fail("Not enough endorsers" + failed.size() + " endorser error: "
						+ firstTransactionProposalResponse.getMessage() + ". Was verified: "
						+ firstTransactionProposalResponse.isVerified());
			}
			channel.sendTransaction(successful).get(testConfig.getTransactionWaitTime(), TimeUnit.SECONDS);

		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	static void accessRequest(HFClient client, Channel channel, ChaincodeID chaincodeID, SampleOrg sampleOrg)
			throws InvalidArgumentException {

//		String[] arg = { "c02475ac116dbac9775d3c12b6a79292852d72f978f1491e493f52537da70d4c" };
		String[] arg = { "7B51A.787C9C5234D1B04E69F8D$3C6515DBED48DBA78F0E57BF033F44E3369B7C", "Nurse", "Read",
				"Education" };

		try {
//	client.setUserContext(sampleOrg.getUser(testUser1));

			out("Now query chaincode %s on channel %s for the value of dara expecting to see:", chaincodeID,
					channel.getName());
			QueryByChaincodeRequest queryByChaincodeRequest = client.newQueryProposalRequest();
			queryByChaincodeRequest.setArgs(arg); // test using bytes as args. End2end uses Strings.
			queryByChaincodeRequest.setFcn("reinforcementConsent");
			queryByChaincodeRequest.setChaincodeID(chaincodeID);

			Collection<ProposalResponse> queryProposals;

			queryProposals = channel.queryByChaincode(queryByChaincodeRequest);

			for (ProposalResponse response : queryProposals) {
				String payload = response.getProposalResponse().getResponse().getPayload().toStringUtf8();
				if (response.getStatus() == ProposalResponse.Status.SUCCESS) {
					out("Successful query response Txid: %s from peer %s and value is %s", response.getTransactionID(),
							response.getPeer().getName(), payload);
				} else {
				}
			}
		} catch (Exception e) {
			throw new CompletionException(e);
		}
	}

	static void queryRecordsByKeyandExpectedValue(HFClient client, Channel channel, ChaincodeID chaincodeID,
			SampleOrg sampleOrg) throws InvalidArgumentException {

//		String[] arg = { "c02475ac116dbac9775d3c12b6a79292852d72f978f1491e493f52537da70d4c" };
		String[] arg = { "ConsentVersion", "v1.3" };

		try {
//	client.setUserContext(sampleOrg.getUser(testUser1));

			out("Now query chaincode %s on channel %s for the value of dara expecting to see:", chaincodeID,
					channel.getName());
			QueryByChaincodeRequest queryByChaincodeRequest = client.newQueryProposalRequest();
			queryByChaincodeRequest.setArgs(arg); // test using bytes as args. End2end uses Strings.
			queryByChaincodeRequest.setFcn("queryRecordsByKeyandExpectedValue");
			queryByChaincodeRequest.setChaincodeID(chaincodeID);

			Collection<ProposalResponse> queryProposals;

			queryProposals = channel.queryByChaincode(queryByChaincodeRequest);

			for (ProposalResponse response : queryProposals) {
				String payload = response.getProposalResponse().getResponse().getPayload().toStringUtf8();
				if (response.getStatus() == ProposalResponse.Status.SUCCESS) {
					out("Successful query response Txid: %s from peer %s and value is %s", response.getTransactionID(),
							response.getPeer().getName(), payload);
				} else {
				}
			}
		} catch (Exception e) {
			throw new CompletionException(e);
		}
	}

	void queryListIdentity(SampleStore samplestore)
			throws IllegalAccessException, InstantiationException, ClassNotFoundException, CryptoException,
			InvalidArgumentException, NoSuchMethodException, InvocationTargetException, EnrollmentException,
			org.hyperledger.fabric_ca.sdk.exception.InvalidArgumentException, IdentityException, IOException,
			NoSuchAlgorithmException, NoSuchProviderException, InvalidKeySpecException {

		out("***** Enrolled Users *****");
		for (SampleOrg sampleOrg : testSampleOrgs) {

			HFCAClient ca = HFCAClient.createNewInstance(
					testConfig.getIntegrationTestsSampleOrg(sampleOrg.getName()).getCALocation(),
					testConfig.getIntegrationTestsSampleOrg(sampleOrg.getName()).getCAProperties());

			final String orgName = sampleOrg.getName();
			final String mspid = sampleOrg.getMSPID();
			ca.setCryptoSuite(CryptoSuite.Factory.getCryptoSuite());

			admin = samplestore.getMember(TEST_ADMIN_NAME, orgName);
			if (!admin.isEnrolled()) { // Preregistered admin only needs to be enrolled with Fabric caClient.
				admin.setEnrollment(ca.enroll(admin.getName(), "adminpw"));
				admin.setRoles(roles);
				admin.setMspId(mspid);
			}

			Collection<HFCAIdentity> foundIdentities = ca.getHFCAIdentities(admin);
			String[] expectedIdenities = new String[] { user };

			for (HFCAIdentity id : foundIdentities) {
				for (String name : expectedIdenities) {
//					System.out.println(sampleOrg.getName() + " " + id.getEnrollmentId() + " " + id.getAffiliation()
//							+ " " + " " + id.getType());
					for (Attribute e : id.getAttributes()) {
//						System.out.println(e.toJsonObject());

					}
					if (id.getEnrollmentId().equals(name)) {
						found++;
					}
				}
			}

			if (found != 0) {

				System.out.println("This " + user + " has already register!");

			}

//			String directoryPath = "usersTest";
//			String privatekeyfile = directoryPath + "/" + doctorOrg1 + "_sk" + ".pem";
//			String publickeyfile = directoryPath + "/" + doctorOrg1 + ".pem";

			SampleUser user = samplestore.getMember(doctorOrg1, sampleOrg.getName());
//			SampleUser user = null;
//			try {
//				user = samplestore.getMemberEdition(testUser1, sampleOrg.getName(), mspid, privatekeyfile,
//						publickeyfile);
//			} catch (GeneralSecurityException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
			final String sampleOrgName = sampleOrg.getName();
			final String sampleOrgDomainName = sampleOrg.getDomainName();

			SampleUser peerOrgAdmin = samplestore.getMember(sampleOrgName + "Admin", sampleOrgName,
					sampleOrg.getMSPID(),
					Util.findFileSk(Paths
							.get(testConfig.getTestChannelPath(), "crypto-config/peerOrganizations/",
									sampleOrgDomainName, format("/users/Admin@%s/msp/keystore", sampleOrgDomainName))
							.toFile()),
					Paths.get(testConfig.getTestChannelPath(), "crypto-config/peerOrganizations/", sampleOrgDomainName,
							format("/users/Admin@%s/msp/signcerts/Admin@%s-cert.pem", sampleOrgDomainName,
									sampleOrgDomainName))
							.toFile());
			sampleOrg.setPeerAdmin(peerOrgAdmin); // A special user that can create channels, join peers and install
			sampleOrg.addUser(user); // chaincode
			sampleOrg.setAdmin(admin); // The admin of this org --

		}

	}

	private static boolean checkInstalledChaincode(HFClient client, Peer peer, String ccName, String ccPath,
			String ccVersion) throws InvalidArgumentException, ProposalException {

		out("Checking installed chaincode: %s, at version: %s, on peer: %s", ccName, ccVersion, peer.getName());
		List<ChaincodeInfo> ccinfoList = client.queryInstalledChaincodes(peer);
		System.out.println(ccinfoList);

		boolean found = false;

		for (ChaincodeInfo ccifo : ccinfoList) {

			if (ccPath != null) {
				found = ccName.equals(ccifo.getName()) && ccPath.equals(ccifo.getPath())
						&& ccVersion.equals(ccifo.getVersion());
				if (found) {
					break;
				}
			}

			found = ccName.equals(ccifo.getName()) && ccVersion.equals(ccifo.getVersion());
			if (found) {
				break;
			}

		}

		return found;
	}

	private static boolean checkInstantiatedChaincode(Channel channel, Peer peer, String ccName, String ccPath,
			String ccVersion) throws InvalidArgumentException, ProposalException {
		out("Checking instantiated chaincode: %s, at version: %s, on peer: %s", ccName, ccVersion, peer.getName());
		List<ChaincodeInfo> ccinfoList = channel.queryInstantiatedChaincodes(peer);

		boolean found = false;

		for (ChaincodeInfo ccifo : ccinfoList) {

			if (ccPath != null) {
				found = ccName.equals(ccifo.getName()) && ccPath.equals(ccifo.getPath())
						&& ccVersion.equals(ccifo.getVersion());
				if (found) {
					break;
				}
			}

			found = ccName.equals(ccifo.getName()) && ccVersion.equals(ccifo.getVersion());
			if (found) {
				break;
			}

		}

		return found;
	}
}
