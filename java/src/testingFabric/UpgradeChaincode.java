package testingFabric;

import static org.junit.Assert.fail;

import java.nio.file.Paths;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.hyperledger.fabric.sdk.ChaincodeID;
import org.hyperledger.fabric.sdk.Channel;
import org.hyperledger.fabric.sdk.HFClient;
import org.hyperledger.fabric.sdk.InstallProposalRequest;
import org.hyperledger.fabric.sdk.Peer;
import org.hyperledger.fabric.sdk.ProposalResponse;
import org.hyperledger.fabric.sdk.UpgradeProposalRequest;
import org.hyperledger.fabric.sdk.ChaincodeResponse.Status;
import main.java.org.app.config.Config;

public class UpgradeChaincode extends End2endIT {

	public static void upgradeChaincode(HFClient client, Channel channel, ChaincodeID chaincodeID_1, ChaincodeID chaincodeID_2,
			Collection<SampleOrg> sampleOrg) {

		Collection<ProposalResponse> successful = new LinkedList<>();
		Collection<ProposalResponse> failed = new LinkedList<>();
		Collection<Peer> peersFromOrg = new LinkedList<>();

		try {
			for (SampleOrg sampOrg : sampleOrg) {
				client.setUserContext(sampOrg.getPeerAdmin());
				peersFromOrg.clear();
				for (String name : sampOrg.getPeerNames()) {
					// for peer0.org2 location is null
					String peerLocation = sampOrg.getPeerLocation(name);
					Properties peerProperties = testConfig.getPeerProperties(name);
					peersFromOrg.add(client.newPeer(name, peerLocation, peerProperties));
				}

				InstallProposalRequest installProposalRequest = client.newInstallProposalRequest();
				installProposalRequest.setChaincodeID(chaincodeID_1);
				//// For GO language and serving just a single user, chaincodeSource is mostly
				//// likely the users GOPATH
				installProposalRequest.setChaincodeSourceLocation(Paths.get(Config.CHAINCODE_ROOT_DIR).toFile());
				installProposalRequest.setChaincodeVersion(chaincodeID_2.getVersion());
				installProposalRequest.setProposalWaitTime(testConfig.getProposalWaitTime());
				installProposalRequest.setChaincodeLanguage(CHAIN_CODE_LANG);

				out("Sending install proposal for channel: %s", channel.getName());

				////////////////////////////
				// only a client from the same org as the peer can issue an install request
				int numInstallProposal = 0;

				Collection<ProposalResponse> responses;
				numInstallProposal = numInstallProposal + peersFromOrg.size();

				// Deprecated use v2.0 Lifecycle chaincode management.
				responses = client.sendInstallProposal(installProposalRequest, peersFromOrg);
				System.out.println(responses.size());
				successful.clear();
				failed.clear();
				for (ProposalResponse response : responses) {
					if (response.getStatus() == Status.SUCCESS) {
						out("Successful install proposal response Txid: %s from peer %s", response.getTransactionID(),
								response.getPeer().getName());
						successful.add(response);
					} else {
						failed.add(response);
					}
				}

				out("Received %d install proposal responses. Successful+verified: %d . Failed: %d", numInstallProposal,
						successful.size(), failed.size());

				if (failed.size() > 0) {
					ProposalResponse first = failed.iterator().next();
					fail("Not enough endorsers for install :" + successful.size() + ".  " + first.getMessage());
				}
			}

			UpgradeProposalRequest upgradeProposalRequest = client.newUpgradeProposalRequest();
			upgradeProposalRequest.setChaincodeID(chaincodeID_2);
			upgradeProposalRequest.setChaincodeLanguage(CHAIN_CODE_LANG);
			upgradeProposalRequest.setProposalWaitTime(DEPLOYWAITTIME);
			upgradeProposalRequest.setFcn("Init");
			upgradeProposalRequest.setArgs(new String[] {});
			Map<String, byte[]> tmap = new HashMap<>();
			tmap.put("test", "data".getBytes());
			upgradeProposalRequest.setTransientMap(tmap);

			out("Sending Upgrade proposal for channel: %s", channel.getName());

			// Collection<ProposalResponse> transactionPropResp =
			// channel.sendTransactionProposalToEndorsers(transactionProposalRequest);
			Collection<ProposalResponse> transactionPropResp = channel.sendUpgradeProposal(upgradeProposalRequest,
					channel.getPeers());
			successful.clear();
			failed.clear();
			for (ProposalResponse response : transactionPropResp) {
				if (response.getStatus() == ProposalResponse.Status.SUCCESS) {
					out("Successful transaction proposal response Txid: %s from peer %s", response.getTransactionID(),
							response.getPeer().getName());
					successful.add(response);
				} else {
					failed.add(response);
				}
			}

			out("Received %d transaction proposal responses. Successful+verified: %d . Failed: %d",
					transactionPropResp.size(), successful.size(), failed.size());
			if (failed.size() > 0) {
				ProposalResponse firstTransactionProposalResponse = failed.iterator().next();
				fail("Not enough endorsers" + failed.size() + " endorser error: "
						+ firstTransactionProposalResponse.getMessage() + ". Was verified: "
						+ firstTransactionProposalResponse.isVerified());
			}

			channel.sendTransaction(successful).get(testConfig.getTransactionWaitTime(), TimeUnit.MINUTES);

		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e);
		}
	}
}
