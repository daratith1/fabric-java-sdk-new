/****************************************************** 
 *  Copyright 2018 IBM Corporation 
 *  Licensed under the Apache License, Version 2.0 (the "License"); 
 *  you may not use this file except in compliance with the License. 
 *  You may obtain a copy of the License at 
 *  http://www.apache.org/licenses/LICENSE-2.0 
 *  Unless required by applicable law or agreed to in writing, software 
 *  distributed under the License is distributed on an "AS IS" BASIS, 
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 *  See the License for the specific language governing permissions and 
 *  limitations under the License.
 */
package main.java.org.app.user;

import main.java.org.app.client.CAClient;
import main.java.org.app.config.Config;
import main.java.org.app.util.Util;

public class RegisterEnrollUser {

	public static void main(String args[]) {
		try {
			Util.cleanUp();
			String caUrlOrg1 = Config.CA_ORG1_URL;
			CAClient caClientOrg1 = new CAClient(caUrlOrg1, null);
			
			String caUrlOrg2 = Config.CA_ORG2_URL;
			CAClient caClientOrg2 = new CAClient(caUrlOrg2, null);
			
			// Enroll Admin to Org1MSP
			UserContext adminUserContextOrg1 = new UserContext();
			adminUserContextOrg1.setName(Config.ADMIN);
			adminUserContextOrg1.setAffiliation(Config.ORG1);
			adminUserContextOrg1.setMspId(Config.ORG1_MSP);
			caClientOrg1.setAdminUserContext(adminUserContextOrg1);
			adminUserContextOrg1 = caClientOrg1.enrollAdminUser(Config.ADMIN, Config.ADMIN_PASSWORD);
			
			// Enroll Admin to org2MSP
			UserContext adminUserContextOrg2 = new UserContext();
			adminUserContextOrg2.setName(Config.ADMIN);
			adminUserContextOrg2.setAffiliation(Config.ORG2);
			adminUserContextOrg2.setMspId(Config.ORG2_MSP);
			caClientOrg2.setAdminUserContext(adminUserContextOrg2);
			adminUserContextOrg2 = caClientOrg2.enrollAdminUser(Config.ADMIN, Config.ADMIN_PASSWORD);
			
			// Register and Enroll user to Org1MSP
			UserContext userContext11 = new UserContext();
			UserContext userContext12 = new UserContext();
			String name11 = "test_user11"+System.currentTimeMillis();
			String name12 = "test_user12"+System.currentTimeMillis();;
			
			userContext11.setName(name11);
			userContext11.setAffiliation(Config.ORG1);
			userContext11.setMspId(Config.ORG1_MSP);
			userContext12.setName(name12);
			userContext12.setAffiliation(Config.ORG1);
			userContext12.setMspId(Config.ORG1_MSP);
			

			String eSecret_name1 = caClientOrg1.registerUser(name11, Config.ORG1);
			String eSecret_name2 = caClientOrg1.registerUser(name12, Config.ORG1);
			
			System.out.println("register 1" + eSecret_name1);

			userContext11 = caClientOrg1.enrollUser(userContext11, eSecret_name1);
			userContext12 = caClientOrg1.enrollUser(userContext12, eSecret_name2);
			
			System.out.println("enrollment ID" + userContext11.getEnrollment().getCert());

//			System.out.println("U 1 " + userContext11.enrollment.getCert() + "SK 1" + userContext11.enrollment.getKey());
//			System.out.println("U 2 " + userContext12.enrollment.getCert());
			
			// Register and Enroll user to Org1MSP
						UserContext userContext21 = new UserContext();
						UserContext userContext22 = new UserContext();
						String name21 = "test_user21"+System.currentTimeMillis();;
						String name22 = "test_user22"+System.currentTimeMillis();;
						
						userContext21.setName(name21);
						userContext21.setAffiliation(Config.ORG2);
						userContext21.setMspId(Config.ORG2_MSP);
						userContext22.setName(name22);
						userContext22.setAffiliation(Config.ORG2);
						userContext22.setMspId(Config.ORG2_MSP);
						

						String eSecret_name21 = caClientOrg2.registerUser(name21, Config.ORG2);
						String eSecret_name22 = caClientOrg2.registerUser(name22, Config.ORG2);

						userContext21 = caClientOrg2.enrollUser(userContext21, eSecret_name21);
						userContext22 = caClientOrg2.enrollUser(userContext22, eSecret_name22);
						
						

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
