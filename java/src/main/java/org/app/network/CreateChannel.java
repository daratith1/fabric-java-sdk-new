/****************************************************** 
 *  Copyright 2018 IBM Corporation 
 *  Licensed under the Apache License, Version 2.0 (the "License"); 
 *  you may not use this file except in compliance with the License. 
 *  You may obtain a copy of the License at 
 *  http://www.apache.org/licenses/LICENSE-2.0 
 *  Unless required by applicable law or agreed to in writing, software 
 *  distributed under the License is distributed on an "AS IS" BASIS, 
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 *  See the License for the specific language governing permissions and 
 *  limitations under the License.
 */
package main.java.org.app.network;

import static org.hyperledger.fabric.sdk.Channel.PeerOptions.createPeerOptions;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumSet;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import main.java.org.app.client.ChannelClient;
import main.java.org.app.client.FabricClient;
import main.java.org.app.config.Config;
import main.java.org.app.user.UserContext;
import main.java.org.app.util.Util;
import org.hyperledger.fabric.sdk.Channel;
import org.hyperledger.fabric.sdk.ChannelConfiguration;
import org.hyperledger.fabric.sdk.Enrollment;
import org.hyperledger.fabric.sdk.Orderer;
import org.hyperledger.fabric.sdk.Peer;
import org.hyperledger.fabric.sdk.ProposalResponse;
import org.hyperledger.fabric.sdk.Peer.PeerRole;
import org.hyperledger.fabric.sdk.TransactionRequest.Type;
import org.hyperledger.fabric.sdk.exception.ChaincodeEndorsementPolicyParseException;
import org.hyperledger.fabric.sdk.exception.InvalidArgumentException;
import org.hyperledger.fabric.sdk.exception.ProposalException;
import org.hyperledger.fabric.sdk.exception.TransactionException;
import org.hyperledger.fabric.sdk.security.CryptoSuite;


public class CreateChannel {
	
	static UserContext org1Admin = new UserContext();
	static File pkFolder1 = new File(Config.ORG1_USR_ADMIN_PK);
	static File[] pkFiles1 = pkFolder1.listFiles();
	static File certFolder1 = new File(Config.ORG1_USR_ADMIN_CERT);
	static File[] certFiles1 = certFolder1.listFiles();
	
	static UserContext org2Admin = new UserContext();
	static File pkFolder2 = new File(Config.ORG2_USR_ADMIN_PK);
	static File[] pkFiles2 = pkFolder2.listFiles();
	static File certFolder2 = new File(Config.ORG2_USR_ADMIN_CERT);
	static File[] certFiles2 = certFolder2.listFiles();
	
	static Peer peer0_org1, peer1_org1, peer0_org2, peer1_org2;
	static FabricClient fabClient;
	static Channel mychannel;

	public static void main(String[] args) throws InvalidArgumentException, ProposalException, ChaincodeEndorsementPolicyParseException, IOException {
		
		CreateChannelHLF();
//        DeployChaincode();
//        DeployChaincodeJava();
        
		
	}
	
	private static void DeployChaincode () throws InvalidArgumentException, ProposalException, IOException, ChaincodeEndorsementPolicyParseException{
		
		try {
		List<Peer> org1Peers = new ArrayList<Peer>();
		org1Peers.add(peer0_org1);
		org1Peers.add(peer1_org1);
		
		List<Peer> org2Peers = new ArrayList<Peer>();
		org2Peers.add(peer0_org2);
		org2Peers.add(peer1_org2);
		
		fabClient.getInstance().setUserContext(org1Admin);
		Collection<ProposalResponse> response = fabClient.deployChainCode(Config.CHAINCODE_1_NAME,
				Config.CHAINCODE_1_PATH, Config.CHAINCODE_ROOT_DIR, Type.GO_LANG.toString(),
				Config.CHAINCODE_1_VERSION, org1Peers);
		
		for (ProposalResponse res : response) {
			Logger.getLogger(CreateChannel.class.getName()).log(Level.INFO,
					Config.CHAINCODE_1_NAME + "- Chain code deployment org 1 " + res.getStatus());
		}

		fabClient.getInstance().setUserContext(org2Admin);
		
		response = fabClient.deployChainCode(Config.CHAINCODE_1_NAME,
				Config.CHAINCODE_1_PATH, Config.CHAINCODE_ROOT_DIR, Type.GO_LANG.toString(),
				Config.CHAINCODE_1_VERSION, org2Peers);
		
		
		for (ProposalResponse res : response) {
			Logger.getLogger(CreateChannel.class.getName()).log(Level.INFO,
					Config.CHAINCODE_1_NAME + "- Chain code deployment org 2 " + res.getStatus());
		}
		
		ChannelClient channelClient = new ChannelClient(mychannel.getName(), mychannel, fabClient);

		String[] arguments = { "" };
		response = channelClient.instantiateChainCode(Config.CHAINCODE_1_NAME, Config.CHAINCODE_1_VERSION,
				Config.CHAINCODE_1_PATH, Type.GO_LANG.toString(), "init", arguments, null);

		for (ProposalResponse res : response) {
			Logger.getLogger(CreateChannel.class.getName()).log(Level.INFO,
					Config.CHAINCODE_1_NAME + "- Chain code instantiation " + res.getStatus());
		}
		
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	private static void DeployChaincodeJava () throws InvalidArgumentException, ProposalException, IOException, ChaincodeEndorsementPolicyParseException{
		
		try {
		List<Peer> org1Peers = new ArrayList<Peer>();
		org1Peers.add(peer0_org1);
		org1Peers.add(peer1_org1);
		
		List<Peer> org2Peers = new ArrayList<Peer>();
		org2Peers.add(peer0_org2);
		org2Peers.add(peer1_org2);
		
		fabClient.getInstance().setUserContext(org1Admin);
		Collection<ProposalResponse> response = fabClient.deployChainCode(Config.JavaCHAINCODE_NAME,
				Config.JAVACHAINCODE_SimpleAsset_DIR, Config.JAVACHAINCODE_ROOT_DIR, Type.JAVA.toString(),
				Config.CHAINCODE_1_VERSION, org1Peers);
		
		for (ProposalResponse res : response) {
			Logger.getLogger(CreateChannel.class.getName()).log(Level.INFO,
					Config.JavaCHAINCODE_NAME + "- Chain code deployment org 1 " + res.getStatus());
		}

		fabClient.getInstance().setUserContext(org2Admin);
		
		response = fabClient.deployChainCode(Config.JavaCHAINCODE_NAME,
				Config.JAVACHAINCODE_SimpleAsset_DIR, Config.JAVACHAINCODE_ROOT_DIR, Type.JAVA.toString(),
				Config.CHAINCODE_1_VERSION, org2Peers);
		
		
		for (ProposalResponse res : response) {
			Logger.getLogger(CreateChannel.class.getName()).log(Level.INFO,
					Config.JavaCHAINCODE_NAME + "- Chain code deployment org 2 " + res.getStatus());
		}
		
		ChannelClient channelClient = new ChannelClient(mychannel.getName(), mychannel, fabClient);

		String[] arguments = { "" };
		response = channelClient.instantiateChainCode(Config.JavaCHAINCODE_NAME, Config.CHAINCODE_1_VERSION,
				Config.JAVACHAINCODE_SimpleAsset_DIR, Type.JAVA.toString(), "init", arguments, null);

		for (ProposalResponse res : response) {
			Logger.getLogger(CreateChannel.class.getName()).log(Level.INFO,
					Config.JavaCHAINCODE_NAME + "- Chain code instantiation " + res.getStatus() + "Massage: " + res.getMessage());
		}
		
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	private static void CreateChannelHLF() {
		try {
			CryptoSuite.Factory.getCryptoSuite();
			Util.cleanUp();
			// Construct Channel
			Enrollment enrollOrg1Admin = Util.getEnrollment(Config.ORG1_USR_ADMIN_PK, pkFiles1[0].getName(),
					Config.ORG1_USR_ADMIN_CERT, certFiles1[0].getName());
			org1Admin.setEnrollment(enrollOrg1Admin);
			org1Admin.setMspId(Config.ORG1_MSP);
			org1Admin.setName(Config.ADMIN);
			
			Enrollment enrollOrg2Admin = Util.getEnrollment(Config.ORG2_USR_ADMIN_PK, pkFiles2[0].getName(),
					Config.ORG2_USR_ADMIN_CERT, certFiles2[0].getName());
			org2Admin.setEnrollment(enrollOrg2Admin);
			org2Admin.setMspId(Config.ORG2_MSP);
			org2Admin.setName(Config.ADMIN);

			fabClient = new FabricClient(org1Admin);

			// Create a new channel
			Orderer orderer = fabClient.getInstance().newOrderer(Config.ORDERER_NAME, Config.ORDERER_URL);
			ChannelConfiguration channelConfiguration = new ChannelConfiguration(new File(Config.CHANNEL_CONFIG_PATH));

			byte[] channelConfigurationSignatures = fabClient.getInstance()
					.getChannelConfigurationSignature(channelConfiguration, org1Admin);

			mychannel = fabClient.getInstance().newChannel(Config.CHANNEL_NAME, orderer, channelConfiguration,
					channelConfigurationSignatures);

			 peer0_org1 = fabClient.getInstance().newPeer(Config.ORG1_PEER_0, Config.ORG1_PEER_0_URL);
			 peer1_org1 = fabClient.getInstance().newPeer(Config.ORG1_PEER_1, Config.ORG1_PEER_1_URL);
			 peer0_org2 = fabClient.getInstance().newPeer(Config.ORG2_PEER_0, Config.ORG2_PEER_0_URL);
			 peer1_org2 = fabClient.getInstance().newPeer(Config.ORG2_PEER_1, Config.ORG2_PEER_1_URL);

			mychannel.joinPeer(peer0_org1, createPeerOptions().setPeerRoles(EnumSet.of(PeerRole.ENDORSING_PEER, PeerRole.LEDGER_QUERY, PeerRole.CHAINCODE_QUERY, PeerRole.EVENT_SOURCE)));
			mychannel.joinPeer(peer1_org1, createPeerOptions().setPeerRoles(EnumSet.of(PeerRole.LEDGER_QUERY, PeerRole.CHAINCODE_QUERY, PeerRole.EVENT_SOURCE)));
			
			mychannel.addOrderer(orderer);

			mychannel.initialize();
			
			fabClient.getInstance().setUserContext(org2Admin);
			mychannel = fabClient.getInstance().getChannel(Config.CHANNEL_NAME);
			mychannel.joinPeer(peer0_org2, createPeerOptions().setPeerRoles(EnumSet.of(PeerRole.ENDORSING_PEER, PeerRole.LEDGER_QUERY, PeerRole.CHAINCODE_QUERY, PeerRole.EVENT_SOURCE)));
			mychannel.joinPeer(peer1_org2, createPeerOptions().setPeerRoles(EnumSet.of(PeerRole.LEDGER_QUERY, PeerRole.CHAINCODE_QUERY, PeerRole.EVENT_SOURCE)));
			
			Logger.getLogger(CreateChannel.class.getName()).log(Level.INFO, "Channel created "+mychannel.getName());
            Collection<?> peers = mychannel.getPeers();
            Iterator<?> peerIter = peers.iterator();
            while (peerIter.hasNext())
            {
            	  Peer pr = (Peer) peerIter.next();
            	  Logger.getLogger(CreateChannel.class.getName()).log(Level.INFO,pr.getName()+ " at " + pr.getUrl());
            }
            
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

}
