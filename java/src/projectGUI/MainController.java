package projectGUI;

import java.awt.Desktop;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import javafx.application.Application;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.control.TextField;
import javafx.scene.control.*;
import javafx.stage.Stage;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.FileChooser;
import testingFabric.CertificateIssue;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.concurrent.Task;

@SuppressWarnings("restriction")
public class MainController extends Application {

	private Desktop desktop = Desktop.getDesktop();
	static Group group = new Group();
	static ObservableList<Node> list = group.getChildren();
	static Scene scene = new Scene(group, 1000, 800);

	static Group groupCreateECert = new Group();
	static ObservableList<Node> list1 = groupCreateECert.getChildren();

	static Scene sceneCreateECert = new Scene(groupCreateECert, 1000, 800);
	static CertificateIssue s;
	static String str = "";

	@Override
	public void start(Stage stage) throws FileNotFoundException {

		final FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Select and Enrolment Certificate");

		scene.setFill(Color.LIGHTGRAY);

		stage.setTitle("Blockchain of Medical Information Sharing");
		stage.setScene(scene);
		if (list.size() != 0) {
			list.remove(0, list.size());
		}

		Label labelTitle = new Label();
		labelTitle.setFont(new Font("Arial", 40));
		labelTitle.setText("Welcome to the Blockchain Application");
		labelTitle.setLayoutX(scene.getWidth() / 10);
		labelTitle.setLayoutY(scene.getHeight() / 15);

		Label userName = new Label();
		userName.setFont(new Font("Arial", 20));
		userName.setText("UserName");
		userName.setLayoutX(scene.getWidth() / 4);
		userName.setLayoutY(scene.getHeight() / 5);

		TextField textFieldUserName = new TextField();

		textFieldUserName.setLayoutX(userName.getLayoutX() * 1.5);
		textFieldUserName.setLayoutY(userName.getLayoutY());
		textFieldUserName.setPromptText("Input your Username");
		textFieldUserName.isEditable();

		Label passWord = new Label();
		passWord.setFont(new Font("Arial", 20));
		passWord.setText("Password");
		passWord.setLayoutX(scene.getWidth() / 4);
		passWord.setLayoutY(scene.getHeight() / 3.5);

		TextField textFieldPassWord = new TextField();

		textFieldPassWord.setLayoutX(passWord.getLayoutX() * 1.5);
		textFieldPassWord.setLayoutY(passWord.getLayoutY());
		textFieldPassWord.setPromptText("Input your Password");
		textFieldPassWord.isEditable();

		Button loginWithECert = new Button("Login with Enrolment Certificate");
		loginWithECert.setWrapText(true);
		loginWithECert.setLayoutX(passWord.getLayoutX() * 0.5);
		loginWithECert.setLayoutY(passWord.getLayoutY() * 1.3);

		Label ecertDir = new Label();
		ecertDir.setFont(new Font("Cambria", 15));
		ecertDir.setLayoutX(passWord.getLayoutX());
		ecertDir.setLayoutY(loginWithECert.getLayoutY() + 40);

		loginWithECert.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				// TODO Auto-generated method stub
				ecertDir.setText("");
				File file = fileChooser.showOpenDialog(stage);
				if (file != null) {
					ecertDir.setText(file.toString());
				}
			}
		});

		Button login = new Button("Login");
		login.setWrapText(true);
		login.setLayoutX(passWord.getLayoutX() * 2.8);
		login.setLayoutY(loginWithECert.getLayoutY());

		Button createNewECert = new Button("Create a new Enrolment Certificate");
		createNewECert.setWrapText(true);
		createNewECert.setLayoutX(passWord.getLayoutX() * 1.6);
		createNewECert.setLayoutY(loginWithECert.getLayoutY());

		createNewECert.setOnAction(e -> {
			try {
				stage.setScene(loadCreateECert(stage));
			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		});

		// list of elements in the scene
		list.add(labelTitle);
		list.add(login);
		list.add(userName);
		list.add(passWord);
		list.add(loginWithECert);
		list.add(textFieldUserName);
		list.add(textFieldPassWord);
		list.add(ecertDir);
		list.add(createNewECert);
		list.add(loadGif("/home/dara/Documents/fabric-java-sdk/images/XOsX.gif", 0));

		stage.show();
	}

	public static Scene loadCreateECert(Stage stage) throws FileNotFoundException {
		sceneCreateECert.setFill(Color.LIGHTGRAY);
		if (list1.size() != 0) {
			list1.remove(0, list1.size());
		}

		Label labelTitle = new Label();
		labelTitle.setFont(new Font("Arial", 30));
		labelTitle.setText("Create a new enrolment certificate");
		labelTitle.setLayoutX(sceneCreateECert.getWidth() / 4);
		labelTitle.setLayoutY(sceneCreateECert.getHeight() / 10);

		Label userName = new Label();
		userName.setFont(new Font("Arial", 20));
		userName.setText("UserName");
		userName.setLayoutX(sceneCreateECert.getWidth() / 3);
		userName.setLayoutY(sceneCreateECert.getHeight() / 5);

		TextField textFieldUserName = new TextField();

		textFieldUserName.setLayoutX(userName.getLayoutX() * 1.5);
		textFieldUserName.setLayoutY(userName.getLayoutY());
		textFieldUserName.setPromptText("Input your Username");
		textFieldUserName.isEditable();

		Label phoneNumber = new Label();
		phoneNumber.setFont(new Font("Arial", 20));
		phoneNumber.setText("Phone number");
		phoneNumber.setLayoutX(userName.getLayoutX());
		phoneNumber.setLayoutY(userName.getLayoutY() + 50);

		TextField textFieldphoneNumber = new TextField();

		textFieldphoneNumber.setLayoutX(textFieldUserName.getLayoutX());
		textFieldphoneNumber.setLayoutY(phoneNumber.getLayoutY());
		textFieldphoneNumber.setPromptText("Input your phone number");
		textFieldphoneNumber.isEditable();

		Label nationalID = new Label();
		nationalID.setFont(new Font("Arial", 20));
		nationalID.setText("National ID");
		nationalID.setLayoutX(userName.getLayoutX());
		nationalID.setLayoutY(phoneNumber.getLayoutY() + 50);

		TextField textFieldnationalID = new TextField();

		textFieldnationalID.setLayoutX(textFieldUserName.getLayoutX());
		textFieldnationalID.setLayoutY(nationalID.getLayoutY());
		textFieldnationalID.setPromptText("Input your national identity number");
		textFieldnationalID.isEditable();

		Button cancel = new Button("Cancel");

		cancel.setOnAction(e -> stage.setScene(scene));
		cancel.setLayoutX(userName.getLayoutX());
		cancel.setLayoutY(nationalID.getLayoutY() + 80);

		Button register = new Button("Register");
		register.setLayoutX(cancel.getLayoutX() + 100);
		register.setLayoutY(cancel.getLayoutY());

		register.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				// TODO Auto-generated method stub
				try {
					System.out.println("ok");
					final Task<String> doCert = doCert(textFieldUserName.getText(), "Patient", "org1",
							textFieldphoneNumber.getText(), textFieldnationalID.getText());

					doCert.setOnRunning((succeesesEvent) -> {
						try {

							list1.add(loadGif("/home/dara/Documents/fabric-java-sdk/images/Loading3.gif", 1));
						} catch (FileNotFoundException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					});

					doCert.setOnSucceeded((succeesesEvent) -> {

						TextArea display = new TextArea();
						display.setPrefHeight(200);
						display.setPrefWidth(700);
						display.setText(s.cert);
						display.setLayoutX(cancel.getLayoutX() - 200);
						display.setLayoutY(cancel.getLayoutY() + 100);
						list1.remove(list1.size() - 1);
						list1.add(display);
					});
					
					new Thread (doCert).start();

				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});

		list1.add(labelTitle);
		list1.add(userName);
		list1.add(textFieldUserName);
		list1.add(phoneNumber);
		list1.add(textFieldphoneNumber);
		list1.add(nationalID);
		list1.add(textFieldnationalID);
		list1.add(cancel);
		list1.add(register);
		list1.add(loadGif("/home/dara/Documents/fabric-java-sdk/images/XOsX.gif", 0));

		return sceneCreateECert;
	}

	public static Task<String> doCert(String username, String role, String organization, String phoneNumber, String nationID) {
		return new Task<String>() {
			@Override
			protected String call() throws Exception {
				// TODO Auto-generated method stub
				s = new CertificateIssue(username, "Patient", "org1", phoneNumber, nationID);
				return s.cert;
			}

		};
	}

	public void runTask() {
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	private static ImageView loadGif(String dir, int symbol) throws FileNotFoundException {
		Image image = new Image(new FileInputStream(dir));

		ImageView imageView = new ImageView(image);

		if (symbol == 0) {
			imageView.setX(0);
			imageView.setY(0);

			imageView.setFitHeight(100);
			imageView.setFitWidth(100);
		} else if (symbol == 1) {
			imageView.setX(sceneCreateECert.getWidth() / 2.5);
			imageView.setY(sceneCreateECert.getHeight() / 1.7);

			imageView.setFitHeight(150);
			imageView.setFitWidth(150);
		}

		imageView.setPreserveRatio(true);

		return imageView;

	}

	private void printLog(Label textArea, List<File> files) {
		if (files == null || files.isEmpty()) {
			return;
		}

		for (File file : files) {
			System.out.println(file.getAbsolutePath() + "\n");
			textArea.setText(file.getAbsolutePath() + "\n");
		}
	}

	private void openFile(File file) {
		try {
			this.desktop.open(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		launch();
	}

}
