/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package proxyReEncryption;

import proxyReEncryption.Tuple;

import java.io.File;

import it.unisa.dia.gas.jpbc.*;

/**
 *
 * @author david
 */
public class ProxyMain {

	static long cpuTime;
	static long time[] = new long[20];
	static int i = 0;
	private static final String filepath = "/home/dara/Documents/SetupCurve.ser";

	/**
	 * @param args the command line arguments
	 */
	public static void execute() throws Exception {

		// java.security.

		cpuTime = System.nanoTime();

		// 80 bits seg: r = 160, q = 512
		// 128 bits seg: r = 256, q = 1536
		// 256 bits seg: r = 512, q = 7680

		int rBits = 256; // 160; // 20 bytes
		int qBits = 1536; // 512; // 64 bytes
		File inputstream = new File(filepath);
//		AFGHGlobalParameters global = new AFGHGlobalParameters(rBits, qBits);
		AFGHGlobalParameters global = new AFGHGlobalParameters(inputstream);

		medirTiempoMicroSegundos();

//        // Secret keys
//
//        byte[] sk_a = AFGH.generateSecretKey(global).toBytes();
//
//        System.out.println(medirTiempo());
//
//        byte[] sk_b = AFGH.generateSecretKey(global).toBytes();
//
//        System.out.println(medirTiempo());
//
//        // Public keys
//
//        byte[] pk_a = AFGH.generatePublicKey(sk_a, global);
//
//        System.out.println(medirTiempo());
//
//        byte[] pk_b = AFGH.generatePublicKey(sk_b, global);
//
//        System.out.println(medirTiempo());
//
//        // Re-Encryption Key
//
//        byte[] rk_a_b = AFGH.generateReEncryptionKey(pk_b, sk_a, global);
//
//        System.out.println(medirTiempo());
//
//        String message = "David";
//        byte[] m = message.getBytes();
//
//        System.out.println(medirTiempo());
//
//        byte[] c_a = AFGH.secondLevelEncryption(m, pk_a, global);
//
//        System.out.println(medirTiempo());
//
//        String c_a_base64 = Base64.encodeBase64URLSafeString(c_a);
//        //System.out.println("c_a_base64 = " + c_a_base64);
//
//        System.out.println(medirTiempo());
//
//        String rk_base64 = Base64.encodeBase64URLSafeString(rk_a_b);
//        //System.out.println("rk_base64 = " + rk_base64);
//        System.out.println(medirTiempo());
//
//        byte[] c, rk;
//        rk = Base64.decodeBase64(rk_base64);
//
//        System.out.println(medirTiempo());
//
//        c = Base64.decodeBase64(c_a_base64);
//
//        System.out.println(medirTiempo());
//
//        byte[] c_b = AFGH.reEncryption(c, rk, global);
//        //System.out.println("cb: " + Arrays.toString(c_b));
//        System.out.println(medirTiempo());
//
//        String c_b_base64 = Base64.encodeBase64URLSafeString(c_b);
//        //System.out.println("c_b_base64 = " + c_b_base64);
//
//        System.out.println(medirTiempo());
//
//        c = Base64.decodeBase64(c_b_base64);
//
//        System.out.println(medirTiempo());
//
//        byte[] m2 = AFGH.firstLevelDecryption(c_b, sk_b, global);
//        //System.out.println("m2:" + new String(m2));
//
//        System.out.println(medirTiempo());
//
//        assert message.equals(new String(m2).trim());
//
//        System.out.println();
//        System.out.println(global.toBytes().length);
//        System.out.println(sk_a.length);
//        System.out.println(sk_b.length);
//        System.out.println(pk_a.length);
//        System.out.println(pk_b.length);
//        System.out.println(rk_a_b.length);
//        System.out.println(m.length);
//        System.out.println(c_a.length);
//        System.out.println(c_b.length);
//
//        //
//        Map<String, byte[]> map = new HashMap<String, byte[]>();
//        map.put("sk_a", sk_a);
//        map.put("sk_b", sk_b);
//        map.put("pk_a", pk_a);
//        map.put("pk_b", pk_b);
//        map.put("rk_a_b", rk_a_b);
//        map.put("global", global.toBytes());
//        map.put("c_a_base64", c_a_base64.getBytes());
//
//        ObjectOutputStream fos = new ObjectOutputStream(new FileOutputStream("/Users/david/Desktop/pre.object"));
//        fos.writeObject(map);
//        fos.close();
		//

		// Secret keys

		Element sk_a = AFGHProxyReEncryption.generateSecretKey(global);

//		System.out.println("sk_a: " + sk_a);
		// 55942703279436754289327350201025129700132479708900881887454717041059574750659

		medirTiempoMicroSegundos();

		Element sk_b = AFGHProxyReEncryption.generateSecretKey(global);
//		System.out.println("sk_b: " + sk_b);
		// 39348816106901207516171888136588166962277047898270637541991097613321231405294

		medirTiempoMicroSegundos();

		Element sk_b_inverse = sk_b.invert();

		medirTiempoMicroSegundos();

		// Public keys

		Element pk_a = AFGHProxyReEncryption.generatePublicKey(sk_a, global);
//		System.out.println("pk_a: " + pk_a);
		// 674299673186346632304941805853713846295253849735051174116381596256728933518015524840710157146775762697561900838973375542606457696840696806321260776162276798016896638090778430827557368543979754958169937445814513052037404708545329876598360716483250151950140851863900201432251122054106952133644461139161294210899565878670768818273349262173809858066058676189529444873951516725712195029271723962109004837908200924094080736889674275139848003269613889107383914832966236,790468771520773474767278678596193715835871004807390978177729367402823328266950551564492418429156639627400354996093649252368016624556021706887890316344513355869407994757375857541874181194340207703556264620934861500558503612896861656474146537710377029020098702475126522661847599895155511414789727695902104041864930288634758781817216262239116828809935652755597999055593737490904625547177844613250953141235242002709963264303260949573148955551191114392792406609807087,0

		medirTiempoMicroSegundos();

		Element pk_b = AFGHProxyReEncryption.generatePublicKey(sk_b, global);
//		System.out.println("pk_b: " + pk_b);
		// 1221935700242564207586455784898935307730657213345853149630384548961150015531960282594423331183141822994099882738540867530277398252929650998355125749104207252320128257655583915844266199437742884180182133974308914381607376967356527774361985956061403417075213253020084769111626779973759072911806646853483636161674028097512887771652562201292673265685704492935113658637388959033062366848565146072713112272415873494062735421617675714069461798132273680199437156862800390,1095273953805706370296377334506969669285485698185668000448413722439627364494582371390660860452820411391025456339723932623356815199131401027877515270379668456743719798708514984366392447912615141669776858703260404294099051744018623639417723860180811712742038280456596743080048026329188084416474906914647995129113513962929910519195731264888507106439378530357149562905485900685863756803355275033758307160698254527212071842139107137994374330918820820921646820143104818,0

		medirTiempoMicroSegundos();

		ElementPowPreProcessing pk_a_ppp = pk_a.pow();

		medirTiempoMicroSegundos();

		// Re-Encryption Key

		Element rk_a_b = AFGHProxyReEncryption.generateReEncryptionKey(pk_b, sk_a);
		System.out.println("rk_a_b: " + rk_a_b);

		medirTiempoMicroSegundos();

		String message = "ThA+zUVJlNVyD6/w7soitS8A+Pkjj98p93KcG1yq45uN05wf";
		Element m = AFGHProxyReEncryption.stringToElement(message, global.getG2());

		medirTiempoMicroSegundos();

		Tuple c_a = AFGHProxyReEncryption.secondLevelEncryption(m, pk_a_ppp, global);
		System.out.println("c_a: " + c_a);

		medirTiempoMicroSegundos();

		PairingPreProcessing e_ppp = global.getE().pairing(rk_a_b);

		medirTiempoMicroSegundos();

		Tuple c_b = AFGHProxyReEncryption.reEncryption(c_a, rk_a_b, e_ppp);
		System.out.println("c_b: " + c_b);

		medirTiempoMicroSegundos();

		Element m2 = AFGHProxyReEncryption.firstLevelDecryptionPreProcessing(c_b, sk_b_inverse, global);
//		System.out.println("m2: " + m2);

		medirTiempoMicroSegundos();

		assert message.equals(new String(m2.toBytes()).trim());
		System.out.println("m2: " + new String(m2.toBytes()).trim());

//        for(int j = 0; j < i; j++){
//            System.out.println(time[j]);
//        }

//        System.out.println("m string : " + message.getBytes().length);
//        System.out.println("m in G2 : " + m.toBytes().length);
//        System.out.println("c_a_1 in G2: " + c_a.get(1).toBytes().length);
//        System.out.println("c_a_2 in G1: " + c_a.get(2).toBytes().length);
//        System.out.println("c_b_1 in G2: " + c_b.get(1).toBytes().length);
//        System.out.println("c_b_2 in G2: " + c_b.get(2).toBytes().length);
//        System.out.println("m2 in G2 : " + m2.toBytes().length);
		// System.out.println(AFGH.elementToString(m2));

		// System.out.println(medirTiempo());

	}

	public static long medirTiempoMicroSegundos() {
		time[i] = (System.nanoTime() - cpuTime) / 1000;
		i++;
		cpuTime = System.nanoTime();
		return time[i];
	}
}
