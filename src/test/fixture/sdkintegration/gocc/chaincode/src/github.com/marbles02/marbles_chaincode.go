/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
*/

// ====CHAINCODE EXECUTION SAMPLES (CLI) ==================

// ==== Invoke marbles ====
// peer chaincode invoke -C myc1 -n marbles -c '{"Args":["initMarble","marble1","blue","35","tom"]}'
// peer chaincode invoke -C myc1 -n marbles -c '{"Args":["initMarble","marble2","red","50","tom"]}'
// peer chaincode invoke -C myc1 -n marbles -c '{"Args":["initMarble","marble3","blue","70","tom"]}'
// peer chaincode invoke -C myc1 -n marbles -c '{"Args":["transferMarble","marble2","jerry"]}'
// peer chaincode invoke -C myc1 -n marbles -c '{"Args":["transferMarblesBasedOnColor","blue","jerry"]}'
// peer chaincode invoke -C myc1 -n marbles -c '{"Args":["delete","marble1"]}'

// ==== Query marbles ====
// peer chaincode query -C myc1 -n marbles -c '{"Args":["readMarble","marble1"]}'
// peer chaincode query -C myc1 -n marbles -c '{"Args":["getMarblesByRange","marble1","marble3"]}'
// peer chaincode query -C myc1 -n marbles -c '{"Args":["getHistoryForMarble","marble1"]}'

// Rich Query (Only supported if CouchDB is used as state database):
//   peer chaincode query -C myc1 -n marbles -c '{"Args":["queryMarblesByOwner","tom"]}'
//   peer chaincode query -C myc1 -n marbles -c '{"Args":["queryMarbles","{\"selector\":{\"owner\":\"tom\"}}"]}'

//The following examples demonstrate creating indexes on CouchDB
//Example hostname:port configurations
//
//Docker or vagrant environments:
// http://couchdb:5984/
//
//Inside couchdb docker container
// http://127.0.0.1:5984/

// Index for chaincodeid, docType, owner.
// Note that docType and owner fields must be prefixed with the "data" wrapper
// chaincodeid must be added for all queries
//
// Definition for use with Fauxton interface
// {"index":{"fields":["chaincodeid","data.docType","data.owner"]},"ddoc":"indexOwnerDoc", "name":"indexOwner","type":"json"}
//
// example curl definition for use with command line
// curl -i -X POST -H "Content-Type: application/json" -d "{\"index\":{\"fields\":[\"chaincodeid\",\"data.docType\",\"data.owner\"]},\"name\":\"indexOwner\",\"ddoc\":\"indexOwnerDoc\",\"type\":\"json\"}" http://hostname:port/myc1/_index
//

// Index for chaincodeid, docType, owner, size (descending order).
// Note that docType, owner and size fields must be prefixed with the "data" wrapper
// chaincodeid must be added for all queries
//
// Definition for use with Fauxton interface
// {"index":{"fields":[{"data.size":"desc"},{"chaincodeid":"desc"},{"data.docType":"desc"},{"data.owner":"desc"}]},"ddoc":"indexSizeSortDoc", "name":"indexSizeSortDesc","type":"json"}
//
// example curl definition for use with command line
// curl -i -X POST -H "Content-Type: application/json" -d "{\"index\":{\"fields\":[{\"data.size\":\"desc\"},{\"chaincodeid\":\"desc\"},{\"data.docType\":\"desc\"},{\"data.owner\":\"desc\"}]},\"ddoc\":\"indexSizeSortDoc\", \"name\":\"indexSizeSortDesc\",\"type\":\"json\"}" http://hostname:port/myc1/_index

// Rich Query with index design doc and index name specified (Only supported if CouchDB is used as state database):
//   peer chaincode query -C myc1 -n marbles -c '{"Args":["queryMarbles","{\"selector\":{\"docType\":\"marble\",\"owner\":\"tom\"}, \"use_index\":[\"_design/indexOwnerDoc\", \"indexOwner\"]}"]}'

// Rich Query with index design doc specified only (Only supported if CouchDB is used as state database):
//   peer chaincode query -C myc1 -n marbles -c '{"Args":["queryMarbles","{\"selector\":{\"docType\":{\"$eq\":\"marble\"},\"owner\":{\"$eq\":\"tom\"},\"size\":{\"$gt\":0}},\"fields\":[\"docType\",\"owner\",\"size\"],\"sort\":[{\"size\":\"desc\"}],\"use_index\":\"_design/indexSizeSortDoc\"}"]}'

package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"strconv"
	"strings"
	"time"

	"github.com/hyperledger/fabric-chaincode-go/shim"
	"golang.org/x/crypto/bcrypt"
	//	"github.com/hyperledger/fabric/core/chaincode/shim/ext/cid"
	pb "github.com/hyperledger/fabric-protos-go/peer"
)

// SimpleChaincode example simple Chaincode implementation
type SimpleChaincode struct {
	CORE_CHAINCODE_ID_NAME string
}

// Medical records type

type medicalrecords struct {
	ObjectType       string `json:"docType"`
	ID               string `json:"id"` //the fieldtags are needed to keep case from bouncing around
	PatientID        string `json:"patientId"`
	Date             string `json:"date"`
	RecordAddress    string `json:"recordAddress"`
	HospitalID       string `json:"hospitalId"`
	DoctorID         string `json:"doctorId"`
	Action           string `json:"action"`
	PermissionStatus string `json:"permissionStatus"`
	RecordHash       string `json:"recordsHash"`
	ConsentVersion   string `json:"consentVersion"`
	ConsentHash      string `json:"consentHash"`
}

// Consent of records
var data = [][]string{
	{"PL1", "Nurse", "Read", "Education", "E-MedicineDiscovery"},
	{"PL2", "Cardiologist", "Copy", "MedicalTreatment, Education", "M-Education"},
	{"PL3", "Physician", "Read", "GeneralPurpose", "Insurance"},
}

var newConsent = [][]string{
	{"PL2", "Cardiologist", "Copy", "MedicalTreatment, Education", "M-Education"},
	{"PL3", "Physician", "Read", "GeneralPurpose", "Insurance"},
	{"PL4", "Research Student", "Copy", "Education", "E-MedicineDiscovery"},
	{"PL5", "Insurance", "Read", "Insurance", "null"},
}

var purposeTree = `{

"Education":{
"E-MedicineDiscovery":"null"
	"E-Statistic": {
		"S-Survey":"null"
	},
	
},

"MedicalTreatment":{
	"M-Cancer": "null",
	"M-Diabetic":"null",
	"M-Mental":"null",
	"M-Education":{
		"E-Reporting": "null"
	}
},

"Insurance":{
	"I-EvaluateInsuranceStatus": "null"
}

}`

type medicalConsent struct {
	ObjectType            string
	ID                    string
	PatientECertHash      string
	Date                  string
	Consent               []*Policy
	RecordHashValue       string
	RecordAddress         string
	EncryptedSymmetricKey string
	RecordsOwnerAddress   string
}

type consentTree struct {
	ConsentType     string `json:"ConsentType"`
	ConsentVersion  string `json:"ConsentVersion"`
	ConsentTreeFull string `json:"ConsentTreeFull"`
}

type Policy struct {
	Name              string `json:"name"`
	Role              string `json:"role"`
	Action            string `json:"action"`
	AllowedPurpose    string `json:"allowedpurpose"`
	ProhibitedPurpose string `json:"prohibitedpurpose"`
}

var i = false
var allowPurpose interface{}

// ===================================================================================
// Main
// ===================================================================================
func main() {
	err := shim.Start(new(SimpleChaincode))
	if err != nil {
		fmt.Printf("Error starting Simple chaincode: %s", err)
	}
}

// Init initializes chaincode
// ===========================
func (t *SimpleChaincode) Init(stub shim.ChaincodeStubInterface) pb.Response {
	return shim.Success(nil)
}

// Invoke - Our entry point for Invocations
// ========================================
func (t *SimpleChaincode) Invoke(stub shim.ChaincodeStubInterface) pb.Response {
	function, args := stub.GetFunctionAndParameters()
	fmt.Println("invoke is running " + function)

	// Handle different functions
	if function == "initRecords" { //create a new marble
		return t.initRecords(stub, args)
	} else if function == "queryRecordsByKeyandExpectedValue" { //find marbles for owner X using rich query
		return t.queryRecordsByKeyandExpectedValue(stub, args)
	} else if function == "createRecordsWithConsent" {
		return t.createRecordsWithConsent(stub, args)
	} else if function == "reinforcementConsent" {
		return t.reinforcementConsent(stub, args)
	} else if function == "uploadPurposeTree" {
		return t.uploadPurposeTree(stub, args)
	} else if function == "searchBasedSalt" {
		return t.searchBasedSalt(stub, args)
	} else if function == "updateConsent" {
		return t.updateConsent(stub, args)
	} else if function == "getHistoryForData" {
		return t.getHistoryForData(stub, args)
	}

	fmt.Println("invoke did not find func: " + function) //error

	return shim.Error("Received unknown function invocation")
}

func (t *SimpleChaincode) searchBasedSalt(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	var buffer bytes.Buffer
	var result []map[string]interface{}
	var dataHash string
	queryString := fmt.Sprintf("{\"selector\":{\"ObjectType\":\"Medical_repository\"}}")

	queryResults, err := getQueryResultForQueryString(stub, queryString)
	if err != nil {
		return shim.Error(err.Error())
	}
	json.Unmarshal(queryResults, &result)
	for key, value := range result {
		fmt.Println()
		fmt.Println("For the", key, " transaction there has policies: ")
		//		test = fmt.Sprintf("transaction%d", key)
		for key1, value1 := range value {
			if key1 == "Record" {
				data := value1.(map[string]interface{})
				for key2, value2 := range data {
					if key2 == "PatientECertHash" {
						dataHash = value2.(string)
						fmt.Println("For hash: ", dataHash)
						dataBool := compareID(dataHash, []byte(args[0]))
						if dataBool {
							dataRow := []string{data["RecordAddress"].(string), data["EncryptedSymmetricKey"].(string)}
							justString := strings.Join(dataRow, " ")
							buffer.WriteString(justString)
							buffer.WriteString(",")
						}
					}
				}
			}
		}
	}
	return shim.Success(buffer.Bytes())
}

func (t *SimpleChaincode) reinforcementConsent(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	var result []map[string]interface{}
	var results map[string]interface{}
	var buffer bytes.Buffer

	queryString := fmt.Sprintf("{\"selector\":{\"ObjectType\":\"Medical_repository\"}}")

	queryResults, err := getQueryResultForQueryString(stub, queryString)
	if err != nil {
		return shim.Error(err.Error())
	}
	json.Unmarshal(queryResults, &result)
	json.Unmarshal([]byte(purposeTree), &results)

	eCertHash := args[0]
	requestorRole := args[1]
	action := args[2]
	accessPurpose := args[3]
	for key, value := range result {
		fmt.Println()
		fmt.Println("For the", key, " transaction there has policies: ")
		//		test = fmt.Sprintf("transaction%d", key)
		for key1, value1 := range value {
			if key1 == "Record" {
				data := value1.(map[string]interface{})
				if compareID(data["PatientECertHash"].(string), []byte(eCertHash)) {
					dataConsent := data["Consent"].([]interface{})
					for key3, value3 := range dataConsent {
						consentvalue := value3.(map[string]interface{})
						fmt.Println()
						fmt.Println("For the", key3, " Policy")
						fmt.Println("For the Role ", consentvalue["role"])
						if consentvalue["role"] == requestorRole && consentvalue["action"] == action {
							RecurvesiveSearchingDescendent(results, consentvalue["prohibitedpurpose"].(string))
							if allowPurpose != nil {
								RecurvesiveSearching(allowPurpose.(map[string]interface{}), accessPurpose)
							}
							if consentvalue["prohibitedpurpose"].(string) != accessPurpose || i != true {
								RecurvesiveSearchingDescendent(results, consentvalue["allowedpurpose"].(string))
								if allowPurpose != nil {
									RecurvesiveSearching(allowPurpose.(map[string]interface{}), accessPurpose)
								}
								if consentvalue["allowedpurpose"].(string) == accessPurpose || i == true {
									dataRow := []string{data["RecordAddress"].(string), data["EncryptedSymmetricKey"].(string)}
									justString := strings.Join(dataRow, " ")
									buffer.WriteString(justString)
									buffer.WriteString(",")
								}
							}
						}
					}
				}
			}
		}
	}

	return shim.Success(buffer.Bytes())
}

func RecurvesiveSearchingDescendent(a map[string]interface{}, expectedData string) {
	for key2, value2 := range a {
		//		fmt.Println("Data test: ", a[key2])
		if key2 == expectedData {
			allowPurpose = value2
		} else if value2 != "null" {
			datatest := value2.(map[string]interface{})
			RecurvesiveSearching(datatest, expectedData)
		}
	}
}
func RecurvesiveSearching(a map[string]interface{}, expectedData string) {
	for key2, value2 := range a {
		//		fmt.Println("Data test: ", a[key2])
		if key2 == expectedData {
			i = true
		} else if value2 != "null" {
			datatest := value2.(map[string]interface{})
			RecurvesiveSearching(datatest, expectedData)
		}
	}
}

func Contains(a []string, x string) bool {
	for _, n := range a {
		if x == n {
			return true
		}
	}
	return false
}

func getPwd(pwd string) []byte {
	return []byte(pwd)
}

func hashAndSalt(pwd []byte) string {

	// Use GenerateFromPassword to hash & salt pwd
	// MinCost is just an integer constant provided by the bcrypt
	// package along with DefaultCost & MaxCost.
	// The cost can be any value you want provided it isn't lower
	// than the MinCost (4)
	hash, err := bcrypt.GenerateFromPassword(pwd, bcrypt.MinCost)
	if err != nil {
		log.Println(err)
	}
	// GenerateFromPassword returns a byte slice so we need to
	// convert the bytes to a string and return it
	return string(hash)
}
func compareID(hashedPwd string, plainPwd []byte) bool {
	// Since we'll be getting the hashed password from the DB it
	// will be a string so we'll need to convert it to a byte slice
	byteHash := []byte(hashedPwd)
	err := bcrypt.CompareHashAndPassword(byteHash, plainPwd)
	if err != nil {
		log.Println("Hashed of ECert with Salt is not the hash of the given ECert ID")
		return false
	}

	return true
}

func (t *SimpleChaincode) createRecordsWithConsent(stub shim.ChaincodeStubInterface, args []string) pb.Response {

	if len(args) != 7 {
		return shim.Error("Incorrect number of arguments. Expecting 7")
	}

	fmt.Println("- start init createRecordsWithConsent")
	for i := 0; i < len(args); i++ {
		if len(args[i]) <= 0 {
			msg := fmt.Sprintf("%dst argument must be a non-empty string", i)
			return shim.Error(msg)
		}
	}
	objectType := "Medical_repository"
	//	fmt.Println("arg", strings.ToLower(args[1]))
	//	byteData := getPwd(strings.ToLower(args[1]))
	//	hashSalt := hashAndSalt(byteData)
	//	toLower := strings.ToLower(hashSalt)
	//	fmt.Println(hashSalt)
	//	fmt.Println(comparePasswords(hashSalt, byteData))
	r := &medicalConsent{ObjectType: objectType, ID: strings.ToLower(args[0]), PatientECertHash: args[1], Date: strings.ToLower(args[2]), RecordHashValue: strings.ToLower(args[3]), RecordAddress: strings.ToLower(args[4]), EncryptedSymmetricKey: strings.ToLower(args[5]), RecordsOwnerAddress: strings.ToLower(args[6])}
	for i := 0; i < len(data); i++ {
		policy := &Policy{data[i][0], data[i][1], data[i][2], data[i][3], data[i][4]}
		r.Consent = append(r.Consent, policy)
	}

	dat, err := json.Marshal(r)
	if err != nil {
		return shim.Error(err.Error())
	}

	err = stub.PutState(strings.ToLower(args[0]), dat)
	if err != nil {
		return shim.Error(err.Error())
	}

	//	fmt.Println(string(dat))
	fmt.Println("- end init medicalrecords")
	return shim.Success(nil)
}

func (t *SimpleChaincode) uploadPurposeTree(stub shim.ChaincodeStubInterface, args []string) pb.Response {

	if len(args) != 2 {
		return shim.Error("Incorrect number of arguments. Expecting 2")
	}

	objectType := "Consent_Tree"
	consentID := args[0]
	consentTreeList := args[1]

	consent := &consentTree{objectType, consentID, consentTreeList}
	marbleJSONasBytes, err := json.Marshal(consent)
	if err != nil {
		return shim.Error(err.Error())
	}

	// === Save marble to state ===
	err = stub.PutState(consentID, marbleJSONasBytes)
	if err != nil {
		return shim.Error(err.Error())
	}

	return shim.Success(nil)

}

func (t *SimpleChaincode) initRecords(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	var err error

	//   0       1       	2     		3		4				5			6   	7
	// "Id", "patientID", "Date", "records", "hospitalID", "doctorID", "action","permissionStatus"
	if len(args) != 11 {
		return shim.Error("Incorrect number of arguments. Expecting 11")
	}

	// ==== Input sanitation ====
	fmt.Println("- start init medicalrecords")
	for i := 0; i < len(args); i++ {
		if len(args[i]) <= 0 {
			msg := fmt.Sprintf("%dst argument must be a non-empty string", i)
			return shim.Error(msg)
		}
	}
	objectType := "Medical_repository"
	mediId := args[0]
	patientId := strings.ToLower(args[1])
	date := strings.ToLower(args[2])
	recordAddress := strings.ToLower(args[3])
	hospitalId := strings.ToLower(args[4])
	doctorId := strings.ToLower(args[5])
	action := strings.ToLower(args[6])
	permissionStatus := strings.ToLower(args[7])
	recordHash := strings.ToLower(args[8])
	consentVersion := strings.ToLower(args[9])
	consentHash := strings.ToLower(args[10])

	// ==== Check if marble already exists ====
	marbleAsBytes, err := stub.GetState(mediId)
	if err != nil {
		return shim.Error("Failed to get medicalrecords: " + err.Error())
	} else if marbleAsBytes != nil {
		fmt.Println("This medicalrecords already exists: " + mediId)
		return shim.Error("This medicalrecords already exists: " + mediId)
	}

	// ==== Create marble object and marshal to JSON ====
	medicalrecords := &medicalrecords{objectType, mediId, patientId, date, recordAddress, hospitalId, doctorId, action, permissionStatus, recordHash, consentVersion, consentHash}
	marbleJSONasBytes, err := json.Marshal(medicalrecords)
	if err != nil {
		return shim.Error(err.Error())
	}

	// === Save marble to state ===
	err = stub.PutState(mediId, marbleJSONasBytes)
	if err != nil {
		return shim.Error(err.Error())
	}

	// ==== Marble saved and indexed. Return success ====
	fmt.Println("- end init medicalrecords")
	return shim.Success(nil)
}

func (t *SimpleChaincode) queryRecordsByKeyandExpectedValue(stub shim.ChaincodeStubInterface, args []string) pb.Response {

	//   1
	// "bob"
	keytoRead := args[0]
	expectedValue := args[1]

	queryString := fmt.Sprintf("{\"selector\":{\"ConsentType\":\"Consent_Tree\",\"%s\":\"%s\"}}", keytoRead, expectedValue)

	queryResults, err := getQueryResultForQueryString(stub, queryString)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(queryResults)
}

func getQueryResultForQueryString(stub shim.ChaincodeStubInterface, queryString string) ([]byte, error) {

	fmt.Printf("- getQueryResultForQueryString queryString:\n%s\n", queryString)

	resultsIterator, err := stub.GetQueryResult(queryString)
	if err != nil {
		return nil, err
	}
	defer resultsIterator.Close()

	buffer, err := constructQueryResponseFromIterator(resultsIterator)
	if err != nil {
		return nil, err
	}

	fmt.Printf("- getQueryResultForQueryString queryResult:\n%s\n", buffer.String())

	return buffer.Bytes(), nil
}

func getQueryResultForString(stub shim.ChaincodeStubInterface, queryString string) (string, error) {

	fmt.Printf("- getQueryResultForQueryString queryString:\n%s\n", queryString)

	resultsIterator, err := stub.GetQueryResult(queryString)
	if err != nil {
		return "", err
	}
	defer resultsIterator.Close()

	buffer, err := constructQueryResponseFromIterator(resultsIterator)
	if err != nil {
		return "", err
	}

	fmt.Printf("- getQueryResultForQueryString queryResult:\n%s\n", buffer.String())

	return buffer.String(), nil
}

func constructQueryResponseFromIterator(resultsIterator shim.StateQueryIteratorInterface) (*bytes.Buffer, error) {
	// buffer is a JSON array containing QueryResults
	var buffer bytes.Buffer
	buffer.WriteString("[")

	bArrayMemberAlreadyWritten := false
	for resultsIterator.HasNext() {
		queryResponse, err := resultsIterator.Next()
		if err != nil {
			return nil, err
		}

		fmt.Print("data from query: %s", queryResponse.Key)
		// Add a comma before array members, suppress it for the first array member
		if bArrayMemberAlreadyWritten == true {
			buffer.WriteString(",")
		}
		buffer.WriteString("{\"Key\":")
		buffer.WriteString("\"")
		buffer.WriteString(queryResponse.Key)
		buffer.WriteString("\"")

		buffer.WriteString(", \"Record\":")
		// Record is a JSON object, so we write as-is
		buffer.WriteString(string(queryResponse.Value))
		buffer.WriteString("}")
		bArrayMemberAlreadyWritten = true
	}
	buffer.WriteString("]")

	return &buffer, nil
}

// ===========================================================
// Update the Consent
// ===========================================================
func (t *SimpleChaincode) updateConsent(stub shim.ChaincodeStubInterface, args []string) pb.Response {

	//   0       1
	// "name", "bob"
	if len(args) < 1 {
		return shim.Error("Incorrect number of arguments. Expecting 1")
	}

	TxID := args[0]
	fmt.Println("- start Update Consent ", TxID, newConsent)

	marbleAsBytes, err := stub.GetState(TxID)
	if err != nil {
		return shim.Error("Failed to get marble:" + err.Error())
	} else if marbleAsBytes == nil {
		return shim.Error("Marble does not exist")
	}

	updateConsent := medicalConsent{}
	err = json.Unmarshal(marbleAsBytes, &updateConsent) //unmarshal it aka JSON.parse()
	if err != nil {
		return shim.Error(err.Error())
	}

	tmpupdateConsent := medicalConsent{}
	for i := 0; i < len(newConsent); i++ {
		policy := &Policy{newConsent[i][0], newConsent[i][1], newConsent[i][2], newConsent[i][3], newConsent[i][4]}
		tmpupdateConsent.Consent = append(tmpupdateConsent.Consent, policy)
	}
	updateConsent.Consent = tmpupdateConsent.Consent
	marbleJSONasBytes, _ := json.Marshal(updateConsent)
	err = stub.PutState(TxID, marbleJSONasBytes) //rewrite the marble
	if err != nil {
		return shim.Error(err.Error())
	}

	fmt.Println("- end Update Consent (success)")
	return shim.Success(nil)
}

// GET history of one specific TxID

func (t *SimpleChaincode) getHistoryForData(stub shim.ChaincodeStubInterface, args []string) pb.Response {

	if len(args) < 1 {
		return shim.Error("Incorrect number of arguments. Expecting 1")
	}

	TxID := args[0]

	fmt.Printf("- start getHistoryForID: %s\n", TxID)

	resultsIterator, err := stub.GetHistoryForKey(TxID)
	if err != nil {
		return shim.Error(err.Error())
	}
	defer resultsIterator.Close()

	// buffer is a JSON array containing historic values for the marble
	var buffer bytes.Buffer
	buffer.WriteString("[")

	bArrayMemberAlreadyWritten := false
	for resultsIterator.HasNext() {
		response, err := resultsIterator.Next()
		if err != nil {
			return shim.Error(err.Error())
		}
		// Add a comma before array members, suppress it for the first array member
		if bArrayMemberAlreadyWritten == true {
			buffer.WriteString(",")
		}
		buffer.WriteString("{\"TxId\":")
		buffer.WriteString("\"")
		buffer.WriteString(response.TxId)
		buffer.WriteString("\"")

		buffer.WriteString(", \"Value\":")
		// if it was a delete operation on given key, then we need to set the
		//corresponding value null. Else, we will write the response.Value
		//as-is (as the Value itself a JSON marble)
		if response.IsDelete {
			buffer.WriteString("null")
		} else {
			buffer.WriteString(string(response.Value))
		}

		buffer.WriteString(", \"Timestamp\":")
		buffer.WriteString("\"")
		buffer.WriteString(time.Unix(response.Timestamp.Seconds, int64(response.Timestamp.Nanos)).String())
		buffer.WriteString("\"")

		buffer.WriteString(", \"IsDelete\":")
		buffer.WriteString("\"")
		buffer.WriteString(strconv.FormatBool(response.IsDelete))
		buffer.WriteString("\"")

		buffer.WriteString("}")
		bArrayMemberAlreadyWritten = true
	}
	buffer.WriteString("]")

	fmt.Printf("- getHistoryForMarble returning:\n%s\n", buffer.String())

	return shim.Success(buffer.Bytes())
}
