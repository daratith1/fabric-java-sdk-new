package org.hyperledger.fabric.chaincode.wallet;

public class Wallet {
    private String userID;
    private Double records;
    private Integer pubkey;
    private String action;
    private String purpose;

    public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getPurpose() {
		return purpose;
	}

	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}

	public Wallet(String userID, Double records) {
        this.userID = userID;
        this.records = records;
    }
	
	public Wallet(String userID, Double records, Integer pubKey, String action, String purpose) {
        this.userID = userID;
        this.records = records;
        this.pubkey = pubKey;
        this.action = action;
        this.purpose = purpose;
    }
    
	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public Double getRecords() {
		return records;
	}

	public void setRecords(Double records) {
		this.records = records;
	}

	public Integer getPubkey() {
		return pubkey;
	}

	public void setPubkey(Integer pubkey) {
		this.pubkey = pubkey;
	}
	

}
